# love2d-projects-2014

This repository contains some of my random gibberish stuff I programmed during the course of 2014 (and late 2013) with Lua's LOVE2D library. (framework?)

It's already sorted and filtered, yet may still contain bad code.

The reason I decided to share it is because the experience I gained during making these experiments was super influential for me.

Actually, some of the projects shared here got their own repo on my account.