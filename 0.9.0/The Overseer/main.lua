require 'mouse'
require 'round'
require 'citygen'
require 'gui'
require 'constants'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 12, 8
	SizeX, SizeY = 1280/TileSize, 24-4
	
	--technical variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	gameclock = 0
	
	MouseX, MouseY, MouseTileX, MouseTileY = 0,0,0,0
	
	--game variables
	rounds = 0
	Year, Month, Day = 1, math.random(1,12), math.random(1,20)
	
	GovernmentColor = math.random(1,12)
	PlayerColor = math.random(1,12)
	
	--distribution definer for age and sex
	
	--male = female > Cloudan > transhuman > robot > transgender
	Distribution_Sex = { 35, 35, 2, 9, 15, 4}
	
	--twenties = thirties > fourties > fifties = children > elders
	Distribution_Age = { 11, 28, 28, 17, 11, 5 }
	

	--to understand this, read "friends and enemies.txt"
	friends = {7, 9, 10, 6, 8, 4, 1, 5, 2, 3, 12, 11}
	
	enemies = {2, 1, 8, 5, 4, 11, 10, 3, 12, 7, 6, 9}

	--font
	font = {}
	
	font[1] = love.graphics.newFont("slkscre.ttf", 16)
	font[2] = love.graphics.newFont("arial.ttf", 12)	
	love.graphics.setFont(font[1])		
	

	
	
	--tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, TilesetX do
		for i=1, TilesetY do 
			Tileset[((i-1)*TilesetX)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX*TileSize, TilesetY*TileSize)
		end
	end	
	
	--logoset
	LogoPic = love.graphics.newImage('LOGOS.png')
	Logos = { }
	
	for j=1, 3 do
		for i=1, 4 do 
			Logos[((i-1)*3)+j] = love.graphics.newQuad((j-1)*64, (i-1)*64, 64,64, 192, 256)
		end
	end	
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = GovernmentColor
		end
		
	end
	
	firstX, firstY = math.random(1,SizeX), math.random(1,SizeY)
	
	Field[firstX][firstY] = PlayerColor
	
	--adding properties, basically
	Properties = { }
	
	for j=1,SizeX do
		Properties[j] = { }	
		for i=1, SizeY do
			Properties[j][i] = { Type = 1, Morale = 100, Population = {  } }
		end		
	end		
	
	--details
	CityGen_Main()

	
end

function love.draw()


	Display_PlayingField()
	Display_GeneralStats()
	Display_TileStats()
	Display_Date(384, 720)
	
	
	--placeholder text
	love.graphics.setFont(font[1])	
	love.graphics.print("The Overseer pre-alpha by Katamori",864,752)
	
	love.graphics.print(lastX..", "..lastY,864,736)
end

function love.update(dt)

	--constants
	gameclock = gameclock +1
	
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	MouseTileX, MouseTileY = math.floor(MouseX/TileSize)+1, math.floor(MouseY/TileSize)+1
	

end













function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if x-1 >= 1 	and Table[x-1][y] == valueToCheck then asd=asd+1 end
	if x+1 <= SizeX and Table[x+1][y] == valueToCheck then asd=asd+1 end
	if y-1 >= 1 	and Table[x][y-1] == valueToCheck then asd=asd+1 end
	if y+1 <= SizeY and Table[x][y+1] == valueToCheck then asd=asd+1 end
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if x-j > 0 and x-j <= SizeX 
			and y-i > 0 and y-i <= SizeY 
			and Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=1,SizeX do
		for i=1, SizeY do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end

function Rounding(value)

	local returnval
	if value - math.floor(value) < 0.5 then returnval = math.floor(value) else returnval = math.ceil(value) end
	
	return returnval
end

function Sum(array, length)
	
	local returnvar = 0
	for i=1, length do returnvar = returnvar + array[i] end	
	return returnvar

end

function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




