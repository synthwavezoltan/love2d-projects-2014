--functions that I have to call in love.draw()

function Display_PlayingField()

	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPic, Tileset[((Properties[j][i].Type-1)*12) + Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
		end
	end
	

end


function Display_GeneralStats()
	
	local GovNum = CountColors(Field, GovernmentColor)
	local GovPerc = CountColors(Field, GovernmentColor)/((SizeX*SizeY)/100)
	local GovTotal = 0
	
	local PlayerNum = CountColors(Field, PlayerColor)
	local PlayerPerc = 100-GovPerc
	local PlayerTotal = 0
	
	
	--global supporting counter
		for j=1,SizeX do
			for i=1, SizeY do
				if Field[j][i] == GovernmentColor then GovTotal = GovTotal + StatCounter(j, i, Engagement, GovernmentColor) 
				elseif Field[j][i] == PlayerColor then PlayerTotal = PlayerTotal + StatCounter(j, i, Engagement, PlayerColor)  end
			end
		end

	--at this point, both GovTotal and PlayerTotal has got their values
	
	love.graphics.print("The "..names[GovernmentColor].."\n"
						..GovNum.." units, "..GovPerc.."% of total\n"
						.."joined supporters = "..GovTotal,0, 640)
						
					
	love.graphics.print("vs. The "..names[PlayerColor].."\n"
						..PlayerNum.." units, "..PlayerPerc.."% of total\n"
						.."joined supporters = "..PlayerTotal,0, 720)						
	--number of rounds
	love.graphics.print("Round "..rounds, 384, 704)						

end

function Display_TileStats()
	
	local teenager, twenties, thirties, fourties, fifties, elder = 0,0,0,0,0,0
	
	if Field[MouseTileX][MouseTileY] ~= nil then			
		love.graphics.draw(LogoPic, Logos[Field[MouseTileX][MouseTileY]], 640,672)
		love.graphics.print("Morale = "..Properties[MouseTileX][MouseTileY].Morale.."\n"..
							"Population = "..#Properties[MouseTileX][MouseTileY].Population,704,672)
		
		--other stats of population	
		--engagement
		for i=1, 12 do
				love.graphics.print(names[i].." population = "..StatCounter(MouseTileX, MouseTileY, Engagement, i),0,16*i)
		end
		
		--age
		for i=14, 99 do
			
			if i <20 then
				teenager = teenager + StatCounter(MouseTileX, MouseTileY, Age, i)
			elseif i > 20 and i < 30 then
				twenties = twenties + StatCounter(MouseTileX, MouseTileY, Age, i)			
			elseif i > 30 and i < 40 then
				thirties = thirties + StatCounter(MouseTileX, MouseTileY, Age, i)			
			elseif i > 40 and i < 50 then
				fourties = fourties + StatCounter(MouseTileX, MouseTileY, Age, i)
			elseif i > 50 and i < 60 then
				fifties = fifties + StatCounter(MouseTileX, MouseTileY, Age, i)				
			else
				elder = elder + StatCounter(MouseTileX, MouseTileY, Age, i)			
			end
			
		end
		
		love.graphics.print("Below 20: "..teenager.."\n"..
							"20-30 years: "..twenties.."\n"..
							"30-40 years: "..thirties.."\n"..
							"40-50 years: "..fourties.."\n"..
							"50-60 years: "..fifties.."\n"..
							"Above 60: "..teenager,0,224)

		
		--sex
		love.graphics.print("Males: "..StatCounter(MouseTileX, MouseTileY, Sex, 1).."\n"..
							"Females: "..StatCounter(MouseTileX, MouseTileY, Sex, 2).."\n"..
							"Transgander: "..StatCounter(MouseTileX, MouseTileY, Sex, 3).."\n"..
							"Moebius members: "..StatCounter(MouseTileX, MouseTileY, Sex, 4).."\n"..							
							"Cloudans: "..StatCounter(MouseTileX, MouseTileY, Sex, 5).."\n"..
							"Androids: "..StatCounter(MouseTileX, MouseTileY, Sex, 6),0,336)

		
		--just debug: complete population list
		love.graphics.setFont(font[2])	
		for i=1, 50 do
		
			love.graphics.print("no. "..i..": "..Citizen_Description(Properties[MouseTileX][MouseTileY].Population[i]),448, 12*(i-1))			

		end
		
	
	end
	
end

--also taken from the Moon Property prototype stuff
--and I didn't even have to modify it...I'm a fuckin' genius sometimes
function Display_Date(x,y)
	
	love.graphics.setFont(font[1])	
	local a,b = "1","2"
	
	if Day == 1 then a = Day.."st"
	elseif Day == 2 then a = Day.."nd"
	elseif Day == 3 then a = Day.."rd"		
	else a = Day.."th" end
	
	if Month == 1 then b = "January" 
	elseif Month == 2 then b = "February"
	elseif Month == 3 then b = "March"	
	elseif Month == 4 then b = "April"
	elseif Month == 5 then b = "May"	
	elseif Month == 6 then b = "June"
	elseif Month == 7 then b = "July"	
	elseif Month == 8 then b = "August"
	elseif Month == 9 then b = "September"
	elseif Month == 10 then b = "October"	
	elseif Month == 11 then b = "November"
	elseif Month == 12 then b = "December"	end
	
	love.graphics.print(a.." "..b..", Year "..Year, x,y)
end

--function to create (currently) full description of citizens
function Citizen_Description(citizen, id)
	
	local result_string
	
	if citizen.Age == "irrelevant" then
		result_string = sex[citizen.Sex]..", "..jobs[citizen.Job]..", "
						..names[citizen.Engagement].." follower"
	else
		result_string = citizen.Age.." years old "
						..sex[citizen.Sex]..", "..jobs[citizen.Job]..", "
						..names[citizen.Engagement].." follower"
	end
	
	return result_string
			
end