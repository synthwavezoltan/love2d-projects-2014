--called when structures for color (in Field) and other properties (in Properties) are made
--therefore these functions "just" change these values to make it look like a proper city
function CityGen_Main()

	--generating population
	for j=1,SizeX do
		for i=1, SizeY do
			for k=1, math.random(50,150) do
				table.insert(Properties[j][i].Population,
							{ Sex = math.random(1,#sex), 
							Age = math.random(14, 99) , 
							Job = math.random(1,#jobs), 
							Engagement = math.random(1,#names) } )
							
				Citizen_Setter(Properties[j][i].Population[k])						
			end
		end		
	end

	
	for j=1,SizeX do
		for i=1, SizeY do
			for k=1, #Properties[j][i].Population do
				

			end
		end		
	end
	
end


--specialization for sex, job and the other shit
--doesn't include random distribution yet, sorry 
	
function Citizen_Setter(citizen)
	
	--basic setting
	Citizen_SetProperties(citizen)
	
	--JOB SPECIALIZATION
	--14-24 years old can be only students
	if citizen.Age < 19 then citizen.Job = 2 
	elseif citizen.Age >= 19 and citizen.Age < 25 then citizen.Job = 3
	
	--above 65: only retired
	elseif citizen.Age > 65 then citizen.Job = 4 end
	
	--SEX SPECIALIZATION
	--for metahuman genders, age isn't relevant
	if citizen.Sex > 3 then
		citizen.Age = "irrelevant"				
	end
	
	--transhumans follow only Moebius
	if citizen.Sex == 4 then
		citizen.Engagement = 11
		
	--Cloudans can't follow their enemy fraction, Golddiggers
	--also, they obviously can't join neither the Moebius nor the Android Nation
	elseif citizen.Sex == 5 then
		if citizen.Engagement == 7 or citizen.Engagement > 10 then
			while citizen.Engagement == 7 or citizen.Engagement > 10 do
				citizen.Engagement = math.random(1,#names)
			end
		end
		
	--robots follow only Android Nation
	elseif citizen.Sex == 6 then
		citizen.Engagement = 12
		
	-- trangenders can't follow fraction in which it's punished
	-- these are: Grand Church, Red Fist, Justice Empire, Jade Legion, and the Khali
	elseif citizen.Sex == 3 then
		if (citizen.Engagement > 2 
		and citizen.Engagement < 7)
		or citizen.Engagement == 8 then
			while (citizen.Engagement > 2 
			and citizen.Engagement < 7)
			or citizen.Engagement == 8 do
					citizen.Engagement = math.random(1,#names)
			end
		end
	end
end

--generating properties according to the distribution rules
function Citizen_SetProperties(citizen)

	local randomvar = math.random(1,100)
	
	--sex
	if randomvar < Distribution_Sex[1] then
		citizen.Sex = 1
	elseif randomvar >= Distribution_Sex[1] and randomvar < Sum(Distribution_Sex, 2) then
		citizen.Sex = 2		
	elseif randomvar >= Sum(Distribution_Sex, 2) and randomvar < Sum(Distribution_Sex, 3) then
		citizen.Sex = 3
	elseif randomvar >= Sum(Distribution_Sex, 3) and randomvar < Sum(Distribution_Sex, 4) then
		citizen.Sex = 4		
	elseif randomvar >= Sum(Distribution_Sex, 4) and randomvar < Sum(Distribution_Sex, 5) then
		citizen.Sex = 5
	elseif randomvar >= Sum(Distribution_Sex, 5) then		
		citizen.Sex = 6		
	end
	
	--age
	randomvar = math.random(1,100)
	
	if randomvar < Distribution_Age[1] then
		citizen.Age = math.random(14,19)
	elseif randomvar >= Distribution_Age[1] and randomvar < Sum(Distribution_Age, 2) then
		citizen.Age = math.random(20, 29)		
	elseif randomvar >= Sum(Distribution_Age, 2) and randomvar < Sum(Distribution_Age, 3) then
		citizen.Age = math.random(30, 39)		
	elseif randomvar >= Sum(Distribution_Age, 3) and randomvar < Sum(Distribution_Age, 4) then
		citizen.Age = math.random(40, 49)				
	elseif randomvar >= Sum(Distribution_Age, 4) and randomvar < Sum(Distribution_Age, 5) then
		citizen.Age = math.random(50, 59)		
	elseif randomvar >= Sum(Distribution_Age, 5) then		
		citizen.Age = math.random(60, 99)				
	end	
end



function StatCounter(x, y, category, value)
	
	local counter = 0
	
	for i=1, #Properties[x][y].Population do
		if category == Sex then
			if Properties[x][y].Population[i].Sex == value then counter = counter + 1 end
		end	
	
		if category == Age then
			if Properties[x][y].Population[i].Age == value then counter = counter + 1 end
		end
		
		if category == Engagement then
			if Properties[x][y].Population[i].Engagement == value then counter = counter + 1 end	
		end
	end
	
	return counter
end