lastX, lastY = 0, 0

--events of a round
function Round()
	
	
	-- a round runs when the player attempts to occupy a building
	-- it can happen only if that building is adjacent to an occupied one
	if Field[MouseTileX][MouseTileY] ~= PlayerColor and CountNeighbours(MouseTileX, MouseTileY, PlayerColor, Field) > 0 then
	
		--player occupies a tile
		Field[MouseTileX][MouseTileY] = PlayerColor

		--storing most recently conquered tile
		lastX, lastY = MouseTileX, MouseTileY
		
		--[[sometimes this occupation doesn't happen despite clicking on a F@KIN VALID TILE!
			I had no idea what caused it, so instead I found a simpler method to prevent it
			all these f@ckin changes can happen ONLY if aforementioned tile is occupied
			it looks safe since we are in a condition that is true only if the tile is VALID!!!]]
		
		if Field[lastX][lastY] == PlayerColor then

			--change tiles to certain colors if directly surrounded by the same color
			for j=1,SizeX do
				for i=1, SizeY do
					if rounds > 1 and CountNeighbours(j,i, GovernmentColor, Field) == 4 then Field[j][i] = GovernmentColor end
					if CountNeighbours(j,i, PlayerColor, Field) == 4 then Field[j][i] = PlayerColor end
				end
			end	
		
			--computer occupies a tile
			--only if player conquered more than 7 areas
			if CountColors(Field, PlayerColor) >= 7 then
				local x = math.random(1,SizeX)
				local y = math.random(1,SizeY)

				while (x == lastX and y == lastY) or not (Field[x][y] == PlayerColor and CountNeighbours(x, y, GovernmentColor, Field) > 0) do
					x, y = math.random(1,SizeX), math.random(1,SizeY)	
				end

				Field[x][y] = GovernmentColor		
			end



			
			--pass the time away
			--well, formally, since the matter has happened above
			rounds = rounds + 1
			Clock()
		end
	end
	


	
end


--modified version of the time calculator stuff I made in my Moon Property prototype
function Clock()

	Day = Day + 1
	
	if (Month == 2 and Day == 28) 
	or ((Month == 4 or Month == 6 or Month == 9 or Month == 11) and Day == 30) 
	or ((Month == 1 or Month == 3 or Month == 5 or Month == 7 or Month == 8 or Month == 10 or Month == 12) and Day == 31)
		then Month = Month + 1
		Day = 1
	end

	if Month > 12 then Month = 1 Year = Year + 1 end 

end