require 'texts'

-- the main menu; put here because it
-- has overgrown on the size of being
-- able to store it in man.lua - of
-- course, this function MUST be
-- called in love.draw(). - K

function MainMenuBehaviour()
	--main menu HUD elements
	love.graphics.draw(Picset[4], 0,BeginningSequence_Y/4+88)		--middle border in main menu
	love.graphics.draw(Picset[4], 400,BeginningSequence_Y/4+88)
	
	if not CreditsMenu then
	love.graphics.draw(Picset[6], 156,BeginningSequence_Y+104) end	--titlepic	

	if BeginningSequence_Y == 0 then
		
		--cursor
		if (not CreditsMenu and MouseY > 400 and MouseY < 445 and MouseX < 768) 
		or (CreditsMenu and MouseY > 400 and MouseY < 445 and MouseX > 192 and MouseX < 576) then
			love.graphics.draw(CursorPic, CursorSet[1+(cursor_changer%2)], math.floor(MouseX/192)*192, math.floor(MouseY/64)*64)
		end
		
		--texts; all of them can be found in misc.lua
		if ChapterSelector then
			for i=1, 3 do
			love.graphics.print("Chapter "..(ChapterSelectorOffset+i-1)..":\n"..Chapters[i+ChapterSelectorOffset], 20+((i-1)*192), 400) 
			end
			
			if ChapterSelectorOffset < ChapterAmount - 3 then 
				love.graphics.print("More", 616, 400) 
			else
				love.graphics.print("Back to\nmain menu", 616, 400) 			
			end	
			
			
		elseif CreditsMenu then
			love.graphics.print("Level editor", 232, 400) 		
			love.graphics.print("Back to\nmain menu", 424, 400) 				
			love.graphics.draw(Picset[5], -10,500)	--Katamori logo
			love.graphics.draw(Picset[7], 268,104)		--game logo
			love.graphics.print(Creators,800-game_clock,50)
			love.graphics.print(AboutInfo,120,540)			
		else
			love.graphics.print(TitleText,156,300)	
				
			love.graphics.print("start game", 30, 400) 
			love.graphics.print("Options", 232, 400) 	
			love.graphics.print("About", 424, 400) 	
			love.graphics.print("Quit game", 616, 400) 			
		end			
			
	else
		--the only text to write ONLY when beginning sequence is running
		love.graphics.print("Katamori Entertainment Presents...",130,1.5*BeginningSequence_Y+600)			
		love.graphics.print("(esc to skip)",630,560)	
	end
end


function IngameHUD_Behaviour()

	love.graphics.draw(Picset[2], 0,88)			--left border
	love.graphics.draw(Picset[3], 648,88)		--right border
	
	--LEFT SIDE
	--chapter, level indicator
	love.graphics.print("Chapt. "..ChapterNumber.."\nLevel "..LevelNumber,13,150) 
	
	--passed time:
	love.graphics.print(Minutes..":"..Seconds,30,255)	
	
	--game progress "bar"; might be removed in the future
	for i=1, 3 do
		for j=1, 5 do
			if (3*(j-1))+ i <= LevelNumber then
				love.graphics.draw(TilesetPic, Tileset[58], i*(TileSize+12) - 20,270+j*(TileSize+12))	
			else
				love.graphics.draw(TilesetPic, Tileset[57], i*(TileSize+12) - 20,270+j*(TileSize+12))					
			end
		end
	end
	
	--RIGHT SIDE
	--showing used abilities
	if Player.Grappling_Hook then love.graphics.draw(TilesetPic, Tileset[59], 675, 150) end		--grappling hook
	if Player.Laser_Activated then  love.graphics.draw(TilesetPic, Tileset[60], 760, 150) end	--laser
	if EditingMode then love.graphics.draw(TilesetPic, Tileset[Selected_Tile], 760, 350) end	--selected tile in editing
	
	--difficulties:		
	--breakable blocks
	if CountColors(Field, 3) ~= 0 then love.graphics.draw(TilesetPic, Tileset[49], 675, 220) end
	
	--mirrors	
	if CountColors(Field, 9) + CountColors(Field, 10) + CountColors(Field, 11) + CountColors(Field, 12) ~= 0 
	then love.graphics.draw(TilesetPic, Tileset[50], 717, 220) end	
	
	--prisms
	if CountColors(Field, 13) + CountColors(Field, 14) + CountColors(Field, 15) + CountColors(Field, 16) ~= 0  
	then love.graphics.draw(TilesetPic, Tileset[51], 760, 220) end
	
	--doors and portals		
	if #Doors ~= 0 then love.graphics.draw(TilesetPic, Tileset[52], 675, 250) end	
	if #Portals ~= 0 then love.graphics.draw(TilesetPic, Tileset[53], 760, 250) end	
	
	--level text
	love.graphics.print(Texts(LevelNumber),675, 290)		
end