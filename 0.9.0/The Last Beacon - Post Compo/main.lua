require 'keybinding'
require 'physics'
require 'abilities'
require 'misc'
require 'load_n_save'
require 'gui'
require 'texts'
require 'Maps/mapmainmenu'
CallChapterZero()

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 24
	TilesetX, TilesetY = TileSize*8, TileSize*16
	SizeX, SizeY = 21, 21
	
	--on-screen offset of playing area
	OffsX, OffsY = 144,88
	
	--variables
	x,y,z,i,j,k = 0,0,1,0,0,0
	game_clock = 0
	
	--mouse stuff
	MouseX, MouseY, MouseTileX, MouseTileY = 0,0,1,1
	
	--remember!!!
	ExitTile = 12
	asd = 0	
	--game variables
	Minutes, Seconds = 0,0
	PlayerLegTile_X, PlayerLegTile_Y = 0,0
	
	--editing mode properties
	EditingMode = false
	Selected_Tile = 1

	--main menu properties
	cursor_changer = 0
	ChapterSelector = false
	ChapterSelectorOffset = 0
	CreditsMenu = false
	
	Victoire = false
	EndingSequence = false	
	ChapterNumber = 0
	LevelNumber = 0
	ChapterAmount = 8				--full number of chapters
	ChapterSize = 15				--number of maps within a chapter
	BeginningSequence_Y = -600
	VictorySequence	= 0
	
	--sounds
	menu = love.audio.newSource("menu.wav", "static")	
	laser = love.audio.newSource("laser.wav", "static")		
	--initializing font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)	
	
	--initializing 16-px tiles
	TilesetPic = love.graphics.newImage('TILESET_24.png')
	Tileset = { }
	
	for j=1, 8 do
		for i=1, 8 do 
			Tileset[((i-1)*8)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--initializing player sprites
	PlayerSprites = love.graphics.newImage('PLAYER.png')
	PlayerCharset = { }
	
	for i=1, 5 do 
		PlayerCharset[i] = love.graphics.newQuad((i-1)*24, 0, 24, 24, 120, 24)
	end	
	
	--initialiing autotiles
	AutoPic = love.graphics.newImage('AUTOTILE.png')
	Autoset = { }
	
	for j=1, 4 do
		for i=1, 4 do 
			Autoset[((i-1)*4)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TileSize*4, TileSize*4)
		end
	end		
	
	--cursor
	CursorPic = love.graphics.newImage('CURSOR.png')
	CursorSet = { }
	
	for i=1, 2 do 
		CursorSet[i] = love.graphics.newQuad(0, (i-1)*64, 192, 64, 192, 128)
	end	
	
	--initializing pics
	Picset = {
	love.graphics.newImage('SKY0.png'),
	love.graphics.newImage('BORDER_LEFT.png'),	
	love.graphics.newImage('BORDER_RIGHT.png'),	
	love.graphics.newImage('BORDER_EMPTY.png'),	
	love.graphics.newImage('KATAMORI.png'),	
	love.graphics.newImage('TITLEPIC.png'),	
	love.graphics.newImage('LOGO_3.png'),		
	}
	
	LoadMap()
	Loadmainmenu()
	
	--redeclaring Player here; just to know about everything
	Player = { 
	X=72, Y=72,
	TileX=3, TileY=3,
	Sprite = 1, 
	Grappling_Hook = false, Laser_Activated = false, 
	Laser_Dir = 'no',}		
end

function love.draw()
	
	--GAME_CLOCK IS HERE, IT'S FUCKIN IMPORTANT!!!!
	game_clock = game_clock+1

	for j=1,SizeX do
		for i=1, SizeY do

			
			if (Victoire and SizeY-i>=VictorySequence) or not Victoire then
				--drawing field itself			
				if Field[j][i] ~= 0 then love.graphics.draw(TilesetPic, Tileset[Field[j][i]], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize) end
			
				--drawing Laser table
				if Laser[j][i] ~= 0 then love.graphics.draw(TilesetPic, Tileset[Laser[j][i]], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize) end
			end

			--damagecounter for debugging
			--love.graphics.print(DamageCounter[j][i], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize)
			
			--fixing Laser table if necessary
			if not Player.Grappling_Hook 									--if the hook ability is disabled
			or 
			(Player.Grappling_Hook 											-- OR enabled, but graphics appear below player
			and j == Player.TileX
			and i >= Player.TileY)
			then	
				if Laser[j][i] == 63 then Laser[j][i] = 0 end				--delete hook graphics 
			end
			
			if not Player.Laser_Activated then								--if the laser ability is disabled
				if Laser[j][i] == 61 or Laser[j][i] == 62
				then Laser[j][i] = 0 end									--delete laser graphics
			else															--if it's enabled then
				if  j>1 and i>1 and j<SizeX-1 and i<SizeY-1
				and not Solidchecker(Field[j][i])
				and 
				Laser[j][i] == 62 and (Laser[j+1][i] == 61 or Laser[j-1][i] == 61)
				or 
				Laser[j][i] == 61 and (Laser[j][i+1] == 62 or Laser[j][i-1] == 62)
				 then		--make crosses
					Laser[j][i] = 64

				end
			end
			
			--behaviour and drawing related functions; I'd rather call them here than in update()
			Mirror_Behaviour(j,i)			--doing the mirror behaviour			
			Prism_Behaviour(j,i)			--doing the prism behaviour	
			Damaging(j,i,18)				--doing the laser damaging stuff
			Animating(j,i)					--animating certain textures
		end
	end	
	
	if not CreditsMenu then
	love.graphics.draw(Picset[1], 0,BeginningSequence_Y/4) end			--sky
	
	--ending
	if game_clock%20000 >300 and Victoire and LevelNumber == 15  then
			love.graphics.print(Chapter_Zero_Ending,OffsX+16,650-(VictorySequence*TileSize))			
	end	
	

	if LevelNumber == 0 then
		--draw happens in main menu
		MainMenuBehaviour()
		
	else
		--draw happens ingame		
		--constant tiles
		Field[20][19] = 1		
		Field[ExitTile][1] = 2	

		--making the HUD behaviour
		IngameHUD_Behaviour()
				
		--player drawing
		if SizeY-Player.TileY>=VictorySequence then
			love.graphics.draw(PlayerSprites, PlayerCharset[Player.Sprite], OffsX+((Player.TileX-1)*TileSize), OffsY+((Player.TileY-1)*TileSize))
		end
		
		--for the case of victory	
		if Victoire then
			for i=1, 4 do
				love.graphics.draw(TilesetPic, Tileset[62], OffsX + ((ExitTile-1)*TileSize), OffsY-(i*TileSize))
			end
			Laser[ExitTile][1] = 62
			Laser[ExitTile][2] = 62	

		else
			--in any other cases	
		end	
	end	
			--autotiling
			Autotiling()
end

function love.update(dt)

	--scripts and similar shit
	Grappling_Hook_Drawing()
	Laser_Beam_Drawing(Player.TileX, Player.TileY, Player.Laser_Dir)
	
	--player's coordinates	
	Player.X = OffsX + (Player.TileX*TileSize)
	Player.Y = OffsY + (Player.TileY*TileSize)
	
	--mouse constants
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	MouseTileX, MouseTileY = 1+math.floor((MouseX-OffsX)/TileSize), 1+math.floor((MouseY-OffsY)/TileSize)	
	
	--victory script
	if Laser[ExitTile][3] == 62 then Victoire = true else Victoire = false end

	--editing mode 
	if love.mouse.isDown( "l"  ) and EditingMode then Field[MouseTileX][MouseTileY] = Selected_Tile end
	
	-- door + portal drawing and behaviour
	if not love.mouse.isDown( "l"  ) and not EditingMode then
		The_Doors()	
		Portal_Behav()
	end
						
	--beginning sequence	
	if BeginningSequence_Y ~= 0 then 
		BeginningSequence_Y = BeginningSequence_Y + 1
	else
		if asd == 0 then love.audio.play(menu) asd = asd+1 end
	end
	
	--victory sequence
	if Victoire and game_clock%10 == 0 
	then VictoryFoo() end
	
	--timer
	--game_clock is iterated in love.draw!!!!!
	
	--actual clock
	if not Victoire then
		if game_clock%60 == 0 then Seconds = Seconds + 1 end	
		if Seconds == 60 then Minutes = Minutes + 1 Seconds = 0 end
	end
	
	--gravity script	
	if game_clock%10 == 0 then 
		if not Player.Grappling_Hook then
			Gravity() 
		end
	end
	
	--player animation
	if game_clock%15 == 0 then 
		if Player.Laser_Activated then 
			if Player.Sprite == 1 or Player.Sprite == 5 then 
				Player.Sprite = 3
			elseif Player.Sprite == 3 or Player.Sprite == 4 then 
				Player.Sprite = Player.Sprite + 1
			end
		end
	end
	
	--cursor anim
	if LevelNumber == 0 then
		if game_clock%10 == 0 then
			cursor_changer = cursor_changer + 1
		end
	end
	
	--sound effects
	if Player.Laser_Activated then 
		love.audio.play(laser)
		if not laser:isStopped() then
			if game_clock%45 ==0 then
				laser:rewind()	
			end
		end	
	end	
	
end

function HasNeighbour(x,y, valueToCheck, Table)

	local itHas = false

	if Table[x-1][y] == valueToCheck then itHas = true
	elseif Table[x+1][y] == valueToCheck then itHas = true
	elseif Table[x][y-1] == valueToCheck then itHas = true
	elseif Table[x][y+1] == valueToCheck then itHas = true
	else itHas = false end
	
	return itHas
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,SizeX-1 do
		for i=2, SizeY-1 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end
