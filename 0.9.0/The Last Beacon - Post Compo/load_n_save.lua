function LoadMap()

	Minutes, Seconds = 0,0
	game_clock = 0
	VictorySequence=0
	
	--redefining EVERY SETS - no, I won't even have a single fucking chance for the maploader to be fucked up!!! 
	Field = { }	for j=1,SizeX do Field[j] = { }	for i=1, SizeY do Field[j][i] = 2 end end
	Laser = { }	for j=1,SizeX do Laser[j] = { }	for i=1, SizeY do Laser[j][i] = 0 end end	
	DamageCounter = { }	for j=1,SizeX do DamageCounter[j] = { }	for i=1, SizeY do DamageCounter[j][i] = 0 end end

	--redefining itemsets
	Player = { 	X=72, Y=72, TileX=3, TileY=3, Sprite = 1, Grappling_Hook = false, Laser_Activated = false, Laser_Dir = 'no',}	
	Doors = {} 
	Portals = {}

	NumLoad(LevelNumber)

end

function SaveMap(mapnum)
	bracket = "{"
	backbracket = "}"
	
	MapFile = love.filesystem.newFile("map" .. mapnum ..".lua")
	asd = MapFile:open("w")
	
	--beginning
	MapFile:write("function Load".. mapnum .."()\r\n \r\n")	
	
	--player
	MapFile:write("Player="..bracket)
	MapFile:write("TileX="..Player.TileX..",TileY="..Player.TileY..",Sprite=1,Grappling_Hook=false,Laser_Activated=false,Laser_Dir='no'")
	MapFile:write(backbracket.."\r\n")

	--doors
	for i=1, #Doors do
	MapFile:write("Doors["..i.."]="..bracket)	
	MapFile:write("X="..Doors[i].X..",Y="..Doors[i].Y..",Solve_X="..Doors[i].Solve_X..",Solve_Y="..Doors[i].Solve_Y..",Sprite="..Doors[i].Sprite)	
	MapFile:write(backbracket.."\r\n")	
	end
	
	--portals
	for i=1, #Portals do
	MapFile:write("Portals["..i.."]="..bracket)	
	MapFile:write("X="..Portals[i].X..",Y="..Portals[i].Y..",PairID="..Portals[i].PairID..",Sender=false")	
	MapFile:write(backbracket.."\r\n")	
	end
	
	--Field itself to the end
	MapFile:write("Field=" .. bracket)
	
	for j=1, SizeX do	
		MapFile:write(bracket)		
		for i=1, SizeY do
			--if something that should be put down from the special tables
			--just write a "2" there
			if (Field[j][i] >= 19 and Field[j][i] <= 22)
			or Field[j][i] == 25 or Field[j][i] == 26 then
				MapFile:write("2,")
			else
				MapFile:write(Field[j][i] .. ",")				
			end
		end	
		
		if j == SizeX
		then
			MapFile:write(backbracket .. ",\r\n")
		else
			MapFile:write(backbracket .. ",\r\n")
		end
	end	
	MapFile:write( "\r\n" ..backbracket.."\r\n\r\n")
	

	--end	
	MapFile:write( "\r\n end")		
	MapFile:close( )	

end

function NextLevel()
	LevelNumber = LevelNumber + 1
	
	if LevelNumber == 1 then Load1() end
end





	--NOTE: it was the original beginning of love.load()
	-- I kept it for historical and sampling reasons. - K

function LoadTest()

	--uploading field
	Field = { }	
	for j=1,SizeX do	
		Field[j] = { }	
		for i=1, SizeY do
			Field[j][i] = 2
		end		
	end
	
	--an additional table for Laser
	Laser = { }	
	for j=1,SizeX do	
		Laser[j] = { }	
		for i=1, SizeY do
			Laser[j][i] = 63
		end		
	end	
	
	--and an additional one for damaging
	DamageCounter = { }	
	for j=1,SizeX do	
		DamageCounter[j] = { }	
		for i=1, SizeY do
			DamageCounter[j][i] = 0
		end		
	end	

	Field[ExitTile][2] = 2	
	Field[ExitTile][2] = 17
	Field[ExitTile][20] = 11
	
	--defining Player
	Player = {
	TileX = 20,
	TileY = 18,
	Sprite = 1,
	Grappling_Hook = false,
	Laser_Activated = false,
	Laser_Dir = 'no',
	}

	--defining doors, sample
	Doors = { }
	
	for i=1, 5 do
		Doors[i] = { X = i*2, Y = 20, Solve_X = 12+i, Solve_Y = 19, Sprite = 21 }
	end	

	-- defining portals, sample
	Portals = {}
	
	Portals[1] = { X = 5, Y = 16, PairID = 2, Sender = false, }
	Portals[2] = { X = ExitTile, Y = 10, PairID = 1, Sender = false, }		
	Portals[3] = { X = 5, Y = 2, PairID = 4, Sender = false, }
	Portals[4] = { X = ExitTile, Y = 5, PairID = 3, Sender = false, }	
	
end
