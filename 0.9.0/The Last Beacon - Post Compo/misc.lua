function Autotiling()

	for j=2,SizeX-1 do
		for i=2, SizeY-1 do
			if Field[j][i] ~= 1 
			and 
			(Field[j][i] ~= 30 and Field[j][i] ~= 31 and Field[j][i] ~= 32 
			and Field[j][i] ~= 54 and Field[j][i] ~= 55 and Field[j][i] ~= 56
			
			and Field[j][i] ~= 3 and Field[j][i] ~= 4 and Field[j][i] ~= 5 and Field[j][i] ~= 6 and Field[j][i] ~= 7 )
			and
			((Victoire and SizeY-i>=VictorySequence) or not Victoire)
			then
				--love.graphics.draw(TilesetPic, Tileset[Field[j][i]], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize)	
				
				if Field[j-1][i] == 1 or Field[j-1][i] == 30 or Field[j-1][i] == 39 or Field[j-1][i] == 31 or Field[j-1][i] == 32 
				or Field[j-1][i] == 54 or Field[j-1][i] == 55 or Field[j-1][i] == 56 then
					love.graphics.draw(AutoPic, Autoset[4], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize)	
				end
				if Field[j+1][i] == 1 or Field[j+1][i] == 30  or Field[j+1][i] == 39 or Field[j+1][i] == 31 or Field[j+1][i] == 32 
				or Field[j+1][i] == 54 or Field[j+1][i] == 55 or Field[j+1][i] == 56 then
					love.graphics.draw(AutoPic, Autoset[3], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize)	
				end
				if Field[j][i-1] == 1 or Field[j][i-1] == 30 or Field[j][i-1] == 39 or Field[j][i-1] == 31 or Field[j][i-1] == 32 
				or Field[j][i-1] == 54 or Field[j][i-1] == 55 or Field[j][i-1] == 56 then
					love.graphics.draw(AutoPic, Autoset[2], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize)	
				end
				if Field[j][i+1] == 1 or Field[j][i+1] == 30 or Field[j][i+1] == 39 or Field[j][i+1] == 31 or Field[j][i+1] == 32 
				or Field[j][i+1] == 54 or Field[j][i+1] == 55 or Field[j][i+1] == 56 then
					love.graphics.draw(AutoPic, Autoset[1], OffsX+(j-1)*TileSize, OffsY+(i-1)*TileSize)	
				end
								
				

				
			end
		end
	end

end

--function for texture animations
function Animating(j,i)

			--animated mark of the aim to reach with your laser	
			if LevelNumber ~= 0 then
				if game_clock%50 < 25 then 
					Field[ExitTile][2] = 17 
					else Field[ExitTile][2] = 18 
				end
			end


end

--what should happen when you win
function VictoryFoo()
	if VictorySequence <= SizeX then 
		VictorySequence = VictorySequence +1
	else 
		if LevelNumber ~= 15 then 
			VictorySequence=0 
			LevelNumber = LevelNumber+1
			Victoire = false
			LoadMap()	
		end		
	end
		

end

function NumLoad(input)

	if input == 1 then Load1()
	elseif input == 2 then Load2()
	elseif input == 3 then Load3()
	elseif input == 4 then Load4()
	elseif input == 5 then Load5()
	elseif input == 6 then Load6() 
	elseif input == 7 then Load7()
	elseif input == 8 then Load8()
	elseif input == 9 then Load9() 
	elseif input == 10 then Load10()
	elseif input == 11 then Load11()
	elseif input == 12 then Load12()
	elseif input == 13 then Load13()
	elseif input == 14 then Load14() 
	elseif input == 15 then Load15()
	elseif input == 16 then EndingSequnce = true
	end

	
end
		
function AutoDetailer()

	for i=1, 5 do
		x = math.random(2, SizeX-1)
		y = math.random(2, SizeY-1)
		
		if Field[x][y] == 1 then Field[x][y] = 29 end			
		if Field[x][y] == 2 then Field[x][y] = 29 end		
		if Field[x][y] == 3 then Field[x][y] = math.random (3,7) end
		
		if Field[x][y] == 39 then 
		
			if math.random(100)%2 ==0 then
				if not Solidchecker(Field[x-1][y]) then Field[x-1][y] = 29 end
				if not Solidchecker(Field[x+1][y]) then Field[x+1][y] = 29 end
				if not Solidchecker(Field[x][y-1]) then Field[x][y-1] = 29 end
				if not Solidchecker(Field[x][y+1]) then Field[x][y+1] = 29 end
			else
				if Solidchecker(Field[x][y+1]) and Field[x][y+1]>8 then Field[x-1][y] = 48 end
			end
		end	
		
		if not Field[x][y] == 29 then Field[x][y] = 40 end	


	end

end

function Player_Laser_Turnoff()
	Player.Laser_Activated = false
	Player.Laser_Dir = 'no'
	Player.Sprite = 1	
end

function CallChapterZero()
	require 'Maps/chapter0/map1'
	require 'Maps/chapter0/map2'
	require 'Maps/chapter0/map3'
	require 'Maps/chapter0/map4'
	require 'Maps/chapter0/map5'
	require 'Maps/chapter0/map6'
	require 'Maps/chapter0/map7'
	require 'Maps/chapter0/map8'
	require 'Maps/chapter0/map9'
	require 'Maps/chapter0/map10'
	require 'Maps/chapter0/map11'
	require 'Maps/chapter0/map12'
	require 'Maps/chapter0/map13'
	require 'Maps/chapter0/map14'
	require 'Maps/chapter0/map15'
end		