SolidTiles = 
{1, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 
18, 21, 23, 24, 25, 26, 30, 31, 32, 39, 54, 55, 56 }

function Solidchecker(Tile_Number)

	local isSolid = false
	
	for i=1, #SolidTiles do
		if Tile_Number == SolidTiles[i] then
			isSolid = true
			break
		else
			isSolid = false
		end
	end
	
	return isSolid
end

function Gravity()
	if not Solidchecker(Field[Player.TileX][Player.TileY+1]) then
		Player.TileY = Player.TileY +1
		if Player.Laser_Activated then Player.Laser_Activated = false end
	end
end

--for tile 3-7; call this little boy in love.draw, at the check of each tiles
function Damaging(x,y, Agility)

	--doesn't happen if we talk about unbreakable tiles.
	--thank god.
	if Field[x][y] >= 3 and Field[x][y] <= 7 then
		if Laser[x+1][y] == 61 or Laser[x-1][y] == 61 or Laser[x][y+1] == 62 or Laser[x][y-1] == 62 then
			DamageCounter[x][y] = DamageCounter[x][y] + 1 
		end
		
		if (Field[x][y] == 3 and DamageCounter[x][y] == Agility) 
		or (Field[x][y] == 4 and DamageCounter[x][y] == 2*Agility)
		or (Field[x][y] == 5 and DamageCounter[x][y] == 3*Agility)
		or (Field[x][y] == 6 and DamageCounter[x][y] == 4*Agility)
		or (Field[x][y] == 7 and DamageCounter[x][y] == 5*Agility)		
		then
			Field[x][y] = Field[x][y] +1
		end
		
	elseif Field[x][y] == 8 and DamageCounter[x][y] ~= 0 then 
			DamageCounter[x][y] = 0	
	end


end

--for tile 9-12: mirrors
function Mirror_Behaviour(x,y)
	
	--left-down mirror
	if Field[x][y] == 9 then
		if Laser[x-1][y] == 61 and not Solidchecker(Field[x][y+1]) and not (Player.TileX == x and Player.TileY > y) then
			Laser_Beam_Drawing(x, y, 'down')
		end
		if Laser[x][y+1] == 62 and not Solidchecker(Field[x-1][y]) and not (Player.TileX < x and Player.TileY == y) then
			Laser_Beam_Drawing(x, y, 'left')		
		end
	--left-up
	elseif Field[x][y] == 10 then
		if Laser[x-1][y] == 61 and not Solidchecker(Field[x][y-1]) and not (Player.TileX == x and Player.TileY < y) then
			Laser_Beam_Drawing(x, y, 'up')
		end
		if Laser[x][y-1] == 62 and not Solidchecker(Field[x-1][y]) and not (Player.TileX < x and Player.TileY == y) then
			Laser_Beam_Drawing(x, y, 'left')		
		end	
	--up-right
	elseif Field[x][y] == 11 then
		if Laser[x+1][y] == 61 and not Solidchecker(Field[x][y-1]) and not (Player.TileX == x and Player.TileY < y) then
			Laser_Beam_Drawing(x, y, 'up')
		end
		if Laser[x][y-1] == 62 and not Solidchecker(Field[x+1][y]) and not (Player.TileX > x and Player.TileY == y) then
			Laser_Beam_Drawing(x, y, 'right')		
		end		
	--right-down
	elseif Field[x][y] == 12 then
		if Laser[x+1][y] == 61 and not Solidchecker(Field[x][y+1]) and not (Player.TileX == x and Player.TileY > y) then
			Laser_Beam_Drawing(x, y, 'down')
		end
		if Laser[x][y+1] == 62 and not Solidchecker(Field[x+1][y]) and not (Player.TileX > x and Player.TileY == y) then
			Laser_Beam_Drawing(x, y, 'right')		
		end
	end
end


-- tile 13-16: prisms
function Prism_Behaviour(x,y)
	
	--down-input, up-input
	if (Field[x][y] == 13 and Laser[x][y+1] == 62) or (Field[x][y] == 15 and Laser[x][y-1] == 62) then
		if not Solidchecker(Field[x-1][y]) then 
			Laser_Beam_Drawing(x, y, 'left')		
		end
		
		if not Solidchecker(Field[x+1][y]) then 
			Laser_Beam_Drawing(x, y, 'right')			
		end		
	--left-input, right-input
	elseif (Field[x][y] == 14 and Laser[x-1][y] == 61) or (Field[x][y] == 16 and Laser[x+1][y] == 61) then
		if not Solidchecker(Field[x][y-1]) then 
			Laser_Beam_Drawing(x, y, 'up')		
		end
		
		if not Solidchecker(Field[x][y+1]) then 
			Laser_Beam_Drawing(x, y, 'down')		
		end	
	end

end

-- tile 19-22: solving doors
function The_Doors()
	for i=1, #Doors do
		--detecting
		if Player.TileX == Doors[i].Solve_X and Player.TileY == Doors[i].Solve_Y + 1 then			
			Doors[i].Sprite = 22
		else
			Doors[i].Sprite = 21
		end	
		--drawing
		Field[Doors[i].X][Doors[i].Y] = Doors[i].Sprite		
		Field[Doors[i].Solve_X][Doors[i].Solve_Y ] = Doors[i].Sprite - 2

	end
end

-- tile 25-26: portals
function Portal_Behav()
	--drawing
	for i=1, #Portals do
		if game_clock%40 < 20 then Field[Portals[i].X][Portals[i].Y] = 25
		else Field[Portals[i].X][Portals[i].Y] = 26 end

		if not Portals[Portals[i].PairID].Sender then
			--teleporting laser away		
			if Laser[Portals[i].X+1][Portals[i].Y] == 61 then
				Laser_Beam_Drawing(Portals[Portals[i].PairID].X, Portals[Portals[i].PairID].Y, 'left')
				Portals[Portals[i].PairID].Sender = false
				Portals[i].Sender = true
			elseif Laser[Portals[i].X-1][Portals[i].Y] == 61 then
				Laser_Beam_Drawing(Portals[Portals[i].PairID].X, Portals[Portals[i].PairID].Y, 'right')	
				Portals[Portals[i].PairID].Sender = false
				Portals[i].Sender = true
			elseif Laser[Portals[i].X][Portals[i].Y-1] == 62 then
				Laser_Beam_Drawing(Portals[Portals[i].PairID].X , Portals[Portals[i].PairID].Y, 'down')	
				Portals[Portals[i].PairID].Sender = false
				Portals[i].Sender = true
			elseif Laser[Portals[i].X][Portals[i].Y+1] == 62 then
				Laser_Beam_Drawing(Portals[Portals[i].PairID].X , Portals[Portals[i].PairID].Y , 'up')	
				Portals[Portals[i].PairID].Sender = false
				Portals[i].Sender = true
			else
				Portals[i].Sender = false		
			end
		end
		
	end	
end



