function Load15()
 
Player={X=408,Y=480,TileX=17,TileY=20,Sprite=1,Grappling_Hook=false,Laser_Activated=false,Laser_Dir='no'}
Doors[1]={X=12,Y=12,Solve_X=4,Solve_Y=3,Sprite=21}
Portals[1]={X=17,Y=18,PairID=2,Sender=false}
Portals[2]={X=4,Y=20,PairID=1,Sender=false}
Portals[3]={X=12,Y=15,PairID=4,Sender=false}
Portals[4]={X=19,Y=2,PairID=3,Sender=false}
Portals[5]={X=2,Y=2,PairID=6,Sender=false}
Portals[6]={X=2,Y=20,PairID=5,Sender=false}
Portals[7]={X=2,Y=5,PairID=8,Sender=false}
Portals[8]={X=3,Y=20,PairID=7,Sender=false}
Portals[9]={X=3,Y=5,PairID=10,Sender=false}
Portals[10]={X=17,Y=15,PairID=9,Sender=false}
Portals[11]={X=17,Y=2,PairID=12,Sender=false}
Portals[12]={X=7,Y=15,PairID=11,Sender=false}
Portals[13]={X=7,Y=2,PairID=14,Sender=false}
Portals[14]={X=16,Y=15,PairID=13,Sender=false}
Portals[15]={X=16,Y=7,PairID=16,Sender=false}
Portals[16]={X=8,Y=15,PairID=15,Sender=false}
Portals[17]={X=8,Y=7,PairID=18,Sender=false}
Portals[18]={X=15,Y=15,PairID=17,Sender=false}
Portals[19]={X=15,Y=11,PairID=20,Sender=false}
Portals[20]={X=9,Y=15,PairID=19,Sender=false}
Portals[21]={X=9,Y=11,PairID=22,Sender=false}
Portals[22]={X=14,Y=15,PairID=21,Sender=false}
Portals[23]={X=14,Y=13,PairID=24,Sender=false}
Portals[24]={X=10,Y=15,PairID=23,Sender=false}
Portals[25]={X=10,Y=13,PairID=26,Sender=false}
Portals[26]={X=20,Y=14,PairID=25,Sender=false}
Portals[27]={X=20,Y=2,PairID=28,Sender=false}
Portals[28]={X=19,Y=14,PairID=27,Sender=false}
Field={{1,56,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,31,},
{1,2,2,16,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,31,},
{1,2,29,29,2,2,29,29,2,2,29,2,2,2,2,2,2,2,2,2,31,},
{1,29,2,40,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,31,},
{1,2,40,40,40,3,3,3,3,3,3,3,3,3,3,40,29,11,2,2,31,},
{1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,40,40,2,2,31,},
{1,2,27,27,27,2,27,27,27,2,27,27,27,27,2,3,40,9,29,10,31,},
{1,56,55,55,55,54,2,27,27,27,27,27,27,27,2,3,4,4,2,2,31,},
{1,1,56,55,55,55,55,55,55,54,2,27,27,27,2,3,40,29,29,2,31,},
{1,3,56,55,55,55,55,55,55,55,55,54,2,29,2,3,40,29,29,2,31,},
{1,2,3,56,55,55,55,55,55,55,55,55,55,54,29,3,4,4,4,2,31,},
{2,17,2,3,3,3,3,3,3,3,3,2,29,29,2,3,40,40,29,2,31,},
{1,2,3,56,55,55,55,55,55,55,55,55,55,54,29,3,4,4,1,1,31,},
{1,3,56,55,55,55,55,55,55,55,55,54,2,29,2,3,40,40,29,2,31,},
{1,1,56,55,55,55,55,55,55,54,2,27,27,27,2,3,40,29,4,4,31,},
{1,56,55,55,55,54,2,27,27,27,27,27,27,27,2,3,40,40,4,2,31,},
{1,2,27,27,27,27,2,27,27,2,27,27,27,27,2,3,40,2,4,2,31,},
{1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,1,1,31,},
{1,2,40,2,40,40,40,2,40,40,2,40,2,2,1,1,1,1,1,1,31,},
{1,2,40,40,40,2,40,40,40,2,40,2,40,2,1,1,1,1,1,39,31,},
{1,56,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,55,31,},

}


 end