--those that pressing once is enough
function love.keypressed(key)
	--let key actions only when a level is loaded and not won yet
	if not Victoire and LevelNumber ~= 0 and not EndingSequence then

		if key == 'w' then
			if not Solidchecker(Field[Player.TileX][Player.TileY-1]) then 			--if I have place above my head
				if Player.Grappling_Hook then										--if I use the grappling hook
					if not Player.Laser_Activated then								--if I don't use the laser at the same time
						Player.TileY = Player.TileY - 1		
					end
				else
					if not Player.Laser_Activated									--if hook isn't active, but laser neither
					and LevelNumber >1 then											--and I'm not on lvl 1
						Player.Grappling_Hook = true								--activate the ability
					end
				end
			end

		end
		
		if key == 's' then
			if Player.Grappling_Hook 												--if I use the grappling hook 
			and not Player.Laser_Activated 											--but I don't use the laser
			and not Solidchecker(Field[Player.TileX][Player.TileY+1]) then			--and there's nothing below my feet 
				Player.TileY = Player.TileY + 1
			end	
		end

		if key == 'a' then
			if Player.TileX > 0 then 												--condition 1: be within boundaries
				if not Solidchecker(Field[Player.TileX -1][Player.TileY]) then		--condition 2: try to step on a non-solid tile
					if not Player.Grappling_Hook then								--condition 3: do not hang on that rope
						if LevelNumber ~= 0 then									--condition 4: game is started	
							Player_Laser_Turnoff()
							Player.TileX = Player.TileX - 1							
						end
					end
				end
			end	
		end

		if key == 'd' then
			if Player.TileX < SizeX-1 then 
				if not Solidchecker(Field[Player.TileX +1][Player.TileY]) then	
					if not Player.Grappling_Hook then		
						if LevelNumber ~= 0	then						
							Player_Laser_Turnoff()
							Player.TileX = Player.TileX + 1
						end
					end
				end
			end
		end	
	
		--activating laser while not hanging on rope
		--only if you're standing anywhere or you're just hanging
		if Solidchecker(Field[Player.TileX][Player.TileY+1]) 
		or (not Solidchecker(Field[Player.TileX][Player.TileY+1]) and Player.Grappling_Hook) then
		
			if key == 'q' then
				if not Solidchecker(Field[Player.TileX -1][Player.TileY]) then				--condition 1: leave at least a single block for yourself
					if not Player.Laser_Activated then										--condition 2: don't have laser activated in other dir
					
						Player.Laser_Activated = true
						Player.Laser_Dir = 'left'
					else
						Player_Laser_Turnoff()												--turning of in case of pressing that key again				
					end
				end
			end
			
			if key == 'e' then	
				if not Solidchecker(Field[Player.TileX +1][Player.TileY]) then	
					if not Player.Laser_Activated then
						Player.Laser_Activated = true
						Player.Laser_Dir = 'right'	
					else
						Player_Laser_Turnoff()												--turning of in case of pressing that key again		
					end
				end
			end
			
		end
	   
	   --deactivating stuff
	   if key == ' ' then
			--if hanging on rope, and laser is active, then turn off only the laser
			if Player.Grappling_Hook then 
				if Player.Laser_Activated then
					Player.Laser_Activated = false
					Player.Laser_Dir = 'no'
					Player.Sprite = 1
				else
					Player.Grappling_Hook = false 
				end
			else
			--but turn off laser even if its active, but hook isnt
				if Player.Laser_Activated then
					Player.Laser_Activated = false
					Player.Laser_Dir = 'no'
					Player.Sprite = 1
				end		
			end
		end
		
 
	   if key == 't' and EditingMode then
		AutoDetailer()
	   end

	end   
	
		--quick exit	
	   if key == 'escape' then
			BeginningSequence_Y = 0
			if EndingSequence then love.event.push('quit') end
	   end
	   
	   if key == 'x' and LevelNumber == 0 then
			if EditingMode then
				LevelNumber = 15
				LoadMap()
			end
	   end	

		--editing mode SUPAH SAKRET - also can be activated by users with some programming skillz
		if key == 'k' then
			--EditingMode = not EditingMode
		end	   
end

function love.mousepressed(x,y,button)

	--mouse actions for editing mode.
	if EditingMode then
	
		if button == "l" then
			--place tiles
			if LevelNumber ~= 0 then 
				--door door
				if Selected_Tile== 21 then
					table.insert(Doors,{X = MouseTileX, Y = MouseTileY, Solve_X = 3, Solve_Y = 3, Sprite = 21})
					Doors[#Doors] ={ X = MouseTileX, Y = MouseTileY, Solve_X = 3, Solve_Y = 3, Sprite = 21 }
				--door lock
				elseif Selected_Tile== 19 then
					Doors[#Doors].Solve_X = MouseTileX
					Doors[#Doors].Solve_Y = MouseTileY	
				--portal
				elseif Selected_Tile== 25 or Selected_Tile== 26 then
					if #Portals%2==0 then
						table.insert(Portals, {})
						Portals[#Portals] = { X = MouseTileX, Y = MouseTileY, PairID = #Portals+1, Sender = false, }
					elseif #Portals%2==1 then
						table.insert(Portals, {})
						Portals[#Portals] = { X = MouseTileX, Y = MouseTileY, PairID = #Portals-1, Sender = false, }
					end
				--all else
				else
					Field[MouseTileX][MouseTileY] = Selected_Tile
				end
			end
		end	

		
		if button == "r" then SaveMap(LevelNumber)	end
		if button == "wu" and Selected_Tile < 64 then Selected_Tile = Selected_Tile + 1 end
		if button == "wd" and Selected_Tile > 1  then Selected_Tile = Selected_Tile - 1 end
		
	--mouse actions outside editing mode
	else
		if button == "l" then
		
			--main menu
			if LevelNumber == 0 and BeginningSequence_Y == 0 
			and MouseY > 400 and MouseY < 445 then
				if math.floor(MouseX/192) == 0 then 
					if ChapterSelector then
						ChapterNumber = 0
						LevelNumber = 1
						LoadMap()
						Victoire = false
						love.audio.play(menu)				
					else
						ChapterSelectorOffset = 0
						ChapterSelector=true
					end
				elseif math.floor(MouseX/192) == 1 then	
					if ChapterSelector then
						ChapterNumber = 1
						
					elseif CreditsMenu then

					else

					end					
				elseif math.floor(MouseX/192) == 2 then
					if ChapterSelector then
						ChapterNumber = 2
					elseif CreditsMenu then
						CreditsMenu = false
					else
						game_clock = 0
						CreditsMenu = true
					end
				
				elseif math.floor(MouseX/192) == 3 then
					if ChapterSelector then
						if ChapterSelectorOffset < ChapterAmount - 3 then
							ChapterSelectorOffset = ChapterSelectorOffset + 1
						else
							ChapterSelector = false
						end
					else
						love.event.push('quit')
					end
				
				end
			end
		end

	end	
end




