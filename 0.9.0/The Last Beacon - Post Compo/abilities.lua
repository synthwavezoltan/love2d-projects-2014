--into update()

function Grappling_Hook_Drawing()
	if Player.Grappling_Hook and not Solidchecker(Field[Player.TileX][Player.TileY-1]) then
		k = 0
		repeat
			Laser[Player.TileX][Player.TileY-k] = 63		
			k = k+1
		until Solidchecker(Field[Player.TileX][Player.TileY-k])
	end
end

function Laser_Beam_Drawing(sourceX, sourceY, dir)
	if dir == 'left' and not Solidchecker(Field[sourceX-1][sourceY]) then
		k = 1
		repeat
			Laser[sourceX-k][sourceY] = 61		
			k = k+1
		until Solidchecker(Field[sourceX-k][sourceY])	
		
	elseif dir =='right' and not Solidchecker(Field[sourceX+1][sourceY]) then
		k = 1
		repeat
			Laser[sourceX+k][sourceY] = 61		
			k = k+1
		until Solidchecker(Field[sourceX+k][sourceY])	
	
	elseif dir =='up' and not Solidchecker(Field[sourceX][sourceY-1]) then
		k = 1
		repeat
			Laser[sourceX][sourceY-k] = 62		
			k = k+1
		until Solidchecker(Field[sourceX][sourceY-k])	

	elseif dir =='down' and not Solidchecker(Field[sourceX][sourceY+1]) then
		k = 1
		repeat
			Laser[sourceX][sourceY+k] = 62		
			k = k+1
		until Solidchecker(Field[sourceX][sourceY+k])	
	end	
end