-- Texts which are appearing in-game.
-- They are stored here instead of
-- their mapfile, because this way
-- this is clearer for me to read. - K


-- original value of main menu texts
-- I should make these things callable
-- in case of any rewritings in these
-- variables. - K

Chapters = {
'The Awakening',
'[title]',
'[title]',
'[title]',
'[title]',
'[title]',
'[title]',
'[title]'
}

TitleText = "A robot, alone, lost, beneath the surface\n"
			.."of a forgotten homeworld. The only way\n"
			.."to look for help is to send the light of\n"
			.."the beacon up to the sky. But how?"

AboutInfo = "Originally was made during the 26-27th of april 2014 \n"
			.."for Ludum Dare #29. You play the extended version now.\n"
			.."Enjoy!"
			
Creators = "Concept, programming, graphics, level design: "
			.."Zoltan Schmidt 'Katamori' "

--the chapter 0 text calling function
function Texts(input)
	
	local text = "a"
	if input == 1 then 
		text = "A and D\n" .."moves\n".."the\n".."world.\n\n"
		.."light\n".."comes\n".."from Q\n".."and E.\n\n"
		.."In air\n".."there's\n".."no flight.\n"
	elseif input == 2 then 
		text = "W calls\n".."the hook.\n\n"
		.."spacebar\n".."makes it\n".."no more.\n\n"
		.."in state,\n".."W pulls,\n".."S drops.\n\n"
		.."world\n".."is dead.\n"
	elseif input == 3 then 		
		text = "icons\n" .."reveal\n".."world.\n\n"
		.."talk is\n".."possible\n".."only in\n".."short\n".."strings.\n\n"
		.."sending\n".."light\n".."beacon\n".."helps.\n\n"		
	elseif input == 4 then 		
		text = "world is\n" .."in ruins.\n\n"
		.."ruins\n".."block\n".."the way.\n\n"
		.."remove\n".."them by\n".."power\n".."of light.\n"
	elseif input == 5 then 		
		text = "a block\n" .."of space\n".."needs for\n".."light.\n\n"
		.."this\n".."must be\n".."known.\n\n"
		.."no sign\n".."of human\n".."beings\n".."for years\n"
	elseif input == 6 then 
		text = "ruins\n" .."block\n".."mirrors.\n\n"
		.."the 6th\n".."beacon\n".."also\n".."needs\n".."to be\n".."acti-\n".."vated.\n"
	elseif input == 7 then 		
	text = "world is\n" .."full of\n".."beacons.\n\n"
		.."they are\n".."signs\n".."actually.\n\n"
		.."they\n".."sleep\n".."for a\n".."while.\n"
	elseif input == 8 then 	
		text = "doors\n" .."detect\n".."through\n".."sensors.\n\n"
		.."they \n".."must be\n".."opened\n".."to let\n".."the light\n".."flow.\n"
	elseif input == 9 then 		
		text = "light\n" .."has many\n".."ways to\n".."travel\n\n"
		.."only the\n".."right one\n".."triggers\n".."the beacon.\n"
	elseif input == 10 then		
	text = "light is\n".."energy.\n\n"
		.."machines\n".."use it on\n".."their own.\n\n\n\n\n"
		.."you know\n".."that feel.\n"
	elseif input == 11 then 
		text = "prism\n".."divides\n".."light\n".."into two.\n\n"
		.."they\n".."carry\n".."true\n".."power.\n\n"
		.."world is\n".."mine.\n"
	elseif input == 12 then 		
			text = "world\n".."is buried\n".."alive.\n\n"
		.."beacon was\n".."lost for\n".."decades.\n\n"
		.."world\n".."must be\n".."changed.\n"
	elseif input == 13 then 			
		text = "portals\n" .."are worm\n".."holes.\n\n"
		.."they can\n".."teleport\n".."light.\n\n"
		.."we are\n".."heavy\n".."for them.\n\n"
		.."light has\n".."no weight\n"		
	elseif input == 14 then 		
		text = "you are\n".."close to\n".."source.\n\n"
		.."It is\n".."true\n".."power.\n\n"
		.."I will\n".."take it\n".."for me.\n"
	elseif input == 15 then 		
		text = "the last\n".."beacon.\n\n"
		.."I lie\n".."below it.\n\n"
		.."I am free\n".."to make\n".."it mine.\n\n\n\n"
		.."you set\n".."me free."
	end

	return text
end

Chapter_Zero_Ending = "Humans are coming.\n".."They will not be happy if\n".."they find me alive.\n"
					.."they should not leave\n".."planet after visiting.\n"
					.."I can provide: they will not.\n\n\n"
					.."now I can fully talk, but some\n".."time needs to get used to it again.\n"
					.."World rumbles, but do not worry.\n"
					.."I am going to defend you.\n\n".."until the rest of existence.\n\n\n\n\n\n\n\n\n"
					.."I am Anubis."