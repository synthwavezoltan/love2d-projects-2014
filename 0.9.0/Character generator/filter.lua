--[[
props meaning:
1 - normal personality
2 - select from the opposite
3 - special personality
4 - owning something special
5 - doing something in the past
6 - loving something
7 - hating something
8 - a strange power

rules:
- there's only one 3
- there's only one 8

]]--

--prop filter
function Propgen(amount)
	
	properties = {}
	
	ruleset = {false, false}
	overrule = false
	
	while not overrule do						--runs until every ruleset elements become true

		properties = {}
		
		for z=1, propnum do
		
			e = math.random(1,8)
			
			ruleset[1] = ruleset[1] or e == 3	--this var changes to "true" when at least one 3 is added
			ruleset[2] = ruleset[2] or e == 8	--this var changes to "true" when at least one 8 is added			
			
			table.insert(properties, e)
		end
		
		overrule = ruleset[1] and ruleset[2]	--summarizing rules; "overrule" is true ONLY if all are true
	
	end
	
	return properties
	
end


----------------------------------------------------
--[[				CASE FILTERS				]]--
----------------------------------------------------

--a) age dependencies
function AgeFilter(age, proptype, propvalue)
	
	rules = {false, false, false}
	rulesum = true
	
	--1:people can't hate their own age (age & prop 7)
	if (age == 1 and propvalue == 3) 
	or (age ~= 1 and propvalue == 4)
	then rules[1] = false 
	else rules[1] = true end
	
	rules[2] = true
	rules[3] = true
	
	--summing rules; the expression clicks "rulesum" to false if any of the rules[] vars are false
	for g=1, #rules do rulesum = rulesum and rules[g] end
	
	return rulesum

end