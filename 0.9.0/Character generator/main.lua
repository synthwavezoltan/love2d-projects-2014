require 'sets'
require 'filter'

function love.load()

	math.randomseed( os.time() )

	list = {}
	propnum = 3
	charamount = 50
	
	InitGen()
	
end

function love.draw()

	love.graphics.print("Katamori's Wonderful Character Idea Generator 2015 Pre-Alpha (c)",0,0)	
	love.graphics.print("What about making...",0,48)

	for i=1, charamount do		
		love.graphics.print(list[i],0,64 + 16*i) 	
	end
	
	love.graphics.print("...or clicking, to generate some new fascinating ideas",0,64 + 16*charamount + 32)
	
end

function love.update(dt)

	if love.mouse.isDown("l") then InitGen() end

end

function InitGen()

	for i=1, charamount do
	
		list[i] = {}
		propset = Propgen(propnum)
		
		Character_Creator(math.random(1,1000), propset , i)
	end

end

--[[
props meaning => type ids:
1 - normal personality
2 - select from the opposite
3 - special personality
4 - owning something special
5 - doing something in the past
6 - loving something
7 - hating something
8 - a strange power

]]--
function Character_Creator(gender, props, iteration)
	
							--tables to store:
	local shards = {}		--the string portions
	local prop_id = {}		--id numbers of properties
	local prop_types = {}	--type ids of properties 
	
	for j=1, #props+1 do shards[j] = " " end 
	
	list[iteration][666] = false
	
	--generate age
	charage = math.random(1,#age)
	
	--generate gender
	if gender%2==0 
	then shards[1] = "...a"..age[charage].." female" 
	else shards[1] = "...a"..age[charage].." male" end
	
	--generate properties based on table "props"
	repeat
	
		prop_id = {{}, {}}
	
	
		for j=1, #props do
		
			prop_types[j] = props[j]
		
			if props[j] == 1 then
				local e = math.random(1,#pers_norm)
				shards[j+1] = "is "..pers_norm[e]
				prop_id[j] = e
	
			elseif props[j] == 2 then
				local e = math.random(1,#pers_oppo)
				shards[j+1] = "is "..pers_oppo[e]	
				prop_id[j] = e
				
			elseif props[j] == 3 then
				local e = math.random(1,#pers_spec)
				shards[j+1] = "is "..pers_spec[e]	
				prop_id[j] = e
				
			elseif props[j] == 4 then
				local e = math.random(1,#owns)
				shards[j+1] = "owns "..owns[e]	
				prop_id[j] = e
				
			elseif props[j] == 5 then	
				local e = math.random(1,#past)
				shards[j+1] = "has "..past[e]
				prop_id[j] = e
				
			elseif props[j] == 6 then
				local e = math.random(1,#hobby)
				shards[j+1] = "loves "..hobby[e]
				prop_id[j] = e
				
			elseif props[j] == 7 then
				local e = math.random(1,#hobby)
				while not AgeFilter(charage, e) do e = math.random(1,#hobby) end
				shards[j+1] = "hates "..hobby[e]
				prop_id[j] = e
				
			elseif props[j] == 8 then
				local e = math.random(1,#power)
				shards[j+1] = power[e]	
				prop_id[j] = e
				
			end	
		end
		
		--NOTE: IT DOESN'T FUCKIN' WORK IF PROPNUM IS BIGGER THAN 3				
	until ((prop_types[1] ~= prop_types[2] or prop_id[1] ~= prop_id[2]) and
			(prop_types[1] ~= prop_types[2] or prop_id[1] ~= prop_id[3]) and
			(prop_types[1] ~= prop_types[2] or prop_id[2] ~= prop_id[3]))
	
	--attaching shards
	list[iteration] = shards[1]..", who "..shards[2]..", "..shards[3]..", and "..shards[4].."? Or..."
	--list[iteration] = shards[1]..", who "..prop_id[1]..", "..prop_id[2]..", and "..prop_id[3].."? Or..."
	--list[iteration] = shards[1]..", who "..prop_types[1]..", "..prop_types[2]..", and "..prop_types[3].."? Or..."
	


end




