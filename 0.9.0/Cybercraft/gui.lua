--love.draw elements put here to make main.lua simplier; they speak for themselves
function Display_HUD()

	--HUD
	love.graphics.draw(Picset[1], 0,0)
	
	--selector-icon
	love.graphics.draw(Picset[2], SelectorPositions[Player.Selected_Slot],672)	
	
	--hotbar
	for i=1, 8 do
		if Player.Hotbar[i] ~= 0 then
			love.graphics.draw(ItemsPic, ItemsTileset[Player.Hotbar[i]], SelectorPositions[i]+16,688) 
		end	
	end
	
	--stats
	love.graphics.print(Player.HP,512,608)
	love.graphics.print(Player.Energy,512,632)
	love.graphics.print(Player.XP,512,656)	
	love.graphics.print(Player.Lvl,512,680)	
	love.graphics.print(Player.Inventory[Player.In_Hand+1],512,712)	
	
	--HP bar
	for i=math.floor(Player.HP/8), 1, -1  do
		love.graphics.draw(Picset[5], 464-(i*32),616)
	end
	
end


function Display_Inventory_Menu()

	love.graphics.draw(Picset[3], InvX,InvY)
	love.graphics.print("Weight: "..InvWeight.."/"..Player.Inv_Capacity,128,536)
	love.graphics.print(InvListStart.." - "..InvListStart+(InvColumnNum*InvColumnLength),464,536)
	
	for j=0, InvColumnNum-1 do
		for i=1, InvColumnLength  do
			if Player.Inventory[InvListStart+i+(j*(InvColumnLength))] ~= 0 then
				love.graphics.draw(ItemsPic, ItemsTileset[InvListStart+i+(j*(InvColumnLength))], InvX+32+(j*InvColDist),32+InvY+(InvRowDist*(i-1)))
				love.graphics.print(" X "..Player.Inventory[InvListStart+i+(j*(InvColumnLength))], InvX+56+(j*InvColDist),40+InvY+(InvRowDist*(i-1)))
			end
		end
	end
	
	--ammo amount
	love.graphics.print("Ammo:\n".."- Test Gun: "..Player.Inventory[2],700,96)	
	
	--selected item ID
	love.graphics.draw(ItemsPic, ItemsTileset[InvSelectedItemID],700,512)
	love.graphics.print(InvSelectedItemID,680,512)
	
		
end

function Display_Dialogue(character)
	
	if Player_Neighbour() ~= 0 and character.State ~= 5 then
		love.graphics.draw(Picset[4], DialogX, DialogY)
		
		if character.Name == nil then
			love.graphics.print(character.Type,DialogX+32,DialogY+32)
		else
			love.graphics.print(character.Name,DialogX+32,DialogY+32)		
		end
		
		
		love.graphics.print(CHARACTER_DOCUMENTATION[character.ID].Random_Texts[Dial_Random_Text],
		DialogX+32,DialogY+96)		
		
	end
end




function Display_Textures()

--step 1: palyfield
	for j=1,ScreenSizeX do
		for i=1, ScreenSizeY do

			love.graphics.draw(TilesetPic, Tileset[Field[OffsetX+j][OffsetY+i]], 
			SmoothOffsetX+(j-1)*TileSize, 
			SmoothOffsetY+(i-1)*TileSize)

		end
	end
	
--step 2: items
	for i=1, #Items do
		if Items[i].X_Tile ~= "equipped" and Items[i].Y_Tile ~= "equipped" 
		and ((Items[i].X_Tile-1-OffsetX)*TileSize)+Items[i].OffsetX >= 0
		and ((Items[i].X_Tile-1-OffsetX)*TileSize)+Items[i].OffsetX < ScreenSizeX*TileSize
		and ((Items[i].Y_Tile-1-OffsetY)*TileSize)+Items[i].OffsetY >= 0
		and ((Items[i].Y_Tile-1-OffsetY)*TileSize)+Items[i].OffsetY < ScreenSizeY*TileSize then
			--the actual drawing itself
			love.graphics.draw(ItemsPic, ItemsTileset[Items[i].Sprite], 
			((Items[i].X_Tile-1-OffsetX)*TileSize)+Items[i].OffsetX+SmoothOffsetX, 
			((Items[i].Y_Tile-1-OffsetY)*TileSize)+Items[i].OffsetY+SmoothOffsetY-TileSize)
			
	
		end
	end		
	
--step 3: charcters, IF THEY ARE ON SHOWED FIELD -> they won't appear inside the HUD!
	for i=1, #Characters do
		if ((Characters[i].X_Tile-1-OffsetX)*TileSize)+Characters[i].OffsetX >= 0
		and ((Characters[i].X_Tile-1-OffsetX)*TileSize)+Characters[i].OffsetX < ScreenSizeX*TileSize
		and ((Characters[i].Y_Tile-1-OffsetY)*TileSize)+Characters[i].OffsetY >= 0
		and ((Characters[i].Y_Tile-1-OffsetY)*TileSize)+Characters[i].OffsetY < ScreenSizeY*TileSize then
			--the actual drawing itself
			love.graphics.draw(CharPic[Characters[i].Type],Charset[Characters[i].Type][Characters[i].Sprite], 
			((Characters[i].X_Tile-1-OffsetX)*TileSize)+Characters[i].OffsetX+SmoothOffsetX, 
			((Characters[i].Y_Tile-1-OffsetY)*TileSize)+Characters[i].OffsetY+SmoothOffsetY-TileSize)
			
			--debug appear of numbers
			--love.graphics.print(Characters[i].HP, 
			--((Characters[i].X_Tile-1-OffsetX)*TileSize)+Characters[i].OffsetX, 
			--((Characters[i].Y_Tile-1-OffsetY)*TileSize)+Characters[i].OffsetY)			
		end
	end	
	
--step 4: projectiles
	if #Projectiles ~= 0 then
		for i=1, #Projectiles do
			if ((Projectiles[i].X_Tile-1-OffsetX)*TileSize)+Projectiles[i].OffsetX >= 0
			and ((Projectiles[i].X_Tile-1-OffsetX)*TileSize)+Projectiles[i].OffsetX < ScreenSizeX*TileSize
			and ((Projectiles[i].Y_Tile-1-OffsetY)*TileSize)+Projectiles[i].OffsetY >= 0
			and ((Projectiles[i].Y_Tile-1-OffsetY)*TileSize)+Projectiles[i].OffsetY < ScreenSizeY*TileSize then
				--the actual drawing itself
				love.graphics.draw(CharPic[Projectiles[i].Charset],Charset[Projectiles[i].Charset][Projectiles[i].Sprite], 
				((Projectiles[i].X_Tile-1-OffsetX)*TileSize)+Projectiles[i].OffsetX+SmoothOffsetX, 
				((Projectiles[i].Y_Tile-1-OffsetY)*TileSize)+Projectiles[i].OffsetY+SmoothOffsetY)
						
			end
		end	
	end		

--step 5: player
	if not GameOver then
	
		love.graphics.draw(CharPic["Player"],Charset["Player"][Player.Sprite], 
		((Player.X_Tile-1-OffsetX)*TileSize)+Player.OffsetX, 
		((Player.Y_Tile-1-OffsetY)*TileSize)+Player.OffsetY-TileSize)
	else
	
		love.graphics.draw(CharPic["Player"],Charset["Player"][24], 
		((Player.X_Tile-1-OffsetX)*TileSize)+Player.OffsetX, 
		((Player.Y_Tile-1-OffsetY)*TileSize)+Player.OffsetY-TileSize)	
	end

	--love.graphics.print(Field[Player.X_Tile][Player.Y_Tile],
	--((Player.X_Tile-1-OffsetX)*TileSize)+Player.OffsetX, 
	--((Player.Y_Tile-OffsetY)*TileSize)+Player.OffsetY-TileSize)	
	

end