function SampleMap()
	
	
	--1: field filled with random tiles
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			if math.random(100) < 2 then
				Field[j][i] = 18
				if i-1 ~= 0 and Field[j][i] ~= 2 then Field[j][i-1] = 2 end
			else
				Field[j][i] = 1			
			end
			
			if i==2 or j==2 or i==150 or j==150 then Field[j][i] = 2 end
		end
		
	end
	
	--2:creating some characters
	Characters = {}

	CreateChar(2)	
	Characters[1].X_Tile = 26
	Characters[1].Y_Tile = 18
	Characters[1].State = 1
	Characters[1].Facing = "down"
	
	CreateChar(3)	
	Characters[2].X_Tile = 24
	Characters[2].Y_Tile = 18
	Characters[2].State = 1
	Characters[2].Facing = "down"	

	CreateChar(1)
	Characters[3].X_Tile = 28
	Characters[3].Y_Tile = 18	
	Characters[3].State = 2
	Characters[3].Facing = "down"	
	Characters[3].Mov_Dir = "up"		
	
	for z=4, 30 do
		CreateChar(3)
		Characters[z].X_Tile = 27+z
		Characters[z].Y_Tile = 19+z	
		Characters[z].State = 4
		Characters[z].Mov_Dir = MovSet[math.random(1,#MovSet)]
		Characters[z].Patrol_Distance = math.random(1,10)
	end	
	
	--3: creating some items
	Items = {}
	
	for i=1, 15 do

		CreateItem(2)
		Items[i].X_Tile = (19+2*i)
		Items[i].Y_Tile = 19	
	
	end	
		CreateItem(1)
		Items[#Items].X_Tile = 21
		Items[#Items].Y_Tile = 22	

	--4: nullifying the (yet) unused rest	
	Projectiles = {}		--defining projectiles
	
	
	--[[
	...aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaand there was light!
	...I mean, finally, I could functionize the map creation!
	Well, I'm actually some sort of God then. - K
	]]
	

end