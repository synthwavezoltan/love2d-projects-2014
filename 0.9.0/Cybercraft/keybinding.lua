require 'physics'
require 'projectile_behaviour'

function Repeated_Key_Behaviour()

	--player moving
	if not Player.Is_Moving and not GameOver then
	
		if love.keyboard.isDown("w") 
		and Player.Y_Tile > 2 
		and not SolidChecker(Player.X_Tile, Player.Y_Tile-1) 
		then
			Player.Is_Moving = true
			Player.Mov_Dir = "up"	
		end
		
		if love.keyboard.isDown("s") 
		and Player.Y_Tile < SizeY 		
		and not SolidChecker(Player.X_Tile, Player.Y_Tile+1) 
		then
			Player.Is_Moving = true
			Player.Mov_Dir = "down"				
		end
		
		if love.keyboard.isDown("a") 
		and Player.X_Tile > 2 
		and not SolidChecker(Player.X_Tile-1, Player.Y_Tile) 
		then 
			Player.Is_Moving = true
			Player.Mov_Dir = "left"	
		end
		
		if love.keyboard.isDown("d")
		and Player.X_Tile < SizeX-1 		
		and not SolidChecker(Player.X_Tile+1, Player.Y_Tile) 
		then
			Player.Is_Moving = true
			Player.Mov_Dir = "right"	
		end
	end
end

function love.keypressed(key)
	
	--talking
	if key == "f" then
		if Player_Neighbour() ~= 0 then
			Dial_Random_Text = math.random(1,#CHARACTER_DOCUMENTATION[Characters[Player_Neighbour()].ID].Random_Texts)
			DialogueActive = not DialogueActive			
		end
	--drop item
	elseif key == "q" then
		if Player.Hotbar[Player.Selected_Slot] ~= 0 then DropItem(Player.Hotbar[Player.Selected_Slot]) end
	end

end

function love.mousepressed(x,y,button)
	
		if button == "l" then
		
			if MouseOnField and not GameOver then
			
				--shooting
				if not Inventory and not DialogueActive
				and Player.In_Hand ~= 0 
				and ITEM_DOCUMENTATION[Player.In_Hand].Type == "Weapon" 
				and Player.Inventory[Player.In_Hand+1] > 0
				then
					if (Player.Sprite+Player.SpritePos == 7 or Player.Sprite+Player.SpritePos == 8)
					and not SolidChecker(Player.X_Tile, Player.Y_Tile-1) then 
						Create_Projectile(Player, "up", ITEM_DOCUMENTATION[Player.In_Hand].Category, Player.In_Hand, true)
					elseif (Player.Sprite+Player.SpritePos == 9 or Player.Sprite+Player.SpritePos == 10)
					and not SolidChecker(Player.X_Tile, Player.Y_Tile+1) then 
						Create_Projectile(Player, "down", ITEM_DOCUMENTATION[Player.In_Hand].Category, Player.In_Hand, true)
					elseif (Player.Sprite+Player.SpritePos == 3 or Player.Sprite+Player.SpritePos == 4)
					and not SolidChecker(Player.X_Tile-1, Player.Y_Tile) then 
							Create_Projectile(Player, "left", ITEM_DOCUMENTATION[Player.In_Hand].Category, Player.In_Hand, true)
					elseif (Player.Sprite+Player.SpritePos == 1 or Player.Sprite+Player.SpritePos == 2)
					and not SolidChecker(Player.X_Tile+1, Player.Y_Tile) then 
						Create_Projectile(Player, "right", ITEM_DOCUMENTATION[Player.In_Hand].Category, Player.In_Hand, true) end
					
					--shoot decreases ammo
					Player.Inventory[ITEM_DOCUMENTATION[Player.In_Hand].Ammo_ID] = Player.Inventory[ITEM_DOCUMENTATION[Player.In_Hand].Ammo_ID] - 1
				end

			--you are clicking on INVENTORY icon
			elseif (MouseX < 250 and MouseY < 50) then
				if not DialogueActive then
					Inventory = not Inventory
				end
			
			--you are clicking on QUIT icon
			elseif (MouseX > 915 and MouseY < 50) then
				love.event.push("quit")
				
			
			end
			
			--you are clicking on items in Inventory -> you put items to the hotbar
			--uh-oh-ow...so, it has several conditions; in order:
			--proper placement for the cursor
			--the item type was already declared	
			--player actually carries the selected item			
			--selected item is usable (otherwise this action wouldn't make sense)
			
			
			if Inventory 
			and (MouseX-32-InvX)%InvColDist <= TileSize 
			and (MouseY-32-InvY)%InvRowDist <= TileSize 
			and InvSelectedItemID <= #ITEM_DOCUMENTATION
			and Player.Inventory[InvSelectedItemID] ~= 0 			
			and ITEM_DOCUMENTATION[InvSelectedItemID].Usable then
			
				--if selected item was already put to the hotbar
				--then delete the others
				if Hotbar_Redundancy(InvSelectedItemID) ~= 0 then
					for i=1, #Player.Hotbar do
						if Player.Hotbar[i] == InvSelectedItemID
						then Player.Hotbar[i] = 0 end
					end
				end
				
				--and add item to hotbar ONLY AFTER THIS
				Player.Hotbar[Player.Selected_Slot] = InvSelectedItemID				
			end	
		
		elseif button == "r" then
		

		elseif button == "wd" then
			if not GameOver then
				if Inventory then
					if InvListStart + (InvColumnNum*InvColumnLength) < #ItemsTileset then InvListStart = InvListStart + InvColumnLength end			
				else
					if Player.Selected_Slot == 8 then 
						Player.Selected_Slot = 1 
					else Player.Selected_Slot = Player.Selected_Slot+1 end
				end
			end
		
		elseif button == "wu" then	
			if not GameOver then	
				if Inventory then
					if InvListStart > 1 then InvListStart = InvListStart - InvColumnLength end	
				else		
					if Player.Selected_Slot == 1 then 
						Player.Selected_Slot = 8 
					else Player.Selected_Slot = Player.Selected_Slot-1 end		
				end
			end
		end		
		
end