function Create_Projectile(source, direction, weapontype, weapon, fromplayer)
	
	--setting texture
	
	--setting direction
	local x, y = 0,0
	if direction == "up" then y = -1 x = 0 end
	if direction == "down" then y = 1 x = 0 end
	if direction == "left" then x = -1 y = 0 end
	if direction == "right" then x = 1 y = 0 end	

	if not (direction == "up" and SolidChecker(source.X_Tile, source.Y_Tile-1)) 
	or (direction == "down" and SolidChecker(source.X_Tile, source.Y_Tile+1)) 
	or (direction == "left" and SolidChecker(source.X_Tile-1, source.Y_Tile)) 
	or (direction == "right" and SolidChecker(source.X_Tile+1, source.Y_Tile)) then 
		
		--create projectile itself
		table.insert(Projectiles, {})			
		Projectiles[#Projectiles] = {
			X_Tile = source.X_Tile+x, Y_Tile = source.Y_Tile+y,
			OffsetX = 0, OffsetY = 0,
			Charset = weapontype,
			Sprite = 1,
			Is_Moving = false,
			Mov_Dir = direction, Mov_Spd = 16,
			Damage = ITEM_DOCUMENTATION[weapon].Damage,
			From_Player = fromplayer
		}
	end
end

function Break_Projectile(projectile, ID)
	
	--if next tile is solid then
	if (projectile.Mov_Dir == "up" and SolidChecker(projectile.X_Tile, projectile.Y_Tile-1)) 
	or (projectile.Mov_Dir == "down" and SolidChecker(projectile.X_Tile, projectile.Y_Tile+1)) 
	or (projectile.Mov_Dir == "left" and SolidChecker(projectile.X_Tile-1, projectile.Y_Tile)) 
	or (projectile.Mov_Dir == "right" and SolidChecker(projectile.X_Tile+1, projectile.Y_Tile)) then 
		--mark projectile as "deletable"
		projectile.Mov_Dir = "DELETE ME"
		
		--if player's hit by the projectile of someone else then injure him
		if (projectile.X_Tile == Player.X_Tile and projectile.Y_Tile == Player.Y_Tile-1)
		or (projectile.X_Tile == Player.X_Tile and projectile.Y_Tile == Player.Y_Tile+1)
		or (projectile.X_Tile == Player.X_Tile-1 and projectile.Y_Tile == Player.Y_Tile)
		or (projectile.X_Tile == Player.X_Tile+1 and projectile.Y_Tile == Player.Y_Tile) then
			if not GameOver and not Immortal and not projectile.fromplayer then
				Player.HP = Player.HP -projectile.Damage			
				if Player.HP < 0 then GameOver = true Player.HP = 0 end
			end
			
		else	
		--else: are there any characters that is hit? hurt them!
			for i=1, #Characters do
				if (projectile.X_Tile == Characters[i].X_Tile and projectile.Y_Tile == Characters[i].Y_Tile-1)
				or (projectile.X_Tile == Characters[i].X_Tile and projectile.Y_Tile == Characters[i].Y_Tile+1)
				or (projectile.X_Tile == Characters[i].X_Tile-1 and projectile.Y_Tile == Characters[i].Y_Tile)
				or (projectile.X_Tile == Characters[i].X_Tile+1 and projectile.Y_Tile == Characters[i].Y_Tile) then
					Characters[i].HP = Characters[i].HP -1
				end		
			end
		end
	end

end

