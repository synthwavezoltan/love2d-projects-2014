require 'character_documentation'

--all the texts which are appearing ingame


CHARACTER_DOCUMENTATION[1].Random_Texts = {
	"IDENTIFIED OBJECT IS APPROACHING.",
	"OBJECT NAME = KATAMORI",
	"OBJECT GENDER = MALE",
	"OBJECT ALLIANCE = UNKNOWN",
	"OBJECT YEAR OF BIRTH = 1993",
	"OBJECT BIRTHDAY = 22TH OCTOBER",		
	"OBJECT HAIR COLOR = LIGHT BROWN",
	"OBJECT SKIN COLOR = WHITE"}	

CHARACTER_DOCUMENTATION[2].Random_Texts = {
	"Where am I???",
	"My arm doesn't work. Strange...my mechanic should see it.",
	"I hope Winry is in safe...",
	"Goddamn desert...",
	"Did you call me small???",
	"I did something terrible. I had to pay its price.",
	"Wasn't that guy Hughes there????",
	"I wish I could protect her...I'm just so helpless sometimes.",
	"Beware of Father! He wants to transform people into energy!",
	"Feel so depressed sometimes...with such a past, it's not a surprise.",
	"That strange shade sent me to this unknown place...",
	"That table says about something 'nanites'. What does it mean?",
	"I have to return to Central at any costs! People are in danger!",
	"Being good in a rotten world is worse than Hell sometimes.",}
	
CHARACTER_DOCUMENTATION[3].Random_Texts = {
	"Get outta my way!",
	"Hey, you are that Youtube guy!",
	"Mind your own business!",
	"My nanites don't seem to work properly. I wish I could afford another \ntreatment...",
	"Are you from the arcology? Augmented guys usually come from there.",
	"Everything so fuckin' expensive...I hate this city.",
	"Could you see those giants outside the city? How on earth people can \nlive there??",
	"Another monorail line is out of order. Public transport is truly\ncatastrophic sometimes.",
	"I swear I'm gonna put some spike traps to my house! These Streetknight \ndouchebags respect nothing.",
	"Heard about that Metallica holoconcert tonight?! Can't wait for that!!"
 }