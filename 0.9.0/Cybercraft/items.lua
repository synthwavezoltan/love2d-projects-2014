function ItemBehaviour(itemnum)
	
	--if player is on item AND he can carry one more of it
	if Items[itemnum].X_Tile == Player.X_Tile
	and Items[itemnum].Y_Tile == Player.Y_Tile+1 
	and 
	((ITEM_DOCUMENTATION[Items[itemnum].ID].Category ~= "Ammo"
	and InvWeight + ITEM_DOCUMENTATION[Items[itemnum].ID].Weight <= Player.Inv_Capacity) 
	
	or (ITEM_DOCUMENTATION[Items[itemnum].ID].Category == "Ammo" 
	and InvWeight + (ITEM_DOCUMENTATION[Items[itemnum].ID].Weight*ITEM_DOCUMENTATION[Items[itemnum].ID].Clip_Size) <= Player.Inv_Capacity))
	then 
		
		--add item to hotbar if it's usable
		if ITEM_DOCUMENTATION[Items[itemnum].ID].Usable then
			if Player.Hotbar[Player.Selected_Slot] == 0 then
				Player.Hotbar[Player.Selected_Slot] = Items[itemnum].ID
			else
				for i=1, 8 do
					if Player.Hotbar[i] == 0 then
						Player.Hotbar[i] = Items[itemnum].ID
						break
					end
				end
			end
		end
		
		--if it's ammo then grab more than 1
		if ITEM_DOCUMENTATION[Items[itemnum].ID].Category == "Ammo" then
				Player.Inventory[Items[itemnum].ID] = Player.Inventory[Items[itemnum].ID] + ITEM_DOCUMENTATION[Items[itemnum].ID].Clip_Size	
		else
			Player.Inventory[Items[itemnum].ID] = Player.Inventory[Items[itemnum].ID] + 1
		end
		
		Items[itemnum].X_Tile, Items[itemnum].Y_Tile = "equipped", "equipped"
	end

end

function CreateItem(typenum)
	
	table.insert(Items,{ID = typenum})	
	
	for k, v in pairs(ITEM_DOCUMENTATION[Items[#Items].ID]) do
		Items[#Items][k] = v
	end	

end

function DropItem(itemID)
	--search the first item in itemlist with the given ID that is equipped
	local itemnum = 0
	
	for i=1, #Items do
		if Items[i].ID == itemID and Items[i].X_Tile == "equipped" and Items[i].Y_Tile == "equipped" then
			itemnum = i
			break
		end
	end

	--put the item we want to drop into the front of Player
	if Player.Facing == "up" and not SolidChecker(Player.X_Tile, Player.Y_Tile) then
		Items[itemnum].X_Tile = Player.X_Tile
		Items[itemnum].Y_Tile = Player.Y_Tile
	elseif Player.Facing == "down" and not SolidChecker(Player.X_Tile, Player.Y_Tile+2) then 
		Items[itemnum].X_Tile = Player.X_Tile
		Items[itemnum].Y_Tile = Player.Y_Tile+2	
	elseif Player.Facing == "left" and not SolidChecker(Player.X_Tile-1, Player.Y_Tile+1) then 
		Items[itemnum].X_Tile = Player.X_Tile-1	
		Items[itemnum].Y_Tile = Player.Y_Tile+1
	elseif Player.Facing == "right" and not SolidChecker(Player.X_Tile+1, Player.Y_Tile+1) then 
		Items[itemnum].X_Tile = Player.X_Tile+1
		Items[itemnum].Y_Tile = Player.Y_Tile+1
	end
	
	--remove it from hotbar and from inventory
	if Items[itemnum].X_Tile ~= "equipped" 
	and Items[itemnum].Y_Tile ~= "equipped" then
		Player.Hotbar[Player.Selected_Slot] = 0	
		Player.Inventory[itemID] = Player.Inventory[itemID] - 1 
	end
	
	
end

--checks if an item ID was already put to the hotbar
function Hotbar_Redundancy(id)
	
	local z = 0
	
	for i=1, #Player.Hotbar do if Player.Hotbar[i] == id then z = z +1 end end
	
	return z
end