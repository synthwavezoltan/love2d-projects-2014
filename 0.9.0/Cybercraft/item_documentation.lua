--[[
A table, that contains ALL the variables of certain item types!
if you wanna return with certain value of items, use their ID
and call them from this table.]]

ITEM_DOCUMENTATION = {}

--THE TEST GUN!
ITEM_DOCUMENTATION[1] = {

--appearing		
X_Tile = 0, Y_Tile = 0, OffsetX = 0, OffsetY = 0, Sprite = 1,
				
--details
ID = 1, Name = "Test Gun", Type = "Weapon", Category = "Special weapon", Origin = "General item",

--common properties
Usable = true,
Weight = 9,
Basic_Prize = 10,

--specials
Projectile_Type = "Special shot",
Damage = 15,
Ammo_ID = 2,

}

ITEM_DOCUMENTATION[2] = {

--appearing		
X_Tile = 0, Y_Tile = 0, OffsetX = 0, OffsetY = 0, Sprite = 2,
				
--details
ID = 2, Name = "Test Ammo", Type = "Supply", Category = "Ammo", Origin = "General item",

--common properties
Usable = false,
Weight = 0.01,
Basic_Prize = 2,

--specials
Clip_Size = 5,
}



