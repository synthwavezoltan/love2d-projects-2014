--[[
A table, that contains ALL the variables of certain character types!
if you wanna get informations about characters, use their ID
and call them from this table.]]

CHARACTER_DOCUMENTATION = {}

--THE TEST PUPPET!
CHARACTER_DOCUMENTATION[1] = {

	--moving and appearing		
	X_Tile = 0, Y_Tile =  0, OffsetX = 0, OffsetY = 0, Sprite = 1, Facing = "right", Is_Moving = false, Mov_Dir = "no", 
																										
	--details
	Fraction = "Robot", Type = "Test Puppet",
	State = 2, ID = 1,
	Weapon_ID = 1,

	--specials
	Hero_Character = false,		--true if character is integral part of the story
	
	--stats
	HP = 10,
			
	--abilities
	Mov_Spd = 1,				--moves 2^Mov_Spd pixels per frame		
	
	--variables for certain states	
	Patrol_Direction = "right",		--this is for Patrolling
	Patrol_Distance = 5,
	Patrol_Storing = 0,	
	Turning_Spd = 100,				--this is for turnarounds
	Walking_Storing = 0,			--this is for walking
	
	
	--texts
	Random_Texts = {}
}	

--Edward Elric test 
CHARACTER_DOCUMENTATION[2] = {
	
	X_Tile = 0, Y_Tile =  0, OffsetX = 0, OffsetY = 0, Sprite = 9, Facing = "up", Is_Moving = false, Mov_Dir = "no", 

	Fraction = "United Forces", Type = "Elric Test",
	State = 4, ID = 2,
	Weapon_ID = 1,
	
	Hero_Character = true,
	Name = "Edward Elric, the Fullmetal Alchemist",
	
	--stats
	HP = 1000,
			
	--abilities
	Mov_Spd = 2,				
			
	Patrol_Distance = 5,
	Patrol_Storing = 0,

	Turning_Spd = 50,
	
	Walking_Storing = 0,

	
	Random_Texts = {}
}

--random civilian
CHARACTER_DOCUMENTATION[3] = {
	
	X_Tile = 0, Y_Tile =  0, OffsetX = 0, OffsetY = 0, Sprite = 9, Facing = "up", Is_Moving = false, Mov_Dir = "no", 

	Fraction = "United Forces", Type = "Civilian",
	State = 4, ID = 3,
	
	Hero_Character = false,
	
	--stats
	HP = 5,
			
	--abilities
	Mov_Spd = 1,				
			
	Patrol_Distance = 5,
	Patrol_Storing = 0,

	Turning_Spd = 50,
	
	Walking_Storing = 0,

	
	Random_Texts = {}
}

--trader
CHARACTER_DOCUMENTATION[4] = {
	
	X_Tile = 0, Y_Tile =  0, OffsetX = 0, OffsetY = 0, Sprite = 9, Facing = "up", Is_Moving = false, Mov_Dir = "no", 

	Fraction = "Justice Empire", Type = "Trader",
	State = 4, ID = 3,
	
	Hero_Character = false,
	
	--stats
	HP = 5,
			
	--abilities
	Mov_Spd = 1,				
			
	Patrol_Distance = 5,
	Patrol_Storing = 0,

	Turning_Spd = 50,
	
	Walking_Storing = 0,

	
	Random_Texts = {}
}
