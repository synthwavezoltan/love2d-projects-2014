--functions for things characters ACTUALLY can do!

--character shoots player if he's on sight
function Shoot_On_Sight(character)

	--if player.X and player.Y equals to char.X or char.Y then shoot to player
	if not GameOver then
		if character.X_Tile == Player.X_Tile and character.Y_Tile > Player.Y_Tile then
			character.Facing = "up"
			Create_Projectile(character, character.Facing, ITEM_DOCUMENTATION[character.Weapon_ID].Category ,character.Weapon_ID, false)				
		elseif character.X_Tile == Player.X_Tile and character.Y_Tile < Player.Y_Tile then
			character.Facing = "down"	
			Create_Projectile(character, character.Facing, ITEM_DOCUMENTATION[character.Weapon_ID].Category ,character.Weapon_ID, false)				
		elseif character.X_Tile > Player.X_Tile and character.Y_Tile == Player.Y_Tile then
			character.Facing = "left"
			Create_Projectile(character, character.Facing, ITEM_DOCUMENTATION[character.Weapon_ID].Category ,character.Weapon_ID, false)				
		elseif character.X_Tile < Player.X_Tile and character.Y_Tile == Player.Y_Tile then
			character.Facing = "right"
			Create_Projectile(character, character.Facing, ITEM_DOCUMENTATION[character.Weapon_ID].Category ,character.Weapon_ID, false)				
		end		
	end
end

function Moving(character, dir)
	if not character.Is_Moving then
	
		if dir == "up"	
		and character.Y_Tile > 1 
		and not SolidChecker(Field[character.X_Tile][character.Y_Tile-1]) 
		then
			character.Is_Moving = true
			character.Mov_Dir = "up"	
		elseif dir == "down"
		and character.Y_Tile < SizeY 		
		and not SolidChecker(Field[character.X_Tile][character.Y_Tile+1]) 
		then
			character.Is_Moving = true
			character.Mov_Dir = "down"				
		elseif dir == "left"
		and character.X_Tile > 1 
		and not SolidChecker(Field[character.X_Tile-1][character.Y_Tile]) 
		then 
			character.Is_Moving = true
			character.Mov_Dir = "left"	
		elseif dir == "right"	
		and character.X_Tile < SizeX 		
		and not SolidChecker(Field[character.X_Tile+1][character.Y_Tile]) 
		then
			character.Is_Moving = true
			character.Mov_Dir = "right"	
		end	
	end
	
end

--simple patrolling 
function Patrol_Simple(character)

	if character.Patrol_Storing % character.Patrol_Distance == 0 then
		if character.Mov_Dir == "up" then
			character.Mov_Dir = "down"	
		elseif character.Mov_Dir == "down" then
			character.Mov_Dir = "up"
		elseif character.Mov_Dir == "left" then
			character.Mov_Dir = "right"	
		elseif character.Mov_Dir == "right" then
			character.Mov_Dir = "left"	
		end
		
		character.Patrol_Storing = 0
	end
	
	if (character.Mov_Dir == "up" and not SolidChecker(character.X_Tile, character.Y_Tile-1))
	or (character.Mov_Dir == "down" and not SolidChecker(character.X_Tile, character.Y_Tile+1))
	or (character.Mov_Dir == "left" and not SolidChecker(character.X_Tile-1, character.Y_Tile))
	or (character.Mov_Dir == "right" and not SolidChecker(character.X_Tile+1, character.Y_Tile)) then	
		Moving(character, character.Mov_Dir)
	end
	
	character.Patrol_Storing = character.Patrol_Storing + 1	
	character.Facing = character.Mov_Dir	
	
	--return angle; just in case!
	--return 180-math.deg(angle)
end

--turning around
function Turning_Around(character)
	if game_clock%character.Turning_Spd == 0 then
		if character.Facing == "up" then character.Facing = "right" character.Sprite = 1
		elseif character.Facing == "down" then character.Facing = "left" character.Sprite = 3
		elseif character.Facing == "left" then character.Facing = "up" character.Sprite = 7
		elseif character.Facing == "right" then character.Facing = "down" character.Sprite = 9 end 	
	end
end

--walking
function Walking(character)

	local Walking_Dst = math.random(4,10)
	local Walking_Dir = MovSet[math.random(1,4)]
	
	character.Mov_Dir = Walking_Dir
	
	if character.Walking_Storing % Walking_Dst == 0 then	
		if character.Mov_Dir == "up" then
			character.Mov_Dir = "down"	
		elseif character.Mov_Dir == "down" then
			character.Mov_Dir = "up"
		elseif character.Mov_Dir == "left" then
			character.Mov_Dir = "right"	
		elseif character.Mov_Dir == "right" then
			character.Mov_Dir = "left"	
		end
		
		character.Walking_Storing = 0
	end
	
	if (character.Mov_Dir == "up" and not SolidChecker(character.X_Tile, character.Y_Tile-1))
	or (character.Mov_Dir == "down" and not SolidChecker(character.X_Tile, character.Y_Tile+1))
	or (character.Mov_Dir == "left" and not SolidChecker(character.X_Tile-1, character.Y_Tile))
	or (character.Mov_Dir == "right" and not SolidChecker(character.X_Tile+1, character.Y_Tile)) then	
		Moving(character, character.Mov_Dir)
	end
	
	character.Walking_Storing = character.Walking_Storing + 1	
	character.Facing = character.Mov_Dir		
end

--dead (kinda special behaviour; this function declares the properties to make the character dead)
function Kill_Character(character)
	character.Is_Moving = false
end