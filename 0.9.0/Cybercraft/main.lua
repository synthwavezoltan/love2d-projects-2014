--[[
------------------------------------------------------------
------------------------------------------------------------
					CYBERCRAFT
			Love2D-based 2D RPG game engine
			  by Zoltán "Katamori" Schmidt
------------------------------------------------------------
------------------------------------------------------------					
]]

require 'gui'
require 'keybinding'
require 'physics'
require 'character_functions'
require 'character_behaviour'
require 'projectile_behaviour'
require 'items'
require 'texts'
require 'item_documentation'
require 'character_documentation'
require 'charset_creation'
require 'animations'
require 'mapload'

function love.load()

--[[
------------------------------------------------------------
			CREATING AND SETTING BASIC VARIABLES									
------------------------------------------------------------				
]]

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 512, 1024
	SizeX, SizeY = 200, 200
	ScreenSizeX, ScreenSizeY = 32, 20+1
	
	--scrolling
	OffsetX, OffsetY = 9,13					--IN TILE UNITS!!
	SmoothOffsetX, SmoothOffsetY = 0,0 		-- in pixels
	OffsetBorder = 8	
	Scrolling_Value = 1						--how many tiles does it jump?
	
	--charset sizes
	CharX = 32
	CharY = CharX*2
	CharFullX, CharFullY = CharX*6, CharY*4

	--important sets	
	--character properties

	Alliances = {
	"United Forces", "Streetknight", "Church", 
	"Red Fist", "Justice Empire", "Jade Legion",
	"Golddigger", "Khali", "Shadow Army", 
	"Cloudan", "Moebius", "Android", "Not alliance-specific" }
	
	Character_Types = {
	"Test Puppet", "Elric Test", "Civilian", "Trader"}
	
	Character_States = {
	"Doing nothing", "Patrolling", "Turning around", "Walking", "Dead",}
	
	Item_Types = {
	"Weapon", "Supply", "General item",}

	MovSet = { "up", "down", "left", "right" }				--the four directions
	
	--global game variables	
	Item_Variety = 1000							--number of possible items in the game!
	
	GameOver = false 							--true if player is killed
	Immortal = false							--player health doesn't change
	
	InvWeight = 0								--sum of weight of inventory items
	
	--GUI
	MouseX, MouseY, MouseTileX, MouseTileY = 0,0,1,1	
	MouseOnField = true
	SelectorPositions = {64,168,272,376,584,688,792,896}	--calibration for hotbar icon appearance	
	
	--menus
	--inventory
	Inventory = false							--true if inventory is open
	InvX, InvY = 64, 64							--inventory menu variables
	InvListStart = 0							--where item listing begins
	InvColumnLength = 8							--length of a column
	InvColumnNum = 6							--number of columns on screen
	InvRowDist = 56
	InvColDist = 96
	InvSelectedItemID = 1						--ID of the item type that can be found at mouse position in inventory menu
	
	--dialogue screen
	DialogueActive = false						--true if player is in a conversation
	DialogX, DialogY = 64, 64					--dialogue window variables	
	Dial_Random_Text = 0						--used in case of randomly generated character speak
	
	--HUD elements
	Picset = {}
	Picset = {
		love.graphics.newImage('GUI/HUD_A.png'),
		love.graphics.newImage('GUI/SELECT.png'),	
		love.graphics.newImage('GUI/HUD_B.png'),
		love.graphics.newImage('GUI/HUD_C.png'),	
		love.graphics.newImage('GUI/GUI_HP.png'),		
	}

	--initializing font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)	

	--others
	x,y,z,i,j,k = 0,0,0,0,0,0
	
	FPS = 30		
	game_clock = 0	
	
	BrokenProjectiles = {}
	
--[[
----------------------------------------------------
			MAJOR EVENTS OF INITIALIZING						
----------------------------------------------------
]]

	
	--creating tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, TilesetX/TileSize do
		for i=1, TilesetY/TileSize do 
			Tileset[((i-1)*(TilesetX/TileSize))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--even player charset creation is here
	Charset_Creation()

	--creating Items-table
	ItemsPic = love.graphics.newImage('ITEMS.png')
	ItemsTileset = { }
	
	for j=1, TilesetX/TileSize do
		for i=1, TilesetY/TileSize do 
			ItemsTileset[((i-1)*(TilesetX/TileSize))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--creating playing field
	Field = { }
	
	for j=1,SizeX do	
		Field[j] = { }	
		for i=1, SizeY do		
			Field[j][i] = 1
		end	
	end

	--declaring tables of "high-level objects"
	Characters = {}
	Projectiles = {}
	Items = {}
	
	--defining player
	--it happens in mapload.lua as well, but before everything (I mean, it's gonna happen later)
	--I do it here, just to keep a sample about the structure of the "Player" object
	Player = {
	
	--moving and appearing
	X_Tile = OffsetX+(ScreenSizeX/2), Y_Tile = OffsetY+((ScreenSizeY-1)/2), 
	OffsetX = 0, OffsetY = 0,	
	Sprite = 1,
	SpritePos = 0,			--while player moves, this variable helps to give the feeling of moving
	Is_Moving = false, 
	Mov_Dir = "no",
	Facing = "right",
	
	In_Hand = 0,
	
	--stats
	HP = 100, Energy = 100,
	XP = 0, Lvl = 1,
	
	--abilities
	Moving_Speed = 3,						--moves 2^Mov_Spd pixels per frame		
	Inv_Capacity = 50,						--how much weight Player can carry on?
	
	--inventory
	Inventory = {}, 
	Hotbar = {0,0,0,0,0,0,0,0},
	Selected_Slot = 1
	}
	
	--inventory size = number of possible items
	for i=1, Item_Variety do
		table.insert(Player.Inventory, 0)
	end
	
	--creating a sample map
	SampleMap()

end





function love.draw()
	--appearing stuff; all these functions are explained in gui.lua
	Display_Textures()
	Display_HUD()

	if Inventory then Display_Inventory_Menu() end
	if DialogueActive then Display_Dialogue(Characters[Player_Neighbour()]) end
	
	--random text
	love.graphics.print(math.floor(FPS),0,16)
	
	if Player_Neighbour() ~= 0 and not DialogueActive then 
		if Characters[Player_Neighbour()].State == 5 then love.graphics.print("This "..Characters[Player_Neighbour()].Type.." is dead.",32,116)
		else love.graphics.print("Talk to "..Characters[Player_Neighbour()].Type,32,116) end
	end
end




function love.update(dt)

	--key actions
	Repeated_Key_Behaviour() 

	--"constants"
	FPS = 1/dt
	
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	MouseTileX, MouseTileY = 1+math.floor((MouseX-OffsetX)/TileSize), 1+math.floor((MouseY-OffsetY)/TileSize)	
	
	if Inventory 
	and MouseX > InvX+32 and MouseY > InvY+32 
	and MouseX < 32+InvX+(InvColDist*InvColumnNum) and MouseY < 32+InvY+(InvColDist*InvColumnLength)
	and (MouseX-32-InvX)%InvColDist < TileSize and (MouseY-32-InvY)%InvRowDist < TileSize then
	
	--[[
	number of column: 1+math.floor((MouseX-32-InvX)/InvColDist)
	last element of columns: (1+math.floor((MouseX-32-InvX)/InvColDist))*InvColumnLength
	number of row: (1+math.floor((MouseY-32-InvY)/InvRowDist))
	]]
	
	InvSelectedItemID = (math.floor((MouseX-32-InvX)/InvColDist))*InvColumnLength + (1+math.floor((MouseY-32-InvY)/InvRowDist)) + InvListStart
	
	end
	
	--feeling of moving on player
	if Player.Is_Moving then Player_Anim() end
	
	--count inventory weight
	InvWeight = 0
	
	for i=1, #Player.Inventory do
		if ITEM_DOCUMENTATION[i] ~= nil then
			InvWeight = InvWeight + (Player.Inventory[i]*ITEM_DOCUMENTATION[i].Weight)
		end
	end
	
	--inhand item is always the selected hotbar item
	Player.In_Hand = Player.Hotbar[Player.Selected_Slot]
	
	--gameclock
	game_clock = game_clock + 1	
	
	--mouse detecting
	if MouseY < 50 and (MouseX < 250 or MouseX > 915) then
		MouseOnField = false
	elseif true then
		MouseOnField = true
	elseif true then
		MouseOnField = true
	else
		MouseOnField = true
	end
	
	--happens if certain menus are not opened
	if not Inventory and not DialogueActive then
	
		--turning
		WatchMousePlayer()
		
		--handle characters
		for i=#Characters, 1, -1 do
		
			--kill chars with 0 HP	
			if Characters[i].HP < 1 then Characters[i].State = 5 end
			
			--move all the chars
			if not Characters[i].Is_Moving then
				--phase 1: activate their behaviour
				Declare_Behaviour(Characters[i])
			else
				--phase 2: smooth graphic moving
				SmoothMoving(false, Characters[i], Characters[i].Mov_Dir, Characters[i].Mov_Spd)			
			end	
			
			--move char face properly
			if Characters[i].State ~= 1 then FacingToMoving(Characters[i]) end

			--character animation
			Char_Anim(Characters[i])
			
		end		


		--doing smooth player moving 
		if Player.Is_Moving then
			SmoothMoving(true, Characters[1], Player.Mov_Dir, math.pow(2, Player.Moving_Speed))
			
			--item behaviour: pick items if standing above
			for i=#Items, 1, -1  do
				ItemBehaviour(i)
			end
			
		end
		
		BrokenProjectiles = {}
		
		--smooth moving of projectiles
		for i=1, #Projectiles do

			if #Projectiles ~= 0 then	
				SmoothMoving(false, Projectiles[i], Projectiles[i].Mov_Dir, Projectiles[i].Mov_Spd)
				
				--set orientation of projectiles
				if Projectiles[i].Mov_Dir == "down" or Projectiles[i].Mov_Dir == "up" then
					Projectiles[i].Sprite = 2
				elseif Projectiles[i].Mov_Dir == "left" or Projectiles[i].Mov_Dir == "right" then
					Projectiles[i].Sprite = 1				
				else
					Projectiles[i].Sprite = 3					
				end
				
				--checks if a projectile is already hit something
				Break_Projectile(Projectiles[i], i)
			end
			
			--adds the unused projectiles to a set
			if Projectiles[i].Mov_Dir == "DELETE ME" then table.insert(BrokenProjectiles, i) end

		end	
		
		--deleting unused projectiles
		for i=#BrokenProjectiles,1, -1 do
			table.remove(Projectiles, BrokenProjectiles[i])
		end
	
	end
end


--search integer in integer-table
function Search_in_table(int_table, number)
	
	local inIt = false
	
	for i=1, #int_table do
		if number == int_table[i] then	
			inIt = true
			break
		else
			inIt = false
		end
	end

		return inIt
end





































--possibly useful functions
function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




