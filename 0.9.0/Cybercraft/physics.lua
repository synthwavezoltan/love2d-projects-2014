SolidTiles = 
{2, 3, 4, 5, 
18}

function SolidChecker(x,y)

	local isSolid = false
	
	--gate 1: it's on SolidTiles list
	for i=1, #SolidTiles do
		if Field[x][y] == SolidTiles[i] then	
			isSolid = true
			break
		else
			isSolid = false
		end
	end
	
	--gate 2: there's a character on it	
	if isSolid == false then
		for i=1, #Characters do
			if Characters[i].X_Tile == x and Characters[i].Y_Tile == y and Characters[i].State ~= 5 then
				isSolid = true
				break
			else
				isSolid = false
			end
		end
	end
	
	--gate 3: there's Player on it
	if isSolid == false then
		if Player.X_Tile == x and Player.Y_Tile == y and not GameOver then
			isSolid = true
		else
			isSolid = false
		end	
	end
	
	return isSolid
end

