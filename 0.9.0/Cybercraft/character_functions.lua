--all those function that are used by characters (or Player) and not defined among "states"

--PLAYER FUNCTIONS
--player turns toward mouse cursor
function WatchMousePlayer()
	
	--count mouse-player-horizon angle
	local angle = math.atan2(((Player.X_Tile-1-OffsetX)*TileSize)+Player.OffsetX+(TileSize/2)-MouseX, 
							((Player.Y_Tile-1-OffsetY)*TileSize)+Player.OffsetY+(TileSize/2)-TileSize-MouseY)
	
	--change player sprite
	if 180-math.deg(angle) > 315 or 180-math.deg(angle) < 45 then
		Player.Sprite = 10 - Player.SpritePos 
		Player.Facing = "down" 
	elseif 180-math.deg(angle) < 315 and 180-math.deg(angle) > 225 then
		Player.Sprite = 2 - Player.SpritePos 
		Player.Facing = "right"
	elseif 180-math.deg(angle) < 225 and 180-math.deg(angle) > 135 then
		Player.Sprite = 8 - Player.SpritePos 
		Player.Facing = "up"
	elseif 180-math.deg(angle) < 135 and 180-math.deg(angle) > 45 then
		Player.Sprite = 4 - Player.SpritePos 
		Player.Facing = "left"
	end
end

--returns ID of character if there's one next to player
function Player_Neighbour()

	local id = 0
	
	for  i=1, #Characters do
		if (Player.Facing == "up" and Characters[i].X_Tile == Player.X_Tile and Characters[i].Y_Tile == Player.Y_Tile-1)
		or (Player.Facing == "down" and Characters[i].X_Tile == Player.X_Tile and Characters[i].Y_Tile == Player.Y_Tile+1)
		or (Player.Facing == "left" and Characters[i].X_Tile == Player.X_Tile-1 and Characters[i].Y_Tile == Player.Y_Tile)
		or (Player.Facing == "right" and Characters[i].X_Tile == Player.X_Tile+1 and Characters[i].Y_Tile == Player.Y_Tile) 
		then id = i break else id = 0 end
	end

	return id
end






--CHARACTER FUNCTIONS
--creates a character based on the documentation
function CreateChar(typenum)
	
	table.insert(Characters,{ID = typenum})		
	for k, v in pairs(CHARACTER_DOCUMENTATION[Characters[#Characters].ID]) do
		Characters[#Characters][k] = v
	end			

end

--stops the moving of a character
function Stop(character)
	character.Is_Moving = false
	character.Mov_Dir = "no"
end

--activates function to behaviours; this is the heart of the behaviour system
function Declare_Behaviour(character)
	if character.State == 2 then Patrol_Simple(character) 
		if character.Weapon_ID ~= nil and game_clock%1 == 0 then Shoot_On_Sight(character) end
	elseif character.State == 3 then Turning_Around(character)
	elseif character.State == 4 then Walking(character)
	elseif character.State == 5 then Kill_Character(character) end	
end

--change sprite properly to character facing
function FacingToMoving(character)
	if game_clock%50<25 then
			if character.Facing == "up" then character.Sprite = 7 elseif character.Facing == "down" then character.Sprite = 9
		elseif character.Facing == "left" then character.Sprite = 3 elseif character.Facing == "right" then character.Sprite = 1 end 
	else
			if character.Facing == "up" then character.Sprite = 8 elseif character.Facing == "down" then character.Sprite = 10
		elseif character.Facing == "left" then character.Sprite = 4 elseif character.Facing == "right" then character.Sprite = 2 end 
	end
end

--smooth moving
function SmoothMoving(playermoving, character, direction, speed)
	
	if playermoving then
		--smooth moving for player
		if (math.abs(Player.OffsetX) == TileSize or math.abs(Player.OffsetY) == TileSize)			--if either the player or
		or (math.abs(SmoothOffsetX) == TileSize or math.abs(SmoothOffsetY) == TileSize)	then		--the screen scolled a tile
			Player.Is_Moving = false																--stop moving and then
			if direction == 'up' then 																--for every directions
				Player.OffsetY = 0 										--change player location
				Player.Y_Tile = Player.Y_Tile - 1 
				if Player.Y_Tile == OffsetY + OffsetBorder 				--if player is edge of screen
				and OffsetY > 0 then									--and within boundaries
					SmoothOffsetY = 0									--then move camera one unit away
					OffsetY = OffsetY -Scrolling_Value 
				end	
			elseif direction == 'down' then 
				Player.OffsetY = 0 
				Player.Y_Tile = Player.Y_Tile + 1
				if Player.Y_Tile == OffsetY + ScreenSizeY - OffsetBorder
				and OffsetY < SizeY - ScreenSizeY then 
					SmoothOffsetY = 0				
					OffsetY = OffsetY +Scrolling_Value 
				end	
			elseif direction == 'left' then
				Player.OffsetX = 0 
				Player.X_Tile = Player.X_Tile - 1
				if Player.X_Tile == OffsetX + OffsetBorder 			
				and OffsetX > 0 then								
					SmoothOffsetX = 0
					OffsetX = OffsetX -Scrolling_Value 
				end	
			elseif direction == 'right' then 
				Player.OffsetX = 0 
				Player.X_Tile = Player.X_Tile + 1
				if Player.X_Tile == OffsetX + ScreenSizeX - OffsetBorder 
				and OffsetX < SizeX - ScreenSizeX then 
					SmoothOffsetX = 0
					OffsetX = OffsetX +Scrolling_Value 
				end					
			end		
		else																--if player is within a tile										
				
			if direction == 'up' then 										--then for every directions
				if Player.Y_Tile-1 == OffsetY + OffsetBorder 				--check if player is near screen edge
				and OffsetY > 0 then		
					SmoothOffsetY = SmoothOffsetY + speed					--if so, then scroll screen
				else
					Player.OffsetY = Player.OffsetY - speed 				--otherwise scroll player
				end
			elseif direction == 'down' then 
				if Player.Y_Tile+1 == OffsetY + ScreenSizeY - OffsetBorder 
				and OffsetY < SizeY - ScreenSizeY then 
					SmoothOffsetY = SmoothOffsetY - speed
				else
					Player.OffsetY = Player.OffsetY + speed  
				end	
			elseif direction == 'left' then 
				if Player.X_Tile-1 == OffsetX + OffsetBorder 			
				and OffsetX > 0 then		
					SmoothOffsetX = SmoothOffsetX + speed
				else
					Player.OffsetX = Player.OffsetX - speed 			
				end
			elseif direction == 'right' then 		
				if Player.X_Tile+1 == OffsetX + ScreenSizeX - OffsetBorder 
				and OffsetX < SizeX - ScreenSizeX
				then
					SmoothOffsetX = SmoothOffsetX - speed
				else
					Player.OffsetX = Player.OffsetX + speed 
				end
			end		
		end	
	
	elseif character.State ~= 5 then
		--smooth moving for characters
		if math.abs(character.OffsetX) == TileSize or math.abs(character.OffsetY) == TileSize then
			character.Is_Moving = false
			if direction == 'up' then 
				character.OffsetY = 0 
				character.Y_Tile = character.Y_Tile - 1 	
			elseif direction == 'down' then 
				character.OffsetY = 0 
				character.Y_Tile = character.Y_Tile + 1		
			elseif direction == 'left' then 
				character.OffsetX = 0 
				character.X_Tile = character.X_Tile - 1					
			elseif direction == 'right' then 
				character.OffsetX = 0 
				character.X_Tile = character.X_Tile + 1 			
			end		
		else
			if direction == 'up' then 
				character.OffsetY = character.OffsetY - speed 
			elseif direction == 'down' then 
				character.OffsetY = character.OffsetY + speed  
			elseif direction == 'left' then 
				character.OffsetX = character.OffsetX - speed 
			elseif direction == 'right' then 
				character.OffsetX = character.OffsetX + speed 
			end				
		end	
	end

end