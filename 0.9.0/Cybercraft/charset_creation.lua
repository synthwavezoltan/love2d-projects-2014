--[[
This code is put here to make main.lua more readable.

------------------------------------------------------------------------------------------------
GENERAL DESCRIPTION OF SPRITES:
- number means that if you "read" the sprites in order, then
  the number 1 means the first one, 2 means the second one, etc.
- 01: moving while facing right, sprite 1 / standing while facing right
- 02: moving while facing right, sprite 2
- 03: moving while facing left, sprite 1 / standing while facing left
- 04: moving while facing left, sprite 2
- 05: UNOCCUPIED YET
- 06: UNOCCUPIED YET
- 07: moving while facing up, sprite 1 / standing while facing up
- 08: moving while facing up, sprite 2
- 09: moving while facing down, sprite 1 / standing while facing down
- 10: moving while facing down, sprite 2
- 11: UNOCCUPIED YET
- 12: UNOCCUPIED YET
- 13: UNOCCUPIED YET
- 14: UNOCCUPIED YET
- 15: UNOCCUPIED YET
- 16: UNOCCUPIED YET
- 17: UNOCCUPIED YET
- 18: UNOCCUPIED YET
- 19: UNOCCUPIED YET
- 20: UNOCCUPIED YET
- 21: UNOCCUPIED YET
- 22: UNOCCUPIED YET
- 23: UNOCCUPIED YET
- 24: dead character
------------------------------------------------------------------------------------------------
]]

function LoadCharsets(CharName, ImageName)
	CharPic[CharName] = ImageName
	Charset[CharName] = {}	
	for j=1, CharFullX/CharX do
		for i=1, CharFullY/CharY do 
			Charset[CharName][((i-1)*(CharFullX/CharX))+j] = love.graphics.newQuad((j-1)*CharX, (i-1)*CharY, CharX, CharY, CharFullX, CharFullY)
		end
	end	
end

--the loading itself
function Charset_Creation()

--CHARACTER CHARSETS
CharPic = {}	
Charset = {}	

LoadCharsets("Test Puppet", love.graphics.newImage('Charset/TESTPUPPET.png'))
LoadCharsets("Elric Test", love.graphics.newImage('Charset/ELRICTEST.png'))
LoadCharsets("Civilian", love.graphics.newImage('Charset/CIVILIAN.png'))



--PLAYER CHARSET
LoadCharsets("Player", love.graphics.newImage('Charset/PLAYER.png'))



--PROJECTILE CHARSETS
CharPic["Special weapon"] = love.graphics.newImage('Charset/SPECIAL_SHOT.png')	
Charset["Special weapon"] = {}	
for j=1, 6 do
	for i=1, 6 do 
		Charset["Special weapon"][((i-1)*(CharFullX/CharX))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TileSize*6, TileSize*6)
	end
end	








end


