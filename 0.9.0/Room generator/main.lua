function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 32, 24
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0

	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = 1		
		end
		
	end
	
	--calling function	
	step = 1 			--number of iterations	
	Rooms = {}		--container of room coordinates	
	
	for i=1, 100 do Rooms[i] = { } end
	
	RoomGenerator(Field, 10, 10)
	
end

function love.draw()

	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPic, Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
		end
	end
	
	love.graphics.print(#Rooms,60,0)
	
	for i=1, #Rooms do
		for j=1, #Rooms[i] do 
			--love.graphics.print(Rooms[i][j].startX..", "..Rooms[i][j].startY, (Rooms[i][j].startX-1)*TileSize, (Rooms[i][j].startY-1)*TileSize)
			--love.graphics.print(Rooms[i][j].finishX..", "..Rooms[i][j].finishY, (Rooms[i][j].finishX-1)*TileSize, (Rooms[i][j].finishY-1)*TileSize)			
		end

	end

	
end

function love.update(dt)

end


function RoomGenerator(Array, Min_X, Min_Y)
	
	local Wall = 0 			--coordinates of wall where it must be put
	
	--1st step
	if step == 1 then
		Wall = math.random(2, SizeX-1)					--set Wall
		
		for i=1, SizeY do Array[Wall][i] = 4 end		--fill with Wall
		
		Array[Wall][math.random(SizeY)] = 6				--put down a door
	
		Rooms[1][1] = { startX = 1, startY = 1, finishX = Wall-1, finishY = SizeY }
		Rooms[1][2] = { startX = Wall + 1, startY = 1, finishX = SizeX, finishY = SizeY }	
	
	else
	
		for j=1, #Rooms[step-1] do
		
			if Rooms[step-1][j] ~= nil 
			and Rooms[step-1][j].finishY ~= nil then
			
				--horizontal walls		
				if step%2 == 0 then
				
					--if large enough
					if Rooms[step-1][j].finishY - Rooms[step-1][j].startY >= Min_Y then
						Wall = math.random(Rooms[step-1][j].startY + 1, Rooms[step-1][j].finishY - 1)
						
						for k=Rooms[step-1][j].startX, Rooms[step-1][j].finishX do
							Array[k][Wall] = 4
						end
						
						Rooms[step][(j*2)-1] = 
						{ startX = Rooms[step-1][j].startX, startY = Rooms[step-1][j].startY, 
						finishX = Rooms[step-1][j].finishX, finishY = Wall - 1 }
						
						Rooms[step][j*2] = 
						{ startX = Rooms[step-1][j].startX, startY = Wall + 1, 
						finishX = Rooms[step-1][j].finishX, finishY = Rooms[step-1][j].finishY }
					else
						if Rooms[step-1][j].finishX - Rooms[step-1][j].startX >= Min_X then
							--else send forward rooms
							Rooms[step][(j*2)-1] = 
							{ startX = Rooms[step-1][j].startX, startY = Rooms[step-1][j].startY, 
							finishX = Rooms[step-1][j].finishX, finishY = Rooms[step-1][j].finishY }
							
							Rooms[step][j*2] = 
							{ startX = 0, startY = 0, 
							finishX = 0, finishY = 0 }
						end
					end
				
				--vertical walls
				else
				
					--if large enough
					if Rooms[step-1][j].finishX - Rooms[step-1][j].startX >= Min_X then
						Wall = math.random(Rooms[step-1][j].startX + 1, Rooms[step-1][j].finishX - 1)
						
						for k=Rooms[step-1][j].startY, Rooms[step-1][j].finishY do
							Array[Wall][k] = 4
						end
						
						Rooms[step][(j*2)-1] = 
						{ startX = Rooms[step-1][j].startX, startY = Rooms[step-1][j].startY, 
						finishX = Wall - 1, finishY = Rooms[step-1][j].finishY }
						
						Rooms[step][j*2] = 
						{ startX = Wall + 1, startY = Rooms[step-1][j].startY, 
						finishX = Rooms[step-1][j].finishX, finishY = Rooms[step-1][j].finishY }
					else
						if Rooms[step-1][j].finishY - Rooms[step-1][j].startY >= Min_Y then
							--else send forward rooms
							Rooms[step][(j*2)-1] = 
							{ startX = Rooms[step-1][j].startX, startY = Rooms[step-1][j].startY, 
							finishX = Rooms[step-1][j].finishX, finishY = Rooms[step-1][j].finishY }
							
							Rooms[step][j*2] = 
							{ startX = 0, startY = 0, 
							finishX = 0, finishY = 0 }
						end
					end			
				end
			end
		end
	end
	
	step = step + 1
	while step <=100 do RoomGenerator(Field, 4, 4) end
end

function love.keypressed(key)
	
	if key == 'escape'
	then
		love.event.push('quit')
	end

	if key == ' '
	then
		for j=1,SizeX do
			for i=1, SizeY do
				Field[j][i] = 1		
			end
		end
		
		step = 1
		for i=1, 20 do Rooms[i] = { } end
		
		RoomGenerator(Field, 10, 10) 	
	end
end












function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




