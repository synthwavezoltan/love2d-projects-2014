require 'keybinding'
require 'physics'
require 'mouse'
require 'gui'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 512, 1024
	
	ScreenSizeX, ScreenSizeY = 32, 24
	SizeX, SizeY = 64, 48
	OffsetX, OffsetY = 0,0
	SmoothOffsetX, SmoothOffsetY = 0,0
	
	OffsetBorder = 8
	Scrolling_Value = 1
	speed = 4
	
	MouseX, MouseY = 0,0
	
	SelectorX, SelectorY = 0,0
	SelectedTileX, SelectedTileY = 0,0
	
	--general variables
	x,y,z,i,j,k = 0,0,0,0,0,0

	--game variables
	game_clock = 0
	stand_clock = 0
	
	--creating tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, TilesetX/TileSize do
		for i=1, TilesetY/TileSize do 
			Tileset[((i-1)*(TilesetX/TileSize))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			if i==1 or j==1 or i==SizeY or j==SizeX then Field[j][i] = 3 else Field[j][i] = 1 end			
		end
		
	end
	
	--random field
	
	for i=1, 200 do Field[math.random(2, SizeX-1)][math.random(2,SizeY-1)] = 2 end
	
	-- player
	Player = {
	X_Tile = 16, Y_Tile = 2, Sprite = 35,
	OffsetX = 0, OffsetY = 0,
	Is_Moving = false, Mov_Dir = 'no',
	Is_Falling = false,
	Is_Hanging = false,
	
	--PlayerRelativeXY = on-window XY of player, declared in love.update(dt)
	RelativeX = 0, RelativeY = 0,
	
	Stored_Block = 1
	}
	
end

function love.draw()


	for j=0,ScreenSizeX+2 do
		for i=0, ScreenSizeY+2 do
			if OffsetX+j > 0 and OffsetY+i >0 and OffsetX+j <= SizeX and OffsetY+i <= SizeY then
				love.graphics.draw(TilesetPic, Tileset[Field[OffsetX+j][OffsetY+i]], 
				SmoothOffsetX+(j-1)*TileSize, 
				SmoothOffsetY+(i-1)*TileSize)
			end
		end
	end
	
	love.graphics.draw(TilesetPic, Tileset[Player.Sprite], Player.RelativeX, Player.RelativeY)
	
	if not Player.Is_Moving and not Player.Is_Falling and stand_clock > 1 then 
		Display_Selector()	
		love.graphics.print(SelectedTileX.." "..SelectedTileY, (SelectedTileX-OffsetX-1)*TileSize, (SelectedTileY-OffsetY-1)*TileSize)
	end
		
	Display_Suspension()
	
	love.graphics.print(stand_clock, 0, 0)

end

function love.update(dt)
	--constants
	game_clock = game_clock + 1
	
	if Player.Is_Moving 
	then stand_clock = 0
	else stand_clock = stand_clock + 1 end
	
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	
	Player.RelativeX = ((Player.X_Tile-1-OffsetX)*TileSize)+Player.OffsetX
	Player.RelativeY = ((Player.Y_Tile-1-OffsetY)*TileSize)+Player.OffsetY
	
	if not Player.Is_Moving then
	
		if SelectedTileX%TileSize < TileSize/2 then
			SelectedTileX = math.ceil((SelectorX - SmoothOffsetX)/TileSize)+OffsetX+1
		else
			SelectedTileX = math.floor((SelectorX - SmoothOffsetX)/TileSize)+OffsetX+1	
		end
		
		if SelectedTileY%TileSize < TileSize/2 then
			SelectedTileY = math.ceil((SelectorY - SmoothOffsetY)/TileSize)+OffsetY+1
		else
			SelectedTileY = math.floor((SelectorY - SmoothOffsetY)/TileSize)+OffsetY+1
		end
		
	end
	
	--keys
	Repeated_Key_Behaviour() 	
	
	--gravity of some kind
	if not Player.Is_Hanging and not SolidChecker(Player.X_Tile, Player.Y_Tile+1) then
		Player.Is_Moving = true
		Player.Mov_Dir = "down"
			
		Player.Is_Falling = true
	else
		Player.Is_Falling = false
	end
	
	--smooth moving function call
	if Player.Is_Moving then
		SmoothMoving_Player(Player.Mov_Dir, speed)
	end
	
end

function SmoothMoving_Player (direction, speed)
	if (math.abs(Player.OffsetX) == TileSize or math.abs(Player.OffsetY) == TileSize)			--if either the player or
	or (math.abs(SmoothOffsetX) == TileSize or math.abs(SmoothOffsetY) == TileSize)	then		--the screen scolled a tile
		Player.Is_Moving = false																--stop moving and then
		if direction == 'up' then 																--for every directions
			Player.OffsetY = 0 										--change player location
			Player.Y_Tile = Player.Y_Tile - 1 
			if Player.Y_Tile == OffsetY + OffsetBorder 				--if player is edge of screen
			and OffsetY > 0 then									--and within boundaries
				SmoothOffsetY = 0									--then move camera one unit away
				OffsetY = OffsetY -Scrolling_Value 
			end	
		elseif direction == 'down' then 
			Player.OffsetY = 0 
			Player.Y_Tile = Player.Y_Tile + 1
			if Player.Y_Tile == OffsetY + ScreenSizeY - OffsetBorder
			and OffsetY < SizeY - ScreenSizeY then 
				SmoothOffsetY = 0				
				OffsetY = OffsetY +Scrolling_Value 
			end	
		elseif direction == 'left' then
			Player.OffsetX = 0 
			Player.X_Tile = Player.X_Tile - 1
			if Player.X_Tile == OffsetX + OffsetBorder 			
			and OffsetX > 0 then								
				SmoothOffsetX = 0
				OffsetX = OffsetX -Scrolling_Value 
			end	
		elseif direction == 'right' then 
			Player.OffsetX = 0 
			Player.X_Tile = Player.X_Tile + 1
			if Player.X_Tile == OffsetX + ScreenSizeX - OffsetBorder 
			and OffsetX < SizeX - ScreenSizeX then 
				SmoothOffsetX = 0
				OffsetX = OffsetX +Scrolling_Value 
			end					
		end		
	else																--if player is within a tile										
			
		if direction == 'up' then 										--then for every directions
			if Player.Y_Tile-1 == OffsetY + OffsetBorder 				--check if player is near screen edge
			and OffsetY > 0 then		
				SmoothOffsetY = SmoothOffsetY + speed					--if so, then scroll screen
			else
				Player.OffsetY = Player.OffsetY - speed 				--otherwise scroll player
			end
		elseif direction == 'down' then 
			if Player.Y_Tile+1 == OffsetY + ScreenSizeY - OffsetBorder 
			and OffsetY < SizeY - ScreenSizeY then 
				SmoothOffsetY = SmoothOffsetY - speed
			else
				Player.OffsetY = Player.OffsetY + speed  
			end	
		elseif direction == 'left' then 
			if Player.X_Tile-1 == OffsetX + OffsetBorder 			
			and OffsetX > 0 then		
				SmoothOffsetX = SmoothOffsetX + speed
			else
				Player.OffsetX = Player.OffsetX - speed 			
			end
		elseif direction == 'right' then 		
			if Player.X_Tile+1 == OffsetX + ScreenSizeX - OffsetBorder 
			and OffsetX < SizeX - ScreenSizeX
			then
				SmoothOffsetX = SmoothOffsetX - speed
			else
				Player.OffsetX = Player.OffsetX + speed 
			end
		end		
	end	
end




























--possibly useful functions
function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




