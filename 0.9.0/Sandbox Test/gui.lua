function Display_Selector()

	--selected block display and calculation
	--offsetted one after the player if he moves; no change if offsetted value equals to player X, Y
	if SelectBlock() > 45 and SelectBlock() < 135 then
		if Player.Is_Moving then
			if Player.Mov_Dir == "up" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX + TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY - TileSize			
			elseif Player.Mov_Dir == "down" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX + TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY + TileSize			
			elseif Player.Mov_Dir == "left" then											--NO CHANGE!
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX + TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY			
			elseif Player.Mov_Dir == "right" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX + (TileSize*2)
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY				
			end
		else
			SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX + TileSize
			SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY		
		end
	elseif SelectBlock() > 135 and SelectBlock() < 225 then
		if Player.Is_Moving then
			if Player.Mov_Dir == "up" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY - (TileSize*2)			
			elseif Player.Mov_Dir == "down" then											--no change
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY - TileSize						
			elseif Player.Mov_Dir == "left" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX - TileSize			
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY - TileSize						
			elseif Player.Mov_Dir == "right" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX + TileSize			
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY - TileSize							
			end
		else
			SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX
			SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY - TileSize					
		end
	elseif SelectBlock() > 225 and SelectBlock() < 315 then
		if Player.Is_Moving then
			if Player.Mov_Dir == "up" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX - TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY - TileSize			
			elseif Player.Mov_Dir == "down" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX - TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY + TileSize			
			elseif Player.Mov_Dir == "left" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX - (TileSize*2)
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY			
			elseif Player.Mov_Dir == "right" then											--no change										
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX - TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY				
			end
		else
			SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX - TileSize
			SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY		
		end
	elseif SelectBlock() > 315 or SelectBlock() < 45 then
		if Player.Is_Moving then
			if Player.Mov_Dir == "up" then											--no change										
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY + TileSize					
			elseif Player.Mov_Dir == "down" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY + (TileSize*2)					
			elseif Player.Mov_Dir == "left" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX - TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY + TileSize					
			elseif Player.Mov_Dir == "right" then
				SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX + TileSize
				SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY + TileSize						
			end
		else
			SelectorX = Player.RelativeX - Player.OffsetX + SmoothOffsetX
			SelectorY = Player.RelativeY - Player.OffsetY + SmoothOffsetY + TileSize		
		end
	end
	
	love.graphics.draw(TilesetPic, Tileset[19], SelectorX, SelectorY)
	
end

function Display_Suspension()

	if Player.Is_Hanging then		
		a = 1		
		while not SolidChecker(Player.X_Tile, Player.Y_Tile-a) do
			love.graphics.draw(TilesetPic, Tileset[511], Player.RelativeX, Player.RelativeY - (a*TileSize))
			a = a+1
		end
	end	

end