
function Repeated_Key_Behaviour()

	--player moving (only left & right)
	--if not moving yet, if not falls by gravity, and if not hanged
	if not Player.Is_Falling and not Player.Is_Moving and not Player.Is_Hanging then
	
		if love.keyboard.isDown("a")and not SolidChecker(Player.X_Tile-1, Player.Y_Tile) then
			Player.Is_Moving = true
			Player.Mov_Dir = "left"	
		elseif love.keyboard.isDown("d") and not SolidChecker(Player.X_Tile+1, Player.Y_Tile) then
			Player.Is_Moving = true
			Player.Mov_Dir = "right"	
		end	
		
	end

	--player hanged moving (only up and down)	
	if Player.Is_Hanging and not Player.Is_Moving then

		if love.keyboard.isDown("q")and not SolidChecker(Player.X_Tile, Player.Y_Tile-1) then
			Player.Is_Moving = true
			Player.Mov_Dir = "up"	
		elseif love.keyboard.isDown("e") and not SolidChecker(Player.X_Tile, Player.Y_Tile+1) then
			Player.Is_Moving = true
			Player.Mov_Dir = "down"	
		end
	end
end

function love.keypressed(key)

	--hanging
	if key == "w" then Player.Is_Hanging = not Player.Is_Hanging end

end