function love.mousepressed(x,y,button)
	
	--click events of removing/placing new blocks
	if not Player.Is_Moving then
		if button == "l" then

			Field[SelectedTileX][SelectedTileY] = Player.Stored_Block
			Player.Stored_Block = 1

		elseif button == "r" then
		
			Player.Stored_Block = Field[SelectedTileX][SelectedTileY]
			Field[SelectedTileX][SelectedTileY] = 1
			
		end
	end

end

function SelectBlock()

	--angle of player-mouse line with X=0 line
	--angle = math.deg(math.atan2(MouseX-Player.X,Player.Y-MouseY))
	--PlayerRelativeXY = on-window XY of player
	local PlayerRelativeX = ((Player.X_Tile-1-OffsetX)*TileSize)+Player.OffsetX+16
	local PlayerRelativeY = ((Player.Y_Tile-1-OffsetY)*TileSize)+Player.OffsetY+16
	
	local angle = math.deg(math.atan2(MouseX-PlayerRelativeX, MouseY-PlayerRelativeY))

	if angle>=360 then angle = angle-360 elseif angle<=0 then angle = angle+360 end
	
	return angle
end