SolidTiles = 
{2, 3, 4, 5, 
18}

function SolidChecker(x,y)

	local isSolid = false
	
	--gate 1: it's on SolidTiles list
	for i=1, #SolidTiles do
		if Field[x][y] == SolidTiles[i] then	
			isSolid = true
			break
		else
			isSolid = false
		end
	end
		
	--gate 2: there's Player on it
	if isSolid == false then
		if Player.X_Tile == x and Player.Y_Tile == y then
			isSolid = true
		else
			isSolid = false
		end	
	end
	
	return isSolid
end