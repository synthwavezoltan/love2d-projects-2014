--step 1: generate the main shape of the stations
function StationGenerator()
	
	local x, y = math.random(1,SizeX), math.random(1,SizeY)	
	
	--firstly: put down starting spots
	BestCandidate_Distribution()
	
	--then for every station colors
	for i=32, 25, -1 do

		
		--the others
		if AreThereAnyPlacesYet(i) then		
			for j=1, 1500 do				
				--choose a tile where I can put a new one down
				x, y = math.random(1,SizeX), math.random(1,SizeY)
			
				while Field[x][y] ~= 1 or CountNeighbours(x,y, i, Field)==0 do
					x, y = math.random(1,SizeX), math.random(1,SizeY)
				end
				
				Field[x][y] = i	
	
			end			
		end
		
	end	
	
	--post-processing: deleting enclaves and rough borders + drawing actual borders
	for k=2, SizeX-1 do
		for l=2, SizeY-1 do
		
			--3 or 4 neighbours => becomes the color; 1 neighbour => becomes border		
			for i=32, 25, -1 do
				if CountNeighbours(k, l, i, Field) > 2 then Field[k][l] = i 
				elseif CountNeighbours(k, l, i, Field) == 1 then Field[k][l] = 2 end
			end
		
			--border
			Neutralizer(k, l)
			
			--final step: transform every 1 into 2
			if Field[k][l] == 1 then Field[k][l] = 2 end
			
		end
	end		
	
end


--step 2: generate railways on them
function RailGenerator()
	
	--for each stations
	for i=25, 32 do
		
		--step 1: put a rail to the northernmost-westernmost corner of a certain station
		--start iteration from [1,1] until the first "i" tile
		x, y = 0, 0
		
		for k=2, SizeX-1 do
			for l=2, SizeY-1 do 
				--accept the first "i" tile around which there's enough place
				--to generate that FUCKIN RAILWAY PROPERLY DAMNIT!
				if Field[l][k] == i and Field[l][k+1] == i 
				and Field[l][k+2] == i and Field[l][k+3] == i 
				and l>3 then
					if x == 0 and y == 0 then x = l-1 y = k-1 end
				end
			end			
		end
		
		table.insert(Railroad, { X = x, Y = y, Station = i-24 }) 
		
		--step 2: right and/or down
		local tempx, tempy = x+1, y+1
		local length = 0
		
		--count distance of startpoint and right border
		while Field[tempx+length][tempy] > 24 do
			length = length + 1
		end

		--if it's large enough, generate a road right
		--either way, then generate a road down
		if length > 2 then 
			
			for m=1, math.random(math.floor(length/2),length-1) do
				x = x+1
				table.insert(Railroad, { X = x, Y = y, Station = i-24 }) 
			end	

			tempx = x+1
		
		end
		
		--generating random downway
		tempx, tempy = Railroad_TrackGen("down", tempx, tempy, i, 0)	
		
		--place for the station
		--step 1: check if I have 6 places on either directions
		-- but also needs place for the train to go though...
		local stationbuild = {true, true}
		local chosen = 1
		
		for k=1, 8 do 
			stationbuild[1] = stationbuild[1] and 
							(tempx > 9 
							and GetRail(tempx-k,tempy-1) ~= 0
							and Field[tempx-k][tempy+1] == i)
			stationbuild[2] = stationbuild[2] and 
							(tempx < SizeX-9 
							and GetRail(tempx+k,tempy-1) ~= 0
							and Field[tempx+k][tempy+1] == i)
		end
		
		--step 2: choose and put down the station
		if stationbuild[1] and stationbuild[2] then chosen = math.random(1,2)
		elseif stationbuild[1] or stationbuild[2] then
			if stationbuild == 1 then chosen = 1
			elseif stationbuild == 2 then chosen = 2 end
		end
		

		if chosen == 1 then 
			for k=0, 8 do
				if k < 8 and k > 0
				and tempy+1 < SizeY
				and tempx-k > 0
				then Details[tempx-k][tempy+1] = 3 end
				
				if tempx-k > 0 then 
				Field[tempx-k][tempy] = i				
				Field[tempx-k][tempy+2] = 2 end end
				
			tempx, tempy = Railroad_TrackGen("left", tempx, tempy, i, 10)
			
		elseif chosen == 2 then
			for k=0, 8 do 			
				if k < 8 and k > 0 then Details[tempx+k][tempy+1] = 3 end
				Field[tempx-k][tempy] = i 
				Field[tempx+k][tempy+2] = 2 end
				
			tempx, tempy = Railroad_TrackGen("right", tempx, tempy, i, 10)
				
		end

		



	end
 
	--post-processing: generator has mistakes
	--I'm trying to eliminate them with this
	--[[for i=1, #Railroad do
		if Field[Railroad[i].X][Railroad[i].Y] ~= nil 
		then Field[Railroad[i].X][Railroad[i].Y] = Railroad[i].Station+24 end
		if Field[Railroad[i].X][Railroad[i].Y+1] ~= nil
		and Field[Railroad[i].X][Railroad[i].Y+1] == 2
		then Field[Railroad[i].X][Railroad[i].Y+1] = Railroad[i].Station+24 end
	end
]]

end

--final step: the details
function GenerateDetails()

end


--FUNCTION USED FOR GENERATORS:
--creating banned tiles
function Neutralizer(x, y)
	
	local colors = {}

	--add colors to a list
	table.insert(colors, Field[x-1][y])
	table.insert(colors, Field[x+1][y])
	table.insert(colors, Field[x][y-1])
	table.insert(colors, Field[x][y+1])
	
	table.insert(colors, Field[x-1][y-1])
	table.insert(colors, Field[x-1][y+1])
	table.insert(colors, Field[x+1][y-1])
	table.insert(colors, Field[x+1][y+1])	
	
	if Field[x][y] > 24 then
		if (colors[1] > 24 and Field[x][y] ~= colors[1])
		or (colors[2] > 24 and Field[x][y] ~= colors[2])
		or (colors[3] > 24 and Field[x][y] ~= colors[3])
		or (colors[4] > 24 and Field[x][y] ~= colors[4])
		or (colors[5] > 24 and Field[x][y] ~= colors[5])
		or (colors[6] > 24 and Field[x][y] ~= colors[6])
		or (colors[7] > 24 and Field[x][y] ~= colors[7])
		or (colors[8] > 24 and Field[x][y] ~= colors[8])		
		then Field[x][y] = 2 end
	end

end

--is the dimension able to spread more?
--got from a map generator I made
function AreThereAnyPlacesYet(num)

	counter = 0
	
	for j=2,SizeX-1 do
		for i=2, SizeY-1 do
			--if the chosen tile has neighbour with "num" AND it's empty yet
			if (Field[j+1][i] == num or Field[j-1][i] == num 
			or Field[j][i+1] == num or Field[j][i-1] == num)
			and Field[j][i] == 1 then
				counter = counter+1
			end
		end
	end
	
	if counter == 0 then return false else return true end
end

--railway track generator
function Railroad_TrackGen(dir, startX, startY, station, internalmin)
	
	local length = 0
	
	if dir == "up" then

		while Field[startX][startY-length] ~= 2 do
			length = length + 1
		end		
		
		local internal = 0
		
		if internalmin == 0 then
			internal = math.random(10, length)
		else
			internal = internalmin
		end
		
		for m=1, internal do		
			table.insert(Railroad, { X = startX-1, Y = startY-1-m, Station = station-24 }) 			
		end	
		
		startY = startY - internal
	
	elseif dir == "down" then
		
		while Field[startX][startY+length+1] ~= 2 do
			length = length + 1
		end		

		local internal = 0
		
		if internalmin == 0 then
			internal = math.random(10, length)
		else
			internal = internalmin
		end
		
		for m=1, internal do		
			table.insert(Railroad, { X = startX-1, Y = startY-1+m, Station = station-24 }) 			
		end	
		
		startY = startY + internal
	
	elseif dir == "left" then

		while Field[startX-length][startY] ~= 2 and Field[startX-length][startY+1] ~= 2 do
			length = length + 1
		end		

		local internal = 0
		
		if internalmin == 0 then
			internal = math.random(10, length)
		else
			internal = internalmin
		end
		
		for m=1, internal do		
			table.insert(Railroad, { X = startX-1-m, Y = startY-1, Station = station-24 }) 			
		end	
		
		startX = startX - internal
	
	elseif dir == "right" then

		while Field[startX+length][startY] ~= 2 and Field[startX+length][startY+1] ~= 2 do
			length = length + 1
		end		

		local internal = 0
		
		if internalmin == 0 then
			internal = math.random(10, length)
		else
			internal = internalmin
		end
		
		for m=1, internal do		
			table.insert(Railroad, { X = startX-1+m, Y = startY-1, Station = station-24 }) 			
		end	
		
		startX = startX + internal		
	
	end
	
	return startX, startY

end