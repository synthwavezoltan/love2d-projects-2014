require 'key'
require 'generator'

function love.load()

	math.randomseed( os.time() )
	-- base variables
	x,y,z,i,j,k,l,m = 0,0,0,0,0,0,0,0
	
	TileSize = 32
	SizeX, SizeY = 192, 192
	ScreenX, ScreenY = (1280/TileSize)-2, (720/TileSize)-2
	OffsetX, OffsetY = 0, 0
	
	timer = 0
	FPS = 0
	
	mousetilex, mousetiley = 0,0
	
	Map = false
	
	--HUD
	HUD = love.graphics.newImage('HUD.png')
	
	--font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)		
	
	--main tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	
	Tileset = {}
	local tilesetX = TilesetPic:getWidth()
	local tilesetY = TilesetPic:getHeight()
	
	for j=1, tilesetX/TileSize do
		for i=1, tilesetY/TileSize  do 
			Tileset[((i-1)*(tilesetX/TileSize))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, tilesetX, tilesetY)
		end
	end		
	
	--rails
	RailsPic = love.graphics.newImage('RAILS.png')
	local tilesetX = RailsPic:getWidth()
	local tilesetY = RailsPic:getHeight()
	
	Rails = {}
	
	for j=1, tilesetX/TileSize do
		for i=1, tilesetY/TileSize  do 
			Rails[((i-1)*(tilesetX/TileSize))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, tilesetX, tilesetY)
		end
	end		
	
	--trains, they have the size of 1x2
	TrainsPic = love.graphics.newImage('TRAINS.png')
	local tilesetX = TrainsPic:getWidth()
	local tilesetY = TrainsPic:getHeight()
	
	Trains = {}
	
	for j=1, tilesetX/TileSize do
		for i=1, tilesetY/(TileSize/2)  do 
			Trains[((i-1)*(tilesetX/TileSize))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*(TileSize*2), TileSize, TileSize*2, tilesetX, tilesetY)
		end
	end			
	
	--passengers' textures
	PassPic = love.graphics.newImage('HUMAN.png')
	Passet = {}
	for i=1, 16 do
		Passet[i] = love.graphics.newQuad((j-1)*4, 0, 4, 16, 64, 16) end
	
	--main playfield
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
				if i==1 or j==1 or i==SizeX or j==SizeY 
				then Field[j][i] = 2 else Field[j][i] = 1 end
		end
		
	end	
	
	--details above main field
	Details = {}

	for j=1,SizeX do
	
		Details[j] = { }
	
		for i=1, SizeY do
			Details[j][i] = 1
		end
		
	end	
	
	--the railroad table
	Railroad = {}


	--the train itself
	MainTrain = {
	{ X = 1, Y = 1, SmoothX = 0, SmoothY = 0, Type = 1, CurrentRail = 7 },
	{ X = 2, Y = 2, SmoothX = 0, SmoothY = 0, Type = 2, CurrentRail = 5 },
	{ X = 2, Y = 2, SmoothX = 0, SmoothY = 0, Type = 3, CurrentRail = 3 },	
	{ X = 2, Y = 2, SmoothX = 0, SmoothY = 0, Type = 2, CurrentRail = 1 },
	
	}


	
	--generating map
	StationGenerator()	

	--generating rail
	RailGenerator()
	
	--put MainTrain to rail
	MainTrain[1].X, MainTrain[1].Y = Railroad[7].X, Railroad[7].Y
	
	--train calculating
	for i=2, #MainTrain do
	
		local myrail = MainTrain[i-1].CurrentRail - 2
		MainTrain[i].CurrentRail = myrail
		MainTrain[i].X = Railroad[myrail].X
		MainTrain[i].Y = Railroad[myrail].Y

	end
	
	--passengers
	Passengers = { }
	
	--find the station of the current "dimension
	local a,b = math.random(2,SizeX-1), math.random(1,SizeY-1)	
	while Field[a][b] ~= 25 or Field[a][b] == 2 do
		a,b = math.random(2,SizeX-1), math.random(1,SizeY-1)	
	end
	
	--generate pixel coordinate for passengers from it
	for i= 1, 200 do
		Passengers[i] = { PixelX = 1, PixelY = 1, Skin = math.random(1,16) }
	end	
	
end

function love.draw()
	
	if not Map then
		
		--gameplay field
		for j=1, ScreenX do
			for i=1, ScreenY do
				if Field[j+OffsetX][i+OffsetY] == Railroad[MainTrain[1].CurrentRail].Station+24
				or Field[j+OffsetX][i+OffsetY] == 2 then
					love.graphics.draw(TilesetPic,Tileset[Field[j+OffsetX][i+OffsetY]], 32+(j-1)*TileSize, 32+(i-1)*TileSize)
				end
				
				if Details[j+OffsetX][i+OffsetY] == 3 then
					love.graphics.draw(TilesetPic,Tileset[Details[j+OffsetX][i+OffsetY]], 32+(j-1)*TileSize, 32+(i-1)*TileSize)
				end	

			end
		end
		
		--railways
		for i=1, #Railroad do 
			if Railroad[i].Station == Railroad[MainTrain[1].CurrentRail].Station then
				if i==1 or i==#Railroad or Railfixator(i) == 7
				then love.graphics.draw(TilesetPic,Tileset[9], 32+(Railroad[i].X-OffsetX)*TileSize, 32+(Railroad[i].Y-OffsetY)*TileSize)
				else love.graphics.draw(RailsPic,Rails[Railfixator(i)], 32+(Railroad[i].X-OffsetX)*TileSize, 32+(Railroad[i].Y-OffsetY)*TileSize) end
			end
		end
		
		--trainz
		for i=1, #MainTrain do
			love.graphics.draw(TrainsPic,Trains[MainTrain[i].Type], 32+((MainTrain[i].X-OffsetX)*TileSize), 32+((MainTrain[i].Y-OffsetY)*TileSize))
		end
		
		--passengers
		for i= 1, #Passengers do
			love.graphics.draw(PassPic, Passet[Passengers[i].Skin], (Passengers[i].PixelX - OffsetX)*TileSize, (Passengers[i].PixelY - OffsetY)*TileSize)
		end		
		
		--HUD
		love.graphics.draw(HUD, 0, 0)
		
		
		
	else
		--minimap
		for i=1, SizeX do
			for j=1, SizeY do
				love.graphics.draw(TilesetPic,Tileset[Field[i][j]], (i-1)*4, (j-1)*4)
			end
		end	
		
		for i=1, #Railroad do love.graphics.rectangle("fill", Railroad[i].X*4, Railroad[i].Y*4, 4, 4) end
		
		love.graphics.rectangle("fill", (MainTrain[1].X*4)-5, (MainTrain[1].Y*4)-5, 14, 14)
	end

	--texts
	--if Details[MainTrain[1].X][MainTrain[1].Y] == 3 then 
		love.graphics.print("Station "..Railroad[MainTrain[1].CurrentRail].Station, 400, 400 )
		love.graphics.print("Field: "..Field[MainTrain[1].X+1][MainTrain[1].Y+1], 400, 416 )
		love.graphics.print("Details: "..Details[MainTrain[1].X+1][MainTrain[1].Y+1], 400, 432 )
	--end

				love.graphics.print(Details[MainTrain[1].X][MainTrain[1].Y+1], 32+((MainTrain[1].X-OffsetX)*TileSize), 32+((MainTrain[1].Y-OffsetY+1)*TileSize))	
	
	love.graphics.print(FPS.." FPS", 8,8 )
	love.graphics.print("Q and E moves the train, WASD scrolls the screen", 8, 696)
	love.graphics.print("F opens door while on station, G shows upgrades", 696, 696)
	

	
	--love.graphics.print(Field[mousetilex][mousetiley],0,64)
end

function love.update(dt)
	
	Repeated_Key_Actions()
	
	FPS = math.floor(1/dt)
	timer = timer+1
	
	mousetilex, mousetiley = math.ceil(OffsetX+(love.mouse.getX()/TileSize)), math.ceil(OffsetY+(love.mouse.getY()/TileSize))
	
	MainTrain[1].CurrentRail = GetRail(MainTrain[1].X, MainTrain[1].Y)
	
	--train properties calculation
	for i=2, #MainTrain do
	
		local myrail = MainTrain[i-1].CurrentRail - 2
		MainTrain[i].CurrentRail = myrail
		MainTrain[i].X = Railroad[myrail].X
		MainTrain[i].Y = Railroad[myrail].Y

	end	
	
end












--checks if maintrain is on-screen
function Train_OnScr(id)

	local returner = false
 
	if MainTrain[id].X > OffsetX and MainTrain[id].X < OffsetX + ScreenX
	and MainTrain[id].Y > OffsetY and MainTrain[id].Y < OffsetY + ScreenY
	then returner = true
	else returner = false end
	
	return returner

end




--get the railroad id from tile
function GetRail(x,y)
	
	local val
	
	for i=1, #Railroad do
		if Railroad[i].X == x and Railroad[i].Y == y then val = i break end
	end
	
	--if exited without finding the proper one
	if val == #Railroad and Railroad[val].X ~= x and Railroad[vak].Y ~= y then val = 0 end
	
	return val

end


--changes rail textures to curves
--ugly function, I know, but it was simpler to do without sleeping
function Railfixator(railid)
	
	local returner
	


	--horizontal
	if (Railroad[railid-1].X == Railroad[railid].X -1 and Railroad[railid+1].X == Railroad[railid].X +1)
	or (Railroad[railid+1].X == Railroad[railid].X -1 and Railroad[railid-1].X == Railroad[railid].X +1) then
		returner = 3	
	--vertical
	elseif (Railroad[railid-1].Y == Railroad[railid].Y -1 and Railroad[railid+1].Y == Railroad[railid].Y +1)
	or (Railroad[railid+1].Y == Railroad[railid].Y -1 and Railroad[railid-1].Y == Railroad[railid].Y +1) then
		returner = 6	
	--left-up
	elseif (Railroad[railid-1].X == Railroad[railid].X -1 and Railroad[railid+1].Y == Railroad[railid].Y -1) 
	or (Railroad[railid+1].X == Railroad[railid].X -1 and Railroad[railid-1].Y == Railroad[railid].Y -1) then
		returner = 5		
	--left-down
	elseif (Railroad[railid-1].X == Railroad[railid].X -1 and Railroad[railid+1].Y == Railroad[railid].Y +1) 
	or (Railroad[railid+1].X == Railroad[railid].X -1 and Railroad[railid-1].Y == Railroad[railid].Y +1)  then
		returner = 2	
	--right-up
	elseif (Railroad[railid-1].X == Railroad[railid].X +1 and Railroad[railid+1].Y == Railroad[railid].Y -1) 
	or (Railroad[railid+1].X == Railroad[railid].X +1 and Railroad[railid-1].Y == Railroad[railid].Y -1) then
		returner = 4		
	--right-down
	elseif (Railroad[railid-1].X == Railroad[railid].X +1 and Railroad[railid+1].Y == Railroad[railid].Y +1) 
	or (Railroad[railid+1].X == Railroad[railid].X +1 and Railroad[railid-1].Y == Railroad[railid].Y +1) then
		returner = 1		

	
	else returner = 7 end
	
	return returner
end

--neighbourcounter
function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if x-1 >= 1 	and Table[x-1][y] == valueToCheck then asd=asd+1 end
	if x+1 <= SizeX and Table[x+1][y] == valueToCheck then asd=asd+1 end
	if y-1 >= 1 	and Table[x][y-1] == valueToCheck then asd=asd+1 end
	if y+1 <= SizeY and Table[x][y+1] == valueToCheck then asd=asd+1 end
	return asd
end

--for starting points of stations
--Mitchell's best-candidate random generator
--I implemented it some months ago
function BestCandidate_Distribution()

	local a, b = math.random(10,SizeX-9), math.random(10,SizeY-9)
	local best_distance = 0
	local attempts = 50
	local bestX, bestY = 0, 0
	local Instances = {}
	local Set = {}
	
	--put first one
	Set[1] = { X = a, Y = b }
	
	--put the second one
	a, b = math.random(10,SizeX-9), math.random(10,SizeY-9)
	
	--and the others
	 for j=1, 7 do
	 
		--basic variables
		a, b = math.random(10,SizeX-9), math.random(10,SizeY-9)
		Instances = {}
		
		--create instances
		for i=1, attempts do
			a, b = math.random(10,SizeX-9), math.random(10,SizeY-9)
			table.insert(Instances, { X = a, Y = b, best_d = 0 })
			
			best_distance = 100000000000
			--count, which one has the possible biggest distance from the closest point
			--1:count smallest around one sample
			for k=1, #Set do
				local d = Distance(Set[k].X, Set[k].Y, Instances[i].X, Instances[i].Y)				
				if d < best_distance then best_distance = d end
			end
			
			Instances[i].best_d = best_distance
		end
		
		best_distance = Instances[1].best_d
		local ordnum = 1
		--2:count biggest from sample set
		for k=1, #Instances do
			local d = Instances[k].best_d			
			if d > best_distance then best_distance = d ordnum = k end
		end		
		
		--add to set
		Set[1+j] = { X = Instances[ordnum].X, Y = Instances[ordnum].Y }
		
	end
	
	for i=1, #Set do
		Field[Set[i].X][Set[i].Y] = 24+i
	end

end

--and it's just some very simple math for the function above
function Distance(a_x, a_y, b_x, b_y)

  local dx = a_x - b_x
  local dy = a_y - b_y
  
  return (dx * dx) + (dy * dy)
  
end
