require 'character_coding'
require 'keys'
require 'operations'
require 'creation'
require 'file_reading'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 1
	--NEVER. EVER.CHANGE. BYTESINROW.VARIABLE.
	--I'M NOT JOKING.
	BytesInRow, RowNum = 128, 320
	SizeX, SizeY = BytesInRow*8, RowNum
	
	FileRegisterSize = 16	--those registers in the first line; given in bytes
	MaxFileNum = 2^5 - 1
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	FPS = 0

	ShowTable = false
	
	Initializing()
	
	--write test
	--WriteKatamori()	
end

function love.draw()
	
	if ShowTable then
		for j=1,SizeX do
			for i=1, SizeY do
				if Table[j][i] == 1 then
					love.graphics.rectangle("fill", (j-1)*(TileSize+1), 4+(i-1)*(TileSize+1), TileSize, TileSize)
				end
				
				
				if j%8 == 1 then
					--love.graphics.print(Reader(j, i), 4+((j-1)*2), 512+((i-1)*11))
				end
			end
		end
	end
	
	--FileReader_Picture_Test()

	for i=1, RowNum-3 do
	
		--love.graphics.print(StringLine(i), 32, 128 + (12*i))
		--love.graphics.print(Table[4][1]..Table[5][1]..Table[6][1]..Table[7][1]..Table[8][1]..Reader(1, 1), 8, 512 + (16*i))
		--love.graphics.print(FirstStringLine(), 32, 192 + (24*i))
	
	end

	love.graphics.print("(press M to show the binary map)\n"..
						BytesInRow*RowNum.." bytes overall\nwith "
						..FPS.." frames per second", 0, 16)

	for i=1, MaxFileNum do
	love.graphics.print(" "..i.." -> "..UpdateFileDatas(i),256, 2+(13*(i+3)))
	end
						
						
	
end



function love.update(dt)
	FPS = math.floor(1/dt)
	UpdateFileDatas(1)
	--file update system
	--for i=1, MaxFileNum do UpdateFileDatas(i) end
end


