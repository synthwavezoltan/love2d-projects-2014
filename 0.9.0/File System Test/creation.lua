--the main actions that create the binary table and form it properly

--creating binary table and uploading that with 0s as beginning values
--let's assume that majority of bits are strings -> setting headers
--creating free space for file datas at the beginning
--occupying one single row for every files the system can handle
function Initializing()

	--creating binary table
	Table = { }
	
	for j=1,SizeX do
	
		Table[j] = { }
	
		for i=1, SizeY do
			Table[j][i] = 0 --math.random(0,1)
		end
		
	end

	
	--setting first 3 properties (third is unused yet)
	for j=1,SizeX do
		for i=1, SizeY do
			
			if j%8 == 1 then
				Table[j][i] = 1		--number or string?
				Table[j+1][i] = 0	--usual or system data?

			end
			
		end
	end	
	
	--creating "system data"
	
	
	--file datas	
	--safety nullifying
	for i=1, SizeX do
		Table[i][1], Table[i][2], Table[i][3], Table[i][4] = 0,0,0,0
	end		
	
	--adding file data index bits (~file IDs)
	for i=1, (SizeX - 15*8), 16*8 do	
		Table[i+1][1], Table[i+1][2], Table[i+1][3], Table[i+1][4] = 1, 1, 1, 1
	end	
	
	--write down index numbers
	for i=0, 7 do
		Writer(1 + (i*16*8), 1, 0, 0, i+1)	
		Writer(1 + (i*16*8), 2, 0, 0, i+9)	
		Writer(1 + (i*16*8), 3, 0, 0, i+17)	
		Writer(1 + (i*16*8), 4, 0, 0, i+25)
		
	end
	
	
	
	--occupy a line for every files
	for i=0, MaxFileNum-1 do
	
		Table[2][5+(i*8)] = 1				--the first byte is a system file
		Writer(1, 5+(i*8), 0, 0, i+1)		--the index number to the first bit

		--nullifying the rest of the bytes		
		for j=9, SizeX-7, 8 do

			Table[j][i+5] = 1	
			Table[j+1][i+5] = 0	
			Table[j+2][i+5] = 0

			Table[j+3][i+5] = 0	
			Table[j+4][i+5] = 0	
			Table[j+5][i+5] = 0	
			Table[j+6][i+5] = 0	
			Table[j+7][i+5] = 0
		
		end
		
	
	end
end

--a function that updates the properties of the file 
--in the header in case of significant change in that
--...I mean, it's just the updater
function UpdateFileDatas(fileID)
	
	local startrow = 1
	local size = 0
	--we have to assume that we know nothing (NUTHIN') about the file. so in first step, we have to
	--go through the first byte of every lines until we find an integer that matches "fileID".


	for i=5, SizeY do
		if Reader(1, i) == fileID then startrow = i break end 
	end

	--NOTE: this loop exits at the last row but does not tell you if he didn't find the file!
	--the last file might be ONLY in the last row so I'm gonna add some additional conditions
	--here to see if [lastrow][1] is a file header. yet, let's assume that we found the first
	--row of the file. now we know where to write new stuff.
	

	--yet, only the size can change.
	
	for i=0, SizeY do
		if Reader(1, startrow+i) ~= fileID + 1 then size = i+1 break end 
	end	


	--yep, I should do that condition again. 
	--But now I write the new size data to the register instead.
	--15.byte is the startrow, 16.byte is the size
	local regx, regy = JumpToRoot(fileID)

	Writer(regx + (13*8), regy, 0, 1, startrow)	
	Writer(regx + (14*8), regy, 0, 0, startrow)
	Writer(regx + (15*8), regy, 0, 0, size)
	
	return size
end