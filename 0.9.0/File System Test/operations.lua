--gets an x,y coordinate - MUST BE THE FIRST BIT OF A BYTE!
--returns the letter belongs to that byte
function Reader(x, y)
	
	--in case of letter
	if Table[x][y] == 1 then
	
		local result = ""		
		for i=1, #Coding do
			
			local checkbool = true		
			--makes "checkbool" false if according to bits, there's match 
			for j=2, 6 do
				checkbool = checkbool and Table[x+j+1][y] == Coding[i][j]
				--e.g. true = true and Table[x+3][y] == Coding[i][2]
			end
			
			if checkbool then result = Coding[i][1] break end
		end		
		return result
	
	--in case of number
	else
		
		local result = 0		
		for j=0, 4 do
			if Table[x+3+j][y] == 1 then result = result + (2^(4-j)) end		
			--if Table[x+(7-j)][y] == 1 then result = result + (2^j) end
		end		
		return result	
		
		
	end
end

--gets the number of a row
--returns the string made out of that line
function StringLine(row)

	local line = ""
	
	for i=1, SizeX do
		if i%8 == 1 then
			line = line..Reader(i, row)
		end
	end
	
	return line
	
end

--gets an x,y coordinate - MUST BE THE FIRST BIT OF A BYTE!
--also gets a letter or number
--returns nothing, instead it writes a letter/number into the "given" byte
function Writer(x, y, letter, special, value)
	
	--before anything: make sure that "special" bit 
	--equals to what we gave to the function
	Table[x+2][y] = special
	
	--in case of letter
	if letter == 1 then
		local ltrID = FindLetterID(value)
		
		--declare that it's a letter
		Table[x][y] = 1
		
		for i=2, 6 do
			Table[x+i+1][y] = Coding[ltrID][i]
		end
	
	--in case of number
	else
		local iterator = 5
		local downcount = value

		--declare that it's a number
		Table[x][y] = 0	

		for iterator = 1, 5 do
		
			if downcount >= (2^(5-iterator)) then 
				downcount = downcount - (2^(5-iterator))
				Table[x+iterator+2][y] = 1
				--the problem is that in the final step this condition returns true even in
				--case of even numbers so the result is always odd.
				--I reduced the number of loops from 5 to 4 to avoid it
				--and placed an exterior condition for this.
			else
				Table[x+iterator+2][y] = 0 
			end
			
		end	
		
		if value%2 == 0 then 
		Table[x+7][y] = 0 
		else Table[x+7][y] = 1 end		
		

	end

	
end

--testing Writer()
function WriteKatamori()

	local row = math.random(1, RowNum)
	local start = math.random(1, (SizeX - 8*8))
	local strng = { "K","A","T","A","M","O","R","I" }
	
	start = start - ((start%8)-1)
	
	for i=0, #strng-1 do Writer(start + (i*8), row, 1, 0, strng[i+1]) end

end

--2 functions: jump to the "root description" of the file from its ID
--and jump to the row of the first row of the file from its ID
--ironically, the latter needs the former (at least it's easier)
function JumpToRoot(ID)
	
	--in which row? -> decided about the division by 8
	local y = math.ceil(ID/8)
	
	--which byte? -> decided about modulus (in case of 1, this remainder is one)
	local x = (((ID-1)%8)*(FileRegisterSize*8)) + 1
	
	return x, y
	
end

function JumpToFile(ID)
	
	local headx, heady = JumpToRoot(ID)
	
	
	
end

