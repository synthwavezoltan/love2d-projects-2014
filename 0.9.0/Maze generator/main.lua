function love.load()
	math.randomseed( os.time() )
	x,y,z,i,j,k = 0,0,0,0,0,0
	TileX, TileY = 0,0
	counter = 0
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	rand=0
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*16, (i-1)*16, 16, 16, 48, 48)
		end
	end	
	
	--uploading field
	Field = { }
	Numbers = { }
	HelpTable = { }
	
	for j=1,128 do
	
	Field[j] = { }
	Numbers[j] = { }
	HelpTable[j] = { }	
	
		for i=1, 96 do
			Field[j][i] = 1
			Numbers[j][i] = 0
			HelpTable[j][i] = 0				
		end
	end

end

function love.draw()

	for j=1,128 do
		for i=1, 96 do
			if Numbers[j][i] ~= 0 then 
			love.graphics.draw(TilesetPic, Tileset[Numbers[j][i]], (j-1)*8, (i-1)*8)
			--love.graphics.print(Numbers[j][i],(j-1)*16, (i-1)*16)
			end			
		end
	end
	
	love.graphics.print(counter,0,0)
	love.graphics.print(TileX,220,0)	
	love.graphics.print(TileY,250,0)	
	love.graphics.print(Numbers[TileX][TileY],280,0)
	love.graphics.print(#ListColors(HelpTable, 2),330,0)

end

function love.update(dt)
	TileX = math.floor(love.mouse.getX()/8)+1
	TileY = math.floor(love.mouse.getY()/8)+1

	if counter == 1 then Answer() Answer() Answer() Answer() Answer() Answer() end

end

function love.mousepressed( x, y, button )	
	
	if button == "l" then
		counter = 1
		if Numbers[TileX][TileY] == 0 or HelpTable[TileX][TileY] == 2
		then Numbers[TileX][TileY] = 9 end
		
		Answer()

	end
end

function Answer()
	
	BackLoad(HelpTable, Numbers)
	
	
	for j=2,127 do
		for i=2, 95 do
		
			if Numbers[j][i] ~= 9 and Numbers[j-1][i] == 9 then HelpTable[j][i] = 1 end
			if Numbers[j][i] ~= 9 and Numbers[j+1][i] == 9 then HelpTable[j][i] = 1 end
			if Numbers[j][i] ~= 9 and Numbers[j][i-1] == 9 then HelpTable[j][i] = 1 end
			if Numbers[j][i] ~= 9 and Numbers[j][i+1] == 9 then HelpTable[j][i] = 1 end			
		end
	end	
	
	
	for j=2,127 do
		for i=2, 95 do
		
			if Numbers[j][i] ~= 9 
			and CountNeighbours(j,i, 9, Numbers) == 1
			and FullNeighbours(j,i, 9, Numbers) < 3			
			and HelpTable[j][i] == 1 
			then HelpTable[j][i] = 2 end
			
		end
	end		
	
	j,i=10,10
	
	if #ListColors(HelpTable, 2) ~= 0 then
		lista = ListColors(HelpTable, 2)
		
		rand = math.random(#ListColors(HelpTable, 2))
		HelpTable[lista[rand][1]][lista[rand][2]] = 9
	else 
		--counter = 0
	end
	
	
	
	BackLoad(Numbers, HelpTable)
end

function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end


