function GUI_Display_FieldScreen()

	--playing field screen
	for j=1,ScreenX do
		for i=1, ScreenY do
			love.graphics.draw(TilesetPic, Tileset[Field[ScreenOffsetX+j][ScreenOffsetY+i]], (j-1)*TileSize, (i-1)*TileSize)

		end
	end
	
	--displaying railroads in-game
	for i=1, #Towns do
	
		love.graphics.line(
		TileSize*Towns[i].X - TileSize/2 - TileSize*ScreenOffsetX, 
		TileSize*Towns[i].Y - TileSize/2 - TileSize*ScreenOffsetY,
		
		TileSize*Towns[Towns[i].Neighbour[1]].X - TileSize/2 - TileSize*ScreenOffsetX,
		TileSize*Towns[Towns[i].Neighbour[1]].Y - TileSize/2 - TileSize*ScreenOffsetY)	
		
	end
end

function GUI_Display_Minimap()

	--minimap: displaying railroads and indicating icons
	for i=1, #Towns do
	
		love.graphics.line(618+(3*Towns[i].X), 18+(3*Towns[i].Y),
						   618+(3*Towns[Towns[i].Neighbour[1]].X), 18+(3*Towns[Towns[i].Neighbour[1]].Y)	)
						   
	end	
	
	for i=1, #Towns do
		--display
		if Towns[i].Category == "City" then
			love.graphics.draw(GUIcons, GUI_Set[1], 618-16+(3*Towns[i].X), 18-16+(3*Towns[i].Y))		
		elseif Towns[i].Category == "Town" then	
			love.graphics.draw(GUIcons, GUI_Set[1+12], 618-14+(3*Towns[i].X), 18-14+(3*Towns[i].Y))	
		end	
	end
	
	--outline on minimap around displayed area
	love.graphics.line(620+(3*ScreenOffsetX), 20+(3*ScreenOffsetY), 
					   620+(3*ScreenOffsetX)+(ScreenX*3)-2, 20+(3*ScreenOffsetY),
					   620+(3*ScreenOffsetX)+(ScreenX*3)-2, 20+(3*ScreenOffsetY)+(ScreenY*3)-2,
					   620+(3*ScreenOffsetX), 20+(3*ScreenOffsetY)+(ScreenY*3)-2,
					   620+(3*ScreenOffsetX), 20+(3*ScreenOffsetY))
					   
end


function GUI_Display_CityDetails()
		
		--name
		love.graphics.print(Towns[ChosenPlace].Category.." of "..Towns[ChosenPlace].Name, 600,344)
		
		--railroad connections of cities
		if Towns[ChosenPlace].Category == "City" then
		
				love.graphics.print("Railroad connections involved:\n",600,648)
				for i=1, #Towns[ChosenPlace].Neighbour do
					love.graphics.print("- "..Towns[Towns[ChosenPlace].Neighbour[i]].Name, 600,664+((i-1)*16)) 
				end

		end
end