require 'citygen'
require 'keybinding'
require 'gui'

function love.load()

	math.randomseed( os.time() )
	--technical values & constants
	TileSize = 16
	TilesetX, TilesetY = TileSize*3, TileSize*8
	SizeX, SizeY = 128, 96
	
	ScreenX, ScreenY = 600/TileSize, 600/TileSize
	ScreenOffsetX, ScreenOffsetY = 0,0
	
	months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }
	letters = { 
		{"A","E","I","O","U"},
		{"B","C","D","F","G","H","J","K","L","M","N","P","Q","R","S","T","V","W","X","Y","Z"} }
	fractions = { "United Forces", "Streetknights", "Church", "Red Fist", "Justice Empire", "Jade Legion",
				"Golddiggers", "Khali", "Shadow Army", "Cloudan Condeferation", "Moebius", "Android Nation"}
	
	game_clock = 0
	FPS = 0
	
	MouseX, MouseY = 16,16
	MouseTileX, MouseTileY = 1,1
	
	ChosenPlace = 0
	
	--letter vars for various uses
	x,y,z,i,j,k = 0,0,0,0,0,0

	--game vars
	Year, Month, Day = math.random(2200,2300), math.random(1,12),math.random(1,28)
	Turn = 1
	
	--font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)	
	
	--tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 8 do
		for i=1, 3 do 
			Tileset[((j-1)*3)+i] = love.graphics.newQuad((i-1)*TileSize, (j-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--logo
	LogoPic = love.graphics.newImage('LOGO_ICON.png')
	Logos = {}

	for j=1, 4 do
		for i=1, 3 do 
			Logos[((j-1)*3)+i] = love.graphics.newQuad((i-1)*(TileSize*8), (j-1)*(TileSize*8), TileSize*8, TileSize*8, TilesetX*8, TilesetY*4)
		end
	end	
	
	--GUI
	GUIcons = love.graphics.newImage('GUI_ICONS.png')
	GUI_Set = {}
	
	for j=1, 8 do
		for i=1, 8 do 
			GUI_Set[((j-1)*8)+i] = love.graphics.newQuad((i-1)*(TileSize*2), (j-1)*(TileSize*2), TileSize*2, TileSize*2, 256, 256)
		end
	end		
	
	--pics
	HUD = love.graphics.newImage('HUD_1024_768.png')
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = 1
		end
		
	end
	
	Towns = {} 
	Towns = Create_Cities(30,50)
	
	for i=1, #Towns do
		Field[Towns[i].X][Towns[i].Y] = 13
	end
	
end

function love.draw()
	
	GUI_Display_FieldScreen()

	--displaying HUD
	love.graphics.draw(HUD, 0, 0)
	
	GUI_Display_Minimap()


	--date
	if Day == 1 then
		love.graphics.print(Day.."st "..months[Month]..", "..Year, 608, 744)
	elseif Day == 2 then
		love.graphics.print(Day.."nd "..months[Month]..", "..Year, 608, 744)
	elseif Day == 3 then
		love.graphics.print(Day.."rd "..months[Month]..", "..Year, 608, 744)
	else
		love.graphics.print(Day.."th "..months[Month]..", "..Year, 608, 744)
	end
	
	for i=1, #Towns do

			--love.graphics.print(Towns[i].X,0, 32*i)
			--love.graphics.print(Towns[i].Y,128, 32*i)
			--love.graphics.print(Towns[i].Neighbour,256, 32*i)			
	end
	
	love.graphics.print(FPS,0,0)
	love.graphics.print(MouseTileX..", "..MouseTileY, 0, 16)

	--city detection
	if MouseX < 600 and MouseY < 600 and ChosenPlace ~= 0 then
	
		--display a selection icon
		love.graphics.draw(GUIcons, GUI_Set[25], (math.floor(MouseX/TileSize)*TileSize)-TileSize/2, (math.floor(MouseY/TileSize)*TileSize)-TileSize/2)
		
		--name
		love.graphics.print(Towns[ChosenPlace].Category.." of "..Towns[ChosenPlace].Name, 600,344)
	end
end

function love.update(dt)
	game_clock = game_clock+1
	
	FPS = math.floor(1/dt)
	
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	
	if MouseX < 600 and MouseY < 600 then
	MouseTileX, MouseTileY = 1+ScreenOffsetX+math.floor(MouseX/TileSize), 1+ScreenOffsetY+math.floor(MouseY/TileSize) end
	
	ChosenPlace = Choose_City(Towns, MouseTileX, MouseTileY)
	
	Repeated_Key_Behaviour()

end

--returns true if there's already something on Field[x][y]
function Occupied_Spot(cityset, x, y)
	
	local tempbool = false
	
	for i=1, #cityset do
		tempbool = tempbool or 
		(
		(cityset[i].X == x and cityset[i].Y == y)
		or (cityset[i].X-1 > 0 and cityset[i].X-1 == x)
		or (cityset[i].X+1 <SizeX and cityset[i].X+1 == x)
		or (cityset[i].Y-1 > 0 and cityset[i].Y-1 == y)
		or (cityset[i].Y+1 <SizeY and cityset[i].Y+1 == y)	
		)
		

	end
	
	return tempbool
end

--same as Occupied_Spot(), but returns the city ID as well
function Choose_City(cityset, x, y)
	
	local CityID = 0
	
	for i=1, #cityset do
		if cityset[i].X == x and cityset[i].Y == y then CityID = i break end

	end
	
	return CityID
end

--general function to find an element within a set, array, or whatever
function Search_in_set(element, array)
	
	local bool = false
	
	for i=1, #array do 
		if array[i] == element then bool = true break end 
	end 
	
	return bool 
	
end

function Distance(a_x, a_y, b_x, b_y)

  local dx = a_x - b_x
  local dy = a_y - b_y
  
  return (dx * dx) + (dy * dy)
  
end



























function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end

function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




