function Create_Cities(placement_citynum, placement_tries)

	local Cities = {}
	
	Place_Cities(Cities, placement_citynum, placement_tries)
	Place_Towns(Cities, placement_citynum*2)
	NameGen(Cities)
	Generate_Railroads(Cities)
	
	
	
	return Cities
end

--a function that generates cities with Mitchell�s best-candidate sampling algorithm
function Place_Cities(Set, citynum, attempts)

	local a, b = math.random(1, SizeX), math.random(1,SizeY)
	local best_distance = 0
	local bestX, bestY = 0, 0
	local Instances = {}
	
	--put first city
	Set[1] = { X = a, Y = b, Category = "City" }
	
	--put the second city
	a, b = math.random(1, SizeX), math.random(1,SizeY)
	
	--and the others
	 for j=1, citynum-1 do
	 
		--basic variables
		a, b = math.random(1, SizeX), math.random(1,SizeY)
		Instances = {}
		
		--create instances
		for i=1, attempts do
			a, b = math.random(1, SizeX), math.random(1,SizeY)
			table.insert(Instances, { X = a, Y = b, best_d = 0 })
			
			best_distance = 100000000000
			--count, which one has the possible biggest distance from the closest city	
			--1:count smallest around one sample
			for k=1, #Set do
				local d = Distance(Set[k].X, Set[k].Y, Instances[i].X, Instances[i].Y)				
				if d < best_distance then best_distance = d end
			end
			
			Instances[i].best_d = best_distance
		end
		
		best_distance = Instances[1].best_d
		local ordnum = 1
		--2:count biggest from sample set
		for k=1, #Instances do
			local d = Instances[k].best_d			
			if d > best_distance then best_distance = d ordnum = k end
		end		
		
		--add city to that place
		Set[1+j] = { X = Instances[ordnum].X, Y = Instances[ordnum].Y, Category = "City" }
	end

end

--place towns with simple random numbers
function Place_Towns(Set, citynum)

	local a, b = math.random(1,SizeX), math.random(1,SizeY)
	
	for i=1, citynum do
		
		while Occupied_Spot(Set, a, b) do
			a = math.random(1,SizeX)
			b = math.random(1,SizeY)
		end

		Set[#Set+1] = { X = a, Y = b, Category = "Town" }
	end

end

--creating connections between cities (ONLY BETWEEN CITIES!)
function Generate_Railroads(Set)


	for i=1, #Set do
		Set[i].Neighbour = {}
		
		--search closest CITIES!
		local f = 10000000000000000
		local ordnum = k

		for k=1, #Set do
			local d = Distance(Set[k].X, Set[k].Y, Set[i].X, Set[i].Y)	

			if d > 0 and d < f and Set[i].Category == "City" and Set[k].Category == "City" then f = d ordnum = k end
		end	
		
		--if place is not a city then neighbour is itself
		if Set[i].Category == "City" and Set[ordnum].Category == "City"
		then Set[i].Neighbour[1] = ordnum 
		else Set[i].Neighbour[1] = i end
	end
	
	--City says: "add my name to my neighbour's Neighbour list
	for i=1, #Set do
		if Set[i].Category == "City" 
		and not Search_in_set(i, Set[Set[i].Neighbour[1]].Neighbour)
		then
			table.insert(Set[Set[i].Neighbour[1]].Neighbour, i)
		end
	end
end

--creating names
function NameGen(Set)

	local letrnum,letr,rand = 1,"A",1
	local v_count, c_count = 0,0
	local text_array = { "A","A","A","A","A","A","A","A" }

	for i=1, #Set do
		
		for j=1, 8 do
				rand = (math.random(10000)%2)+1
				letrnum = math.random(1,#letters[rand]) 			
				letr = letters[rand][letrnum]
		end
	
		Set[i].Name = text_array[1]..text_array[2]..text_array[3]..text_array[4]..
		text_array[5]..text_array[6]..text_array[7]..text_array[8]
	end
	

end