function love.load()

	x,y,z,i,j,k = 0,0,0,0,0,0
	Graph_Size = 30

	Set = {}
	for i=1, Graph_Size do Set[i] = { Number = i, Value = 0 } end

	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*32, (i-1)*32, 32, 32, 96, 96)
		end
	end	
	
	--creating graph
	Graph = {}

      for i=1, Graph_Size do	
		x = math.random(768)
		y = math.random(568)
		z = math.random(8)
		
		Graph[i] = {
		X_Pos = x,
		Y_Pos = y,
		Texture = z,
		Neighbour = ChooseClosest(i)
		}
	end
	
end

function love.draw()
	for i=1, Graph_Size do
		love.graphics.draw(TilesetPic, Tileset[Graph[i].Texture], Graph[i].X_Pos, Graph[i].Y_Pos)
		love.graphics.line( 
		Graph[i].X_Pos +16, Graph[i].Y_Pos +16, 
		Graph[Graph[i].Neighbour].X_Pos +16, Graph[Graph[i].Neighbour].Y_Pos +16)
	end
end

function love.update(dt)

end

function love.keypressed(key)
   if key == "escape" then
      	for i=1, Graph_Size do	
		x = math.random(768)
		y = math.random(568)
		z = math.random(8)
		
		Graph[i] = {
		X_Pos = x,
		Y_Pos = y,
		Texture = z,
		Neighbour = ChooseClosest(i)
		}
		end
   end
end

-- simple function to calculate distance of 2 points
function Distance_of_Points(x1,x2,y1,y2)

	dst = (math.pow((x2-x1),2)+math.pow((y2-y1),2))
	
	return dst

end

function Replacing(a,b)

	a = a-b
	b = a+b
	a = b-a

end

function ChooseClosest(ID)

	--count distances
	for i=1, Graph_Size do
		if i ~= ID then
			Set[i].Number = i
			--Set[i].Value = Distance_of_Points(Graph[i].X_Pos,Graph[ID].X_Pos,Graph[i].Y_Pos,Graph[ID].Y_Pos)
			--Value = Distance_of_Points(Graph[i].X_Pos,Graph[i].Y_Pos,Graph[ID].X_Pos,Graph[ID].Y_Pos)
		end
	end
	
	--sort elements of Set
	for j=1, Graph_Size do
		for i=1, Graph_Size-1 do
			if Set[i].Value > Set[i+1].Value then
				Replacing(Set[i].Value,Set[i+1].Value)	
			end
		end	
	end
	
	return Set[1].Number

end


