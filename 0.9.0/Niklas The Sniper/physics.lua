--
--SIMPLE FUNCTIONS ("GETTERS")
--
function CheckIfClimbing(Actor_X, Actor_Y, Array, movdir)

	local indicator = false
	local X_Tile, Y_Tile = TileCoordinates(Actor_X,Actor_Y)				--get the tile coordinates of actor
	
	if (movdir == "left up" or movdir == "left" or movdir == "left down") then
		--avoiding glue-effct
		X_Tile, Y_Tile = TileCoordinates(Actor_X+TileSize/2,Actor_Y)
	
		if SolidChecker(X_Tile, Y_Tile, Array) then
			indicator = true
		end
		
	elseif (movdir == "right up" or movdir == "right" or movdir == "right down") then
		--avoiding glue-effct
		X_Tile, Y_Tile = TileCoordinates(Actor_X-TileSize/2,Actor_Y)	
	
		if SolidChecker(X_Tile, Y_Tile, Array) then
			indicator = true
		end		
	end
	
	return indicator
end
--
--VECTOR FUNCTIONS
--

function Gravity(Actor_X, Actor_Y, Array)

	local X_Tile, Y_Tile = TileCoordinates(Actor_X,Actor_Y)				--get the tile coordinates of actor

	if Array[X_Tile][Y_Tile+1] ~= 3 then								--if below is not solid	then
		if GravityAmount < 10 then										-- accelerating is small enough
			GravityAmount = GravityAmount + GForce							--accelerate with GForce
		end		
		
		Actor_Y = Actor_Y + GravityAmount								--actually change its speed
	else
		GravityAmount = 0	
	end
	
	return Actor_Y
end

function DrawPath(Actor_X, Actor_Y, Force, radian, list, Array)
		
		local k = Actor_X	--Xpos
		local l = Actor_Y	--Ypos
		local m = 0			--Gravity
		local n = 0			--Wind
		local i=1
		
		local X_Tile, Y_Tile = TileCoordinates(k,l)
		
		list = {}
		
		repeat
			list[i*2-1] = k
			list[i*2] = l
			m = m + GForce
			n = n + WForce
			k = k + math.cos(radian)*Force - n
			l = l + math.sin(radian)*Force + m

			X_Tile, Y_Tile = TileCoordinates(list[i*2-1],list[i*2])
			X_Tile, Y_Tile = X_Tile + OffsetX, Y_Tile + OffsetY
			i=i+1
			
		until 
		SolidChecker(X_Tile, Y_Tile, Field)

		
	return list
end

function Vector_Jump(Actor_X, Actor_Y, Force, radian, Array)

	local X_Tile, Y_Tile = TileCoordinates(Actor_X,Actor_Y)				--get the tile coordinates of actor

	GravityAmount = GravityAmount + GForce
	WindAmount = WindAmount + WForce

	if math.cos(radian) >0 then Actor_X = Actor_X + 1*Force - WindAmount
	else Actor_X = Actor_X - 1*Force - WindAmount end
	Actor_Y = Actor_Y + (math.sin(radian)/math.cos(radian))*Force --+ GravityAmount
	

	return Actor_X, Actor_Y
end

function GetMovingDirection(Force, radian)
	--checking X change and Y change
	
	if 0 < math.cos(radian)*Force - WindAmount and 0 < math.sin(radian)*Force + GravityAmount then
		return "right down"
	elseif 0 == math.cos(radian)*Force - WindAmount and 0 < math.sin(radian)*Force + GravityAmount then
		return "down"	
	elseif 0 > math.cos(radian)*Force - WindAmount and 0 < math.sin(radian)*Force + GravityAmount then
		return "left down"	
	elseif 0 < math.cos(radian)*Force - WindAmount and 0 == math.sin(radian)*Force + GravityAmount then
		return "right"		
	elseif 0 == math.cos(radian)*Force - WindAmount and 0 == math.sin(radian)*Force + GravityAmount then
		return "no move"		
	elseif 0 > math.cos(radian)*Force - WindAmount and 0 == math.sin(radian)*Force + GravityAmount then
		return "left"		
	elseif 0 < math.cos(radian)*Force - WindAmount and 0 > math.sin(radian)*Force + GravityAmount then
		return "right up"		
	elseif 0 == math.cos(radian)*Force - WindAmount and 0 > math.sin(radian)*Force + GravityAmount then
		return "up"
	elseif 0 > math.cos(radian)*Force - WindAmount and 0 > math.sin(radian)*Force + GravityAmount then
		return "left up"
	end	

end



