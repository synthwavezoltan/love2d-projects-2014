require 'physics'
require 'collision'
require 'misc'

function love.load()
	
	math.randomseed( os.time() )
	love.keyboard.setKeyRepeat( true )
	--general variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	
	--display variables
	TileSize = 32
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 128, 128
	ScreenSizeX, ScreenSizeY = 32, 24
	
	OffsetX, OffsetY = 0,0		--IN TILE UNITS!!
	Offset_LimitX = 192			--how close you should be to edges to start scrolling
	Offset_LimitY = 192
	Scrolling_Value = 1			--how many tiles does it jump?
	
	--input
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	MouseTileX, MouseTileY = love.mouse.getX(), love.mouse.getY()
	MouseX_OnScr, MouseY_OnScr = love.mouse.getX(), love.mouse.getY()
	click = false
	SpecX, SpecY = 0,0

	--tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, TilesetX/TileSize do
		for i=1, TilesetY/TileSize do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--game variables
	--
	--playing field
	Field = { }
	
	for j=1,SizeX do	
		Field[j] = { }
		for i=1, SizeY do
			Field[j][i] = 1		
			end
	end	

	--vector-related variables
	--
	--table for trajectory
	Trajectory = { }					
	
	--force values
	GForce = 0.3 							--gravity: a vector with X:0 and some positive Y value
	WForce = 0.00 							--wind: a vector with Y:0 and some positive X value
	
	--store values (for increased moving)
	GravityAmount = 0 
	WindAmount = 0
		
	--general actors & stuff like that
	Player = {
		Name = 'Niklas',
		X = 512,
		Y = 512,
		X_OnScr = 512,
		Y_OnScr = 512,
		TileX = 16,
		TileY = 16,
		Force = 12, 					--how strong he can push himself away
		MovSpd_Ground = 10,				--how fast he can move on ground
		Energy = 100,
		Is_Climbing = false,
		Direction = "no move",			--where does it move
	}
	
	
	--some level element
	for x=1, SizeX do 
	Field[x][32] = 3 
	Field[x][33] = 3 	
	Field[1][x] = 3 	
	Field[2][x] = 3 	
	Field[x][1] = 3 
	Field[x][2] = 3 
	end
	
	for i=1, 100 do Field[math.random(SizeX)][math.random(SizeY)] = 3 end
	
	
	
end

function love.draw()



	for j=1,ScreenSizeX do
		for i=1, ScreenSizeY do
			love.graphics.draw(TilesetPic, Tileset[Field[OffsetX+j][OffsetY+i]], (j-1)*TileSize, (i-1)*TileSize)
		end
	end
	
	love.graphics.draw(TilesetPic, Tileset[4], Player.X_OnScr, Player.Y_OnScr, math.rad(90), 1, 1, 16, 16)
	
	love.graphics.print(Player.Force, TileSize*(Player.TileX - 1 - OffsetX), TileSize*(Player.TileY - 1 - OffsetY))
	
	m,n = TileCoordinates(Trajectory[#Trajectory-1], Trajectory[#Trajectory])
	love.graphics.print(Player.X.."\n"..Player.Y.."\n"..#Trajectory, 0,0)
	
	if not click then
		love.graphics.line(MouseX_OnScr, MouseY_OnScr, Player.X_OnScr, Player.Y_OnScr)
		
		if #Trajectory > 2 then 
		love.graphics.line(Trajectory)
		--love.graphics.print(m..","..n..": "..Field[m][n], TileSize*m, TileSize*n)
		end
	end
	
end

function love.update(dt)

		--stuff that have to check ALL THE TIME!
		--
		--display
		Scrolling(Offset_LimitX, Offset_LimitY, Scrolling_Value)
		
		--mouse
		MouseX = love.mouse.getX() + TileSize*OffsetX 
		MouseY = love.mouse.getY() + TileSize*OffsetY 
		
		MouseTileX = love.mouse.getX()/TileSize
		MouseTileY = love.mouse.getY()/TileSize

		MouseX_OnScr, MouseY_OnScr = love.mouse.getX(), love.mouse.getY()		
		
		--player
		Player.TileX,Player.TileY = TileCoordinates(Player.X,Player.Y)							--set Player's tile properties properly
			
		Player.X_OnScr = Player.X - (TileSize*OffsetX)											--set Player's OnScreen coordinates
		Player.Y_OnScr = Player.Y - (TileSize*OffsetY)	
		
		Player.Is_Climbing = CheckIfClimbing(Player.X,Player.Y,Field, Player.Direction)			--check if Player is climbing at the moment
	
		
		--vectors
		--launch!
		if click then
			--coordinate fix attempts for avoiding "sink in" effects
			if SolidChecker(Player.TileX, Player.TileY, Field) then 

			end			
	
			--calculate the move of Player
			k = math.rad(GetDegree(Player.X, Player.Y, SpecX, SpecY))
			Player.X, Player.Y = Vector_Jump(Player.X, Player.Y, Player.Force, k, Field) 
			
			--also check its direction
			Player.Direction = GetMovingDirection(Player.Force, k)
			
			--stop in certain circumstances
			if Player.Is_Climbing 
			or SolidChecker(Player.TileX+1, Player.TileY, Field) 
			or SolidChecker(Player.TileX-1, Player.TileY, Field) 		
			or SolidChecker(Player.TileX, Player.TileY+1, Field) 
			or SolidChecker(Player.TileX, Player.TileY-1, Field) 		
			then 			
				click = false
			end		
		else
			--draw trajectory
			z = math.rad(GetDegree(Player.X_OnScr, Player.Y_OnScr, MouseX_OnScr, MouseY_OnScr))
			Trajectory = DrawPath(Player.X_OnScr, Player.Y_OnScr, Player.Force, z, Trajectory, Field)

			
			Player.Force = math.floor(GetDirection(Player.X, Player.Y, MouseX, MouseY)/20)	--set the "jumping force" of Player
			Player.Direction = "no move"
		end
		

		
		

end

--mouse actions
function love.mousepressed(x,y,button)

	if button == 'l' then
		--for launch!
		if not click then
			SpecX = MouseX + OffsetX + (MouseX - Player.X)*1000 
			SpecY = MouseY + OffsetY + (MouseY - Player.Y)*1000 
			GravityAmount, WindAmount = 0,0
			click = true			
		end
	end
end

--keyboard actions
function love.keypressed(key, isrepeat)
	
	if key == 'escape'
	then
		love.event.push('quit')
	end
	
	if key == ' ' then
	
	end
	
	if key == 'w' then
		Player.Y = Player.Y-2
	end
	
	if key == 's' then
		Player.Y = Player.Y+2
	end

	if key == 'a' then
		
		Player.X = Player.X - Player.MovSpd_Ground	
	end

	if key == 'd' then	
		Player.X = Player.X + Player.MovSpd_Ground	
	end
end






















