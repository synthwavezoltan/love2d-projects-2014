function GetDegree(X_start, Y_start, X_finish, Y_finish)

		local VectorX = X_finish - X_start
		local VectorY = Y_finish - Y_start
		
		degree = math.deg(math.atan2(VectorY,VectorX)) 

		return degree
end

function GetDirection(startX, startY, finishX, finishY)

	return math.floor(math.sqrt( math.pow(finishX-startX,2) + math.pow(finishY-startY,2) ))

end

function TileCoordinates(X,Y)
	
	local NewX = math.floor(X/TileSize)+1
	local NewY = math.floor(Y/TileSize)+1
		
	return NewX, NewY
end

--
--SCROLLING
--

function Scrolling(bordersizeX, bordersizeY, scrollvalue)

	if Player.X_OnScr < bordersizeX and OffsetX > 0 then
		OffsetX = OffsetX - scrollvalue
	end
	
	if Player.Y_OnScr < bordersizeY and OffsetY > 0 then
		OffsetY = OffsetY - scrollvalue
	end	
	
	if Player.X_OnScr > (ScreenSizeX*TileSize) - bordersizeX and OffsetX < SizeX - ScreenSizeX then
		OffsetX = OffsetX + scrollvalue
	end
	
	if Player.Y_OnScr > (ScreenSizeY*TileSize) - bordersizeY and OffsetY < SizeY - ScreenSizeY then
		OffsetY = OffsetY + scrollvalue
	end		
end


--
--other stuffz from my earlier projects
--

function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)

	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end


function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end

