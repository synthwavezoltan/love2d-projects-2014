function love.load()

	--key values
	TileSize = 32
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 32, 24
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	SpecX, SpecY = 0,0
	VectorX, VectorY = 0,0
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	degree = 0
	radian = 0

	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	Player = {
	X = 500,
	Y = 500,
	}
	
end

function love.draw()

	love.graphics.draw(TilesetPic, Tileset[5], Player.X, Player.Y, math.rad(degree+90), 1, 1, 16, 16)
	--love.graphics.line(MouseX, MouseY, Player.X, Player.Y)
	love.graphics.print("Angle in radians: "..radian..
						"\nAngle in degrees: "..degree..
						"\nAngle sine: "..math.sin(radian)..
						"\nAngle cosine: "..math.cos(radian),MouseX,MouseY+20)
	love.graphics.print(SpecX..","..SpecY,0,0)
end

function love.update(dt)
	MouseX = love.mouse.getX()
	MouseY = love.mouse.getY()
	
	
	
	--I need cos/sin: in front of/neighbour
	--if there's no aim yet, then look to the dir of mouse
	if SpecX ==0 and SpecY == 0 then
		VectorX = MouseX-Player.X
		VectorY = MouseY-Player.Y
	else 
		VectorX = SpecX-Player.X
		VectorY = SpecY-Player.Y	
	end
	
	degree = math.deg(math.atan2(VectorY,VectorX)) 
	radian = math.rad(degree)
	
	if degree>=360 then degree = degree-360 end
	
	if degree<=0 then degree = degree+360 end
	
	if Player.X>0 and Player.Y>0 then
		--MATH.COS AND MATH.SIN NEED THE ANGLE IN FUCKIN RADIAN!!!!!!!!
		Player.X = Player.X + math.cos(radian)*3
		Player.Y = Player.Y + math.sin(radian)*3
	else
	
	end
end

function love.mousepressed(x,y,button)

	if button == 'l' then
		SpecX = MouseX
		SpecY = MouseY
	end
end




