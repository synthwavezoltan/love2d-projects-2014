function Place_Mines(minenum)

	for i=1, minenum do
		Mines[i] = {}
		
		local a, b = math.random(2, SizeX-1), math.random(2, SizeY-1)				
		Field[a][b] = 9		
		a, b = math.random(2, SizeX-1), math.random(2, SizeY-1)	
		
		while Field[a][b] ~= 1
		or MineAdj[a][b] == 666		
		do	
			a, b = math.random(2, SizeX-1), math.random(2, SizeY-1)	
		end
		
		Mines[i] = { X = a, Y = b }	
		MineAdj[Mines[i].X][Mines[i].Y] = 666
	
	end

	--adjacency table	
	for i=1, #Mines do
		
		local tempx = Mines[i].X-1
			local tempy = Mines[i].Y-1		
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end
			tempy = Mines[i].Y
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end		
			tempy = Mines[i].Y+1
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end			
		tempx = Mines[i].X		
			tempy = Mines[i].Y-1
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end
			tempy = Mines[i].Y
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end
			tempy = Mines[i].Y+1
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end			
		tempx = Mines[i].X+1		
			tempy = Mines[i].Y-1
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end		
			tempy = Mines[i].Y
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end		
			tempy = Mines[i].Y+1
				if MineAdj[tempx][tempy] ~= 666 then MineAdj[tempx][tempy] = MineAdj[tempx][tempy] + 1 end			
		

	end	

	--clear the main field
	for i=1,SizeX do
		for j=1, SizeY do
			if Field[i][j] == 9 then Field[i][j] = 1 end
		end
	end

end