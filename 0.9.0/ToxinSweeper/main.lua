require 'input'
require 'lvlcrt'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	SizeX, SizeY = 1*40, 1*20
	GlobalOffsetX = 0
	GlobalOffsetY = 0
	WindowX, WindowY = love.graphics.getDimensions( )
	
	--variables
	x,y,z,i,j,k = 0,0,1,0,0,0
	
	MouseX, MouseY, MouseTileX, MouseTileY = 0,0,0,0
	
	--game variables
	gameclock = 0
	FPS = 0
	
	PlayerCol = 4
	EnemyCol = 12
	
	PlayerArea = 1
	EnemyArea = 1
	
	Initial = { 
	Player = { 1, 1 }, 
	Enemy = {  1, 1 }}
	Mines = {}
	
	Flagged = {}
	
	Victory = false
	GameOver = false
	Ingame = true
	
	--pre-set and pre-load
	love.graphics.setDefaultFilter( "nearest", "nearest", 0 )
	
	TilesetPics = {}
	Tileset = {}
	Load_Tilesets(Tileset, 'TILESET.png', 32)	
	
	--creating playfield, and the adjacency table with the same dimensions 
	Field = { }
	MineAdj = {}
	Occupation = {}
	
	for j=1,SizeX do
	
		Field[j] = { }
		MineAdj[j] = {}
		Occupation[j] = {}
		
		for i=1, SizeY do
			Field[j][i] = 1
			MineAdj[j][i] = 0	
			Occupation[j][i] = 0	
		end
		
	end
	

	
	--initializing 
	Place_Mines(200)
	
	--look for new player initials where there are no mines
	x, y = math.random(3,SizeX-1), math.random(3,SizeY-1)	
	while MineAdj[x][y] > 0 do x, y = math.random(3,SizeX-1), math.random(3,SizeY-1) end	
	Initial.Player[1], Initial.Player[2] = x, y
	
	--same for enemy
	x, y = math.random(2,SizeX-1), math.random(2,SizeY-1)	
	while MineAdj[x][y] > 0 do x, y = math.random(3,SizeX-1), math.random(3,SizeY-1) end	
	Initial.Enemy[1], Initial.Enemy[2] = x, y	
	
	--mark initials as occupied
	Occupation[Initial.Player[1]][Initial.Player[2]] = PlayerCol
	Occupation[Initial.Enemy[1]][Initial.Enemy[2]] = EnemyCol
	
end

function love.draw()

	love.graphics.setDefaultFilter( "nearest", "nearest", 0 )
	
	for i=1,SizeX do
		for j=1, SizeY do
			
			love.graphics.draw(TilesetPics[1], Tileset[Field[i][j]], GlobalOffsetX+(i-1)*TileSize, GlobalOffsetY+(j-1)*TileSize)
			
			--occupied areas
			love.graphics.setBlendMode("additive")
			if Occupation[i][j] ~= 0 then
				love.graphics.draw(TilesetPics[1], Tileset[Occupation[i][j]], GlobalOffsetX+(i-1)*TileSize, GlobalOffsetY+(j-1)*TileSize)
			end
			love.graphics.setBlendMode("alpha")	
		end
	end

	
	
	--initials
	love.graphics.draw(TilesetPics[1], Tileset[2], GlobalOffsetX + (Initial.Player[1]-1)*TileSize, GlobalOffsetY + (Initial.Player[2]-2)*TileSize)	
	love.graphics.draw(TilesetPics[1], Tileset[10], GlobalOffsetX + (Initial.Player[1]-1)*TileSize, GlobalOffsetY + (Initial.Player[2]-1)*TileSize)
	
	love.graphics.draw(TilesetPics[1], Tileset[3], GlobalOffsetX + (Initial.Enemy[1]-1)*TileSize, GlobalOffsetY + (Initial.Enemy[2]-2)*TileSize)	
	love.graphics.draw(TilesetPics[1], Tileset[11], GlobalOffsetX + (Initial.Enemy[1]-1)*TileSize, GlobalOffsetY + (Initial.Enemy[2]-1)*TileSize)	
	
	--mines
	for i=1, #Mines do 
		if Occupation[Mines[i].X][Mines[i].Y] == 4 then
			love.graphics.draw(TilesetPics[1], Tileset[9], (Mines[i].X-1)*TileSize, (Mines[i].Y-1)*TileSize)
		end
	end
	
	--adjacency table
	--show only if area is occupied
	
	for i=1,SizeX do
		for j=1, SizeY do
			if MineAdj[i][j] > 0 and MineAdj[i][j] ~= 666 and Occupation[i][j] == 4 
			then love.graphics.print(MineAdj[i][j], 12 + (i-1)*TileSize, 12 + (j-1)*TileSize) end
			
		end
	end

	--stats
	love.graphics.print("Occupied area:".."\n"..
	"by Player = "..PlayerArea.."\n"..
	"by enemy = "..EnemyArea.."\n\n"..
	"Uninhabitable = "..Colors(Occupation, 17, "count"), 0, 20*TileSize)
	
	--victory and fail
	if Victory then love.graphics.print("You win!", 640,360) end
	if GameOver then love.graphics.print("You lose!", 640,360) end
	if not Ingame then love.graphics.print("Placeholder for stuff out of actual game loop", 640,360) end
	
	--debug text
	love.graphics.print(MouseTileX..", "..MouseTileY..
	"\n"..FPS

	, 0, 0)
	
end

function love.update(dt)
	
	
	
	--constants
	gameclock 	= gameclock+1
	FPS			= math.floor(1/dt)
		
	MouseX 		= love.mouse.getX()
	MouseY		= love.mouse.getY()
	MouseTileX 	= math.floor(MouseX/TileSize) + 1
	MouseTileY 	= math.floor(MouseY/TileSize) + 1
	
	PlayerArea 	= Colors(Occupation, PlayerCol, "count")
	EnemyArea 	= Colors(Occupation, EnemyCol, "count")
	
	--check winning situation
	if Occupation[Initial.Enemy[1]][Initial.Enemy[2]] ~= EnemyCol then Victory = true else Victory = false end
	
	--check losing sitution
	if Occupation[Initial.Player[1]][Initial.Player[2]] ~= PlayerCol then GameOver = true else GameOver = false end
	
	--no uninhabitable areas belong to anyone
	for i=1, SizeX do
		for j=1, SizeY do
			if Field[i][j] == 17 then Occupation[i][j] = 0 end
		end
	end
	
	Drag()
	
end



--looks through every tiles; if given occupied tile has zero adjacent mines, then occupies every neighbours
function Automatic_Occupation(color)

	for i=1, SizeX do
		for j=1, SizeY do
			if Occupation[i][j] == color and MineAdj[i][j] == 0 then
				
				if i-1 > 0 		and j-1 > 0 		then Occupation[i-1][j-1] = color end
				if i-1 > 0 				  			then Occupation[i-1][j  ] = color end
				if i-1 > 0 		and j+1 <= SizeY 	then Occupation[i-1][j+1] = color end
				
				if			   		j-1 > 0 		then Occupation[i  ][j-1] = color end
				if			   		j+1 <= SizeY 	then Occupation[i  ][j+1] = color end
				
				if i+1 <= SizeX and j-1 > 0			then Occupation[i+1][j-1] = color end
				if i+1 <= SizeX						then Occupation[i+1][j  ] = color end
				if i+1 <= SizeX and	j+1 <= SizeY	then Occupation[i+1][j+1] = color end
				
			end
		end
	end

end


--the catastrophic consequence of running into a mine
function Explosion(x,y)
	
	--remove mine
	table.remove(Mines, FindMine(x,y))
	--recalc MineAdj
	if x-1 >= 1 	then if MineAdj[x-1][y] > 0 and MineAdj[x-1][y] < 10 then MineAdj[x-1][y] = MineAdj[x-1][y] -1	end end
	if y-1 >= 1 	then if MineAdj[x][y-1] > 0 and MineAdj[x][y-1] < 10 then MineAdj[x][y-1] = MineAdj[x][y-1] -1	end end
	if x+1 <= SizeX then if MineAdj[x+1][y] > 0 and MineAdj[x+1][y] < 10 then MineAdj[x+1][y] = MineAdj[x+1][y] -1 	end end
	if y+1 <= SizeY then if MineAdj[x][y+1] > 0 and MineAdj[x][y+1] < 10 then MineAdj[x][y+1] = MineAdj[x][y+1] -1 	end end
	
	--set initial area uninhabitable
	Field[x][y] = 17
	
	--walkover player area and transform areas until half of initial player area is transformed
	local transformed = 1					--amount of transformed area (obvious 1 at the beginning)
	local full = math.floor(PlayerArea/2)	--amount of areas that must be transformed (half of initial player area)
	local potentials = {}
	
	--first add + transform
	if x-1 >= 1 	and Occupation[x-1][y] == PlayerCol then potentials[#potentials+1] = { x-1, y } Field[x-1][y] = 17 end
	if y-1 >= 1 	and Occupation[x][y-1] == PlayerCol then potentials[#potentials+1] = { x, y-1 } Field[x][y-1] = 17 end
    if x+1 <= SizeX and Occupation[x+1][y] == PlayerCol then potentials[#potentials+1] = { x+1, y } Field[x+1][y] = 17 end
	if y+1 <= SizeY and Occupation[x][y+1] == PlayerCol then potentials[#potentials+1] = { x, y+1 } Field[x][y+1] = 17 end

	--checking if an element has no valid neighbour 
	for i=#potentials, 1, -1 do
		if CountNeighbours(potentials[i][1], potentials[i][2], 17, Field) == 4
		or CountNeighbours(potentials[i][1], potentials[i][2], PlayerCol, Occupation) == 0
		then table.remove(potentials, #potentials) end
	end

	--repeat add
	--while Colors(Occupation, 17, "count") < 1 do
	for i=1, 1+math.floor(PlayerArea/20) do
		
		for k=1, #potentials do
		--add + transform
		--I'm terribly sorry, it's hard to follow.
		--I was unable to format it properly. - K
			if potentials[k][1]-1 >= 1 		
			and Occupation[potentials[k][1]-1][potentials[k][2]] == PlayerCol and PlayerArea > full
				then potentials[#potentials+1] = { potentials[k][1]-1, potentials[k][2] } 
					Field[potentials[k][1]-1][potentials[k][2]] = 17 
					transformed = transformed + 1 end
			if potentials[k][2]-1 >= 1 		
			and Occupation[potentials[k][1]][potentials[k][2]-1] == PlayerCol and PlayerArea > full 
				then potentials[#potentials+1] = { potentials[k][1], potentials[k][2]-1 } 
					Field[potentials[k][1]][potentials[k][2]-1] = 17 
					transformed = transformed + 1 end
			if potentials[k][1]+1 <= SizeX  
			and Occupation[potentials[k][1]+1][potentials[k][2]] == PlayerCol and PlayerArea > full 
				then potentials[#potentials+1] = { potentials[k][1]+1, potentials[k][2] } 
					Field[potentials[k][1]+1][potentials[k][2]] = 17
					transformed = transformed + 1 end
			if potentials[k][2]+1 <= SizeY  
			and Occupation[x][potentials[k][2]+1] == PlayerCol and PlayerArea > full 
				then potentials[#potentials+1] = { potentials[k][1], potentials[k][2]+1 } 
					Field[potentials[k][1]][potentials[k][2]+1] = 17
					transformed = transformed + 1 end
					
				
		end
		
		--checking if an element has no valid neighbour 
		--[[
		for i=#potentials, 1, -1 do
			if CountNeighbours(potentials[i][1], potentials[i][2], 17, Field) == 4
			or CountNeighbours(potentials[i][1], potentials[i][2], PlayerCol, Occupation) == 0
			then table.remove(potentials, #potentials) end
		end				
]]

		
		
	end
	


end


function AI_OccupyArea(occasions)
	
	local k=1
	local loopcounter = 0
	
	while k < occasions do
		for i=1, SizeX do
			for j=1, SizeY do
				if Occupation[i][j] == EnemyCol then
					local rand = math.random(1,4)
					
					--select a neighbour randomly
					--happens only if there's a proper neighbour to choose
					if CountNeighbours(i,j, 0, Occupation) > 0 then
						while (rand == 1 and i-1 == 0 	) 
						or	  (rand == 2 and j-1 == 0 	) 
						or	  (rand == 3 and i+1 > SizeX)
						or	  (rand == 4 and j+1 > SizeY) do
							rand = math.random(1,4)
						end
						
						if 		rand == 1 
						and Occupation[i-1][j  ] == 0 
						and 	 Field[i-1][j  ] ~= 17 
							then Occupation[i-1][j  ] = EnemyCol k = k + 1 break
						elseif	rand == 2 
						and Occupation[i  ][j-1] == 0 
						and 	 Field[i  ][j-1] ~= 17 
							then Occupation[i  ][j-1] = EnemyCol k = k + 1 break							
						elseif	rand == 3 
						and Occupation[i+1][j  ] == 0 
						and 	 Field[i+1][j  ] ~= 17 
							then Occupation[i+1][j  ] = EnemyCol k = k + 1 break							
						elseif	rand == 4 
						and Occupation[i  ][j+1] == 0 
						and 	 Field[i  ][j+1] ~= 17 
							then Occupation[i  ][j+1] = EnemyCol k = k + 1 break end
					end
				end
			end
		end
		
		--loopcounter variable summarizes the number of iterations
		--of the main while loop, and exits automatically if it
		--indicates that it will likely never quit
		--risk:
		--at too low value - it might quit even if a proper field so step to is found
		--at too high value - function might take extremely long time to run
		loopcounter = loopcounter + 1
		
		if loopcounter > 500 then k = occasions end
	end

end



--how a round looks like? -> player stat changes and enemy steps
--triggered by mouseclick -> CONSIDER REWRITING IF IT CHANGES
function Round()
	if Colors(Occupation, 0, "count") > 0 then AI_OccupyArea(2) end
end



--a table with the name of "SetName" must be declared before its call
function Load_Tilesets(SetName, ImageName, UnitSize)

	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local CharX = TilesetPics[1]:getWidth()
	local CharY = TilesetPics[1]:getHeight()
	
	local TilesX = CharX/TileSize
	local TilesY = CharY/TileSize

	for j=1, TilesY do
		for i=1, TilesX do			
			SetName[((j-1)*TilesX) + i] = love.graphics.newQuad((i-1)*UnitSize, (j-1)*UnitSize, UnitSize, UnitSize, CharX, CharY)
		end
	end	

end

function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if x-1 > 0 		and Table[x-1][y] == valueToCheck then asd=asd+1 end
	if x+1 <= SizeX and Table[x+1][y] == valueToCheck then asd=asd+1 end
	if y-1 > 0 		and Table[x][y-1] == valueToCheck then asd=asd+1 end
	if y+1 <= SizeY and Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end



function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if x-j > 0 and x-j <= SizeX
			and y-i > 0 and y-i <= SizeY
			and Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

--count mode returns the amount of "value" in "Table"
--indicate mode returns true if "value" is found in "Table"
--list mode returns the coordinates of "value" positions in "Table"
function Colors(Table, valueToCheck, mode)

	local asd = 0
	local list = {}
	local indicator = true
	
	for j=1, SizeX do
		for i=1, SizeY do
			if mode == "count" then
				if Table[j][i] == valueToCheck then asd=asd+1 end
			elseif mode == "list" then
				if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
			elseif mode == "indicate" then
				indicator = indicator or Table[j][i] == valueToCheck
			end
		end
	end

	if mode == "count" then
		return asd
	elseif mode == "list" then
		return list
	elseif mode == "indicate" then
		return indicator
	end
	
	
end

--returns the ID of the mine that stays on x,y
function FindMine(x,y)

	local result = 0
	
	for i=1, #Mines do
		if		Mines[i].X == x
		and		Mines[i].Y == y
		then	result = i 
		break 	end
	end
	
	if 	 result==#Mines 
	then return 0
	else return result end
end

function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




