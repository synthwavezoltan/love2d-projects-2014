--So, let's divide the tileset! Each number will indicate a certain object, item, tile or special, 
--and these numbers will apply to every style, to make it easy to expand!
--(animation frames are not included, by obvious reasons)
--[[
1: passable surface 				-> 1-8
2: impenetrable wall 				-> 9-12
3: deathpit							-> 13-16
4: destroyable obstacle				-> 17-24
5: periodic obstacle "true" phase	-> 25-28
6: periodic obstacle "false" phase	-> 29-32
]]--s