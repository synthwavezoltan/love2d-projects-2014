require 'input'
require 'collision'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 40
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 16, 16
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	
	TilesetPics, Tileset, PlayerSet = {},{},{}
	Create_Tileset(Tileset, 'TILESET.png', TileSize, TileSize, 8, 8)
	Create_Tileset(PlayerSet, 'PLAYER.png', TileSize, TileSize, 16, 16)
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			if i==1 or i==SizeY or j==1 or j==SizeX then Field[j][i] = 2 else Field[j][i] = 1 end		
		end
		
	end
	
	--player definition
	Player = { X = 2, Y =2 }
	
end

function love.draw()

	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPics[1], Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
		end
	end
	
	love.graphics.draw(TilesetPics[2], PlayerSet[1], (Player.X-1)*TileSize, (Player.Y-1)*TileSize)
	
end

function love.update(dt)

end




--creates a tileset from any pictures with any scaling
--a table with the name of "SetName" must be declared before its call
function Create_Tileset(SetName, ImageName, UnitSizeX, UnitSizeY, OrigX, OrigY)
	love.graphics.setDefaultFilter( "nearest", "nearest", 1 )
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local CharX = TilesetPics[1]:getWidth()*(TileSize/OrigX)
	local CharY = TilesetPics[1]:getHeight()*(TileSize/OrigY)
	
	local TilesX = CharX/TileSize
	local TilesY = CharY/TileSize

	for j=1, TilesY do
		for i=1, TilesX do			
			SetName[((j-1)*TilesX) + i] = love.graphics.newQuad((i-1)*UnitSizeX, (j-1)*UnitSizeY, UnitSizeX, UnitSizeY, CharX, CharY)
		end
	end	
end







function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end


--count mode returns the amount of "value" in "Table"
--indicate mode returns true if "value" is found in "Table"
--list mode returns the coordinates of "value" positions in "Table"
function Colors(Table, valueToCheck, mode)

	local asd = 0
	local list = {}
	local indicator = true
	
	for j=1, SizeX do
		for i=1, SizeY do
			if mode == "count" then
				if Table[j][i] == valueToCheck then asd=asd+1 end
			elseif mode == "list" then
				if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
			elseif mode == "indicate" then
				indicator = indicator or Table[j][i] == valueToCheck
			end
		end
	end
	
	return asd
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




