function love.load()

	math.randomseed( os.time() )	
	--variables
	TileSize = 32
	x,y,z,i,j,k = 0,0,0,0,0,0
	
	--uploading field
	Field = { }
	
	for j=1,32*24 do	
		Field[j] = 0	
	end
	
	for i=1, 1024 do 
	x= math.random(1, 32*24) 
	Field[x] = Field[x]+1 end
	
end

function love.draw()

	for j=1,32 do
		for i=1, 23 do
			--love.graphics.draw(TilesetPics[1], Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
			local coord = ((i-1)*32)+j
			local br = Field[coord]*16
			
			love.graphics.setColor( br,br,br )
			love.graphics.rectangle("fill", (j-2)*(TileSize), (i-1)*(TileSize), TileSize, TileSize)	

			love.graphics.setColor( 255,255,255 )
			--love.graphics.print(coord.."\n"..br, (j-2)*(TileSize), (i-1)*(TileSize))
			
		end
	end
	
end

function love.update(dt)

end




--a table with the name of "SetName" must be declared before its call
function Load_Tilesets(SetName, ImageName, UnitSize)
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local CharX = TilesetPics[1]:getWidth()/UnitSize
	local CharY = TilesetPics[1]:getHeight()/UnitSize
	
	for j=1, UnitSize*CharX do
		for i=1, UnitSize*CharY do 
			SetName[((i-1)*CharX)+j] = love.graphics.newQuad((j-1)*UnitSize, (i-1)*UnitSize, UnitSize, UnitSize, UnitSize*CharX, UnitSize*CharY)
		end
	end	
end







function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




