function Player_SetProps()
	
	Player.MainE = FindLargest(Player.Engagement)
	Player.MainC = FindLargest(Player.Category)

end

--it is recommended to use only multiplications of 3 as "amount"
--"prop" should be allowed only to be either. "Player.Engagement" or "Player.Category"
function Player_ChangeProp(prop, id, amount)
	
	local f, amd = 0, ((#prop)-1)
	local nowrong = true
	
	--failsafe: if any of the additions or divisions would produce improper values then if doesn't happen
	for f=1, #prop do
		nowrong = nowrong and ((f==id and prop[f] <= 100-amount) or (f~=id and prop[f] > amount/amd))
	end
	
	if nowrong then
		if id == 1 then 
			for f=2, #prop do prop[f] = prop[f] - (amount/amd) end
		elseif id == 2 then
			prop[1] = prop[1] - (amount/amd)
			for f=2, #prop do prop[f] = prop[f] - (amount/amd) end
		else
			for f=1, 	id-1 	do prop[f] = prop[f] - (amount/amd) end
			for f=id+1, #prop 	do prop[f] = prop[f] - (amount/amd) end
		end
			
		prop[id] = prop[id] + amount
	end
	
	--final failsafe: if it somehow goes below 100, then increase a random item until it becomes 100
	amd = 0
	local rand = math.random(1,#prop)
	for f=1, #prop do amd = amd + prop[f] end
	if amd < 100 then prop[rand] = prop[rand] + (100-amd) end
end