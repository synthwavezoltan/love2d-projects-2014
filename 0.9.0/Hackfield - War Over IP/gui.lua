----------------------------------------------------------------
-----------------------DISPLAY FUNCTIONS------------------------
----------------------------------------------------------------
function Display_Playground(br)
	
	brightness = br Set_RGB()
	
	for j=1,SizeX do
		for i=1, SizeY do
			
			love.graphics.setColor(
			RGB_set[Field[j][i].eng][1], 
			RGB_set[Field[j][i].eng][2], 
			RGB_set[Field[j][i].eng][3])
			
			local coordx = border 	+ (j-1)*TileSize
			local coordy = fieldst 	+ (i-1)*TileSize	- scroller
			
			if 	coordy>-TileSize/2 and coordy<WindowY*0.8 then
			love.graphics.draw(TilesetPics[1], Tileset[Field[j][i].cat],
			coordx, coordy)
			--[[
				if i==1 then
					love.graphics.setColor(255,255,255)
					love.graphics.setFont(font_b)	
					love.graphics.print(j,coordx, coordy)
				end--]]
			end	
			
		end
	end
end

function Display_Interface(br)
	--create a rect with color of the player's engagement
	brightness = br Set_RGB()
	ChangeToColor(Player.MainE)
	love.graphics.rectangle("fill", 0, WindowY-hudheight, WindowX, hudheight)
	love.graphics.rectangle("fill", 0, 0, WindowX, fieldst)
	
	--texts
	love.graphics.setColor(255,255,255)
	Display_DateTime(0,0, true)
	
	love.graphics.print(currentrow, 0, border+TileSize)
end

function Display_Buttons()
	for i=0, 3 do
		love.graphics.setColor(192,192,192)
		love.graphics.rectangle("fill", WindowX*(i*0.25)+1, WindowY-btnheight + 1, 	WindowX*0.25 - 1, 	btnheight-1)
		love.graphics.setColor(0,0,0)
		love.graphics.rectangle("line", WindowX*(i*0.25), 	WindowY-btnheight, 		WindowX*0.25, 		btnheight)	
	end
end

function Display_ParallaxBodies()
	
	if #ParallaxBodies > 0 then
		for i=1, #ParallaxBodies do
		
			--county mishmash before
			local derivscale = stepunit/ParallaxBodies[i].distance

			--displaying itself
			ChangeToColor(3)
			--[[
			if ParallaxBodies[i].distance <= 0.4 then
				brightness = 17-(ParallaxBodies[i].distance/0.1)
			elseif ParallaxBodies[i].distance > 0.4 and ParallaxBodies[i].distance <= 1.8  then
				brightness = 12-(ParallaxBodies[i].distance/0.1)
			end
			--]]
			Set_RGB()
			
			love.graphics.draw(
			ParallaxBodies[i].image,							--what image
			ParallaxBodies[i].xpos,								--where
			ParallaxBodies[i].ypos,													
			0,													--how many degrees of turn
			derivscale,derivscale				--how big
			)
			
			--county mishmash after
			if ParallaxBodies[i].constant_mov
			or ((not ParallaxBodies[i].constant_mov) 
			and counter % scrolprd >= scrolprd - (steps2bedone)) then
				local spd = (derivscale*stepunit)/2
				ParallaxBodies[i].ypos = ParallaxBodies[i].ypos - spd
			end
			
		end
	end

end

function Display_DateTime(x,y, displaytime)

	local a,b,c,d = "1","2","3","4"
	
	if day == 1 then a = day.."st"
	elseif day == 2 then a = day.."nd"
	elseif day == 3 then a = day.."rd"		
	else a = day.."th" end
	
	if month == 1 then b = "January" 
	elseif month == 2 then b = "February"
	elseif month == 3 then b = "March"	
	elseif month == 4 then b = "April"
	elseif month == 5 then b = "May"	
	elseif month == 6 then b = "June"
	elseif month == 7 then b = "July"	
	elseif month == 8 then b = "August"
	elseif month == 9 then b = "September"
	elseif month == 10 then b = "October"	
	elseif month == 11 then b = "November"
	elseif month == 12 then b = "December"	end
	
	if displaytime then
		if hour < 10 then c = "0"..hour else c = hour end
		if minute < 10 then d = "0"..math.floor(minute) else d = math.floor(minute) end
		love.graphics.print(a.." "..b..", "..year.."\n"..c..":"..d, x,y)
	else
		love.graphics.print(a.." "..b..", "..year, x,y)
	end
end

function Display_Stats(thickness)

	--displaying engagement ratios
	local current = 0
	for f=1, 4 do
		ChangeToColor(f)
		love.graphics.rectangle("fill", current, WindowY - hudheight, WindowX*(Player.Engagement[f]/100), thickness)
		current = current + (WindowX*(Player.Engagement[f]/100))	
	end

	--displaying class ratios
	current = 0	
	for f=1, 4 do	
		love.graphics.setColor(64, 256-(64*f)-1, (64*f)-1)
		love.graphics.rectangle("fill", current, fieldst-thickness, WindowX*(Player.Category[f]/100), thickness)
		current = current + (WindowX*(Player.Category[f]/100))		
	end	

	love.graphics.setColor(255,255,255)	
	love.graphics.print(
	Player.Name..", "..Player.Age..
	"\n"..Categs[Player.MainC].." of the "..Forces[Player.MainE],
	0,WindowY - hudheight + WindowY*0.03)
end

----------------------------------------------------------------
----------------------VARIOUS GFX FUNCTIONS---------------------
----------------------------------------------------------------

--step sequence
function ScrollPlayground(period)

	--period: how many frames it waits until the beginning of the sequence
	local periodsec = (period/love.timer.getFPS())
	
	counter = counter+1
	
	if counter % period >= period - (steps2bedone) then 
		scroller = scroller + stepunit 
		love.graphics.print("Go to next step...", 0, fontsize*2)
		Datetime_Incrementor(rowtime/steps2bedone)
	else
		local secsleft = math.ceil(periodsec - ((counter % period)/love.timer.getFPS()))
		love.graphics.print(secsleft.." seconds left", 0, fontsize*2)
	end
	
	if counter % period == 0 then
		--Player_ChangeProp(Player.Engagement, math.random(1,#Player.Engagement), 3)
		--Player_ChangeProp(Player.Category, math.random(1,#Player.Category), 3)
	end
	
end

--set colors to current brightness
function Set_RGB()

	RGB_set = {
		{16*brightness -1, 	8*brightness -1, 	0},
		{16*brightness -1, 	16*brightness -1, 	16*brightness -1},
		{16*brightness -1,	16*brightness -1,	0},
		{16*brightness -1,	0,					0},
		{0	,				0,					16*brightness -1},
	}

end

--resolution test
function love.keypressed(key)

if key =="1" then love.window.setMode(240,320) WindowDeclaration() end
if key =="2" then love.window.setMode(360,480) WindowDeclaration() end
if key =="3" then love.window.setMode(600,800) WindowDeclaration() end
if key =="4" then love.window.setMode(720,900) WindowDeclaration() end
if key =="5" then love.window.setMode(960,1280) WindowDeclaration() end

if key =="9" then love.window.setMode(360,640) WindowDeclaration() end
end