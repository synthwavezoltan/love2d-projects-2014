----------------------------------------------------------------
-------------EVERY FUNCTIONS THAT CREATE VITAL DATA-------------
----------------------------------------------------------------

--creates a tileset from any pictures with any scaling
--a table with the name of "SetName" must be declared before its call
function Create_Tileset(SetName, ImageName, UnitSizeX, UnitSizeY, OrigX, OrigY)
	love.graphics.setDefaultFilter( "nearest", "nearest", 1 )
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local CharX = TilesetPics[1]:getWidth()*(TileSize/OrigX)
	local CharY = TilesetPics[1]:getHeight()*(TileSize/OrigY)
	
	local TilesX = CharX/TileSize
	local TilesY = CharY/TileSize

	for j=1, TilesY do
		for i=1, TilesX do			
			SetName[((j-1)*TilesX) + i] = love.graphics.newQuad((i-1)*UnitSizeX, (j-1)*UnitSizeY, UnitSizeX, UnitSizeY, CharX, CharY)
		end
	end	
end

--creating resolution-dependent variables; called in love.load and when resolution is changed
function WindowDeclaration()
	WindowX, WindowY = love.graphics.getWidth(), love.graphics.getHeight()
	
	TileSize = math.floor(WindowX/rowsize)
	
	stepunit = math.ceil((WindowX/240)/(2^scrolspd))	--amount of move on one frame (in pixels)
	steps2bedone = math.floor(TileSize/stepunit)
	
	--sizes
	fieldst = TileSize										--Y offset, where playground starts
	hudheight = WindowY*0.2									--size of the hud on the bottom of the screen
	btnheight = WindowY*0.08								--size of buttons --||--
	border = (WindowX - (TileSize*rowsize))/2				--X offset of playground
	
	fontsize = math.floor(WindowX/30)					--8 to 240, 12 to 360, 20 to 600, 24 to 720, 32 to 960
	
	font = love.graphics.newFont("slkscre.ttf", fontsize)
	love.graphics.setFont(font)		

	--gfx
	TilesetPics = {} Tileset = {}
	Create_Tileset(Tileset, 'TILESET.png', TileSize, TileSize, 8, 8)	

	ParallaxBodies = {}
	SetParallaxBodies()
	
	counter, scroller = 0,0
	
end

--creates playground; called in love.load
function Create_Playground(sizex, sizey)
	Field = { }	
	for j=1,sizex do
		Field[j] = { }
		for i=1, sizey do
			Field[j][i] = {cat = 1, eng = 5}
			
			if j==1 	then Field[j][i].cat = 2 end
			if j==sizex then Field[j][i].cat = 3 end
		end	
	end	
end

--player constructor - of some kind; called in love.load
function Create_Player()
	
	Player = {
		Name = "Rookie Rothens",
		Age = 24,
		Engagement = {25, 25, 25, 25},
		Category = {25, 25, 25, 25},
		MainE = 1, MainC = 1,
		Strength = 1, Agility = 1, Dexterity = 1, 
		Intelligence = 1, Toughness = 1, Luck = 1
		
	}

end

--adds a parallax decoration to the background
--[[
pic: Image; picture to display
dst: number; distance from playground, influences size, speed and brightness
mode: bool; true if picture moves constantly, false if only with playground
dir: string; moving direction of the parallax: up/down/upright/up-left/downright/down-left
repe: bool; true if parallax forms a constant stripe, false if appears only once
x,y: num; coordinates
--]]
function Create_ParallaxBody(pic, dst, mode, dir, repe, x, y)
	
	local maxplus = #ParallaxBodies+1
	ParallaxBodies[maxplus] = {
		image = pic,
		distance = dst,
		constant_mov = mode,
		direction = dir,
		repeated = repe,
		xpos = x,
		ypos = y
	}

end

--defining all parallax decorations
function SetParallaxBodies()
	
	--[[test
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 3.8, false, "up", false, 
						0, WindowY-hudheight - TileSize)
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 3.6, false, "up", false, 
						WindowX*0.25, WindowY-hudheight - TileSize)	
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 3.4, false, "up", false, 
						WindowX*0.55, WindowY-hudheight - TileSize)	

	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 3.2, false, "up", false, 
						0, WindowY-hudheight - TileSize)
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 3, false, "up", false, 
						WindowX*0.25, WindowY-hudheight - TileSize)	
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 2.8, false, "up", false, 
						WindowX*0.55, WindowY-hudheight - TileSize)	


	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 2.6, false, "up", false, 
						0, WindowY-hudheight - TileSize)
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 2.4, false, "up", false, 
						WindowX*0.25, WindowY-hudheight - TileSize)	
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 2.2, false, "up", false, 
						WindowX*0.55, WindowY-hudheight - TileSize)		

	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 2, false, "up", false, 
						0, WindowY-hudheight - TileSize)
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 1.8, false, "up", false, 
						WindowX*0.25, WindowY-hudheight - TileSize)	
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 1.6, false, "up", false, 
						WindowX*0.55, WindowY-hudheight - TileSize)	

	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 1.4, false, "up", false, 
						0, WindowY-hudheight - TileSize)
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 1.2, false, "up", false, 
						WindowX*0.25, WindowY-hudheight - TileSize)	
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 1, false, "up", false, 
						WindowX*0.55, WindowY-hudheight - TileSize)	


	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 0.8, false, "up", false, 
						0, WindowY-hudheight - TileSize)
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 0.6, false, "up", false, 
						WindowX*0.25, WindowY-hudheight - TileSize)	
	Create_ParallaxBody(love.graphics.newImage("TESTPIC.png"), 0.4, false, "up", false, 
						WindowX*0.55, WindowY-hudheight - TileSize)								
	--]]

end


