require 'gui'
require 'constructors'
require 'gameplay'
require 'tools'

function love.load()

	math.randomseed( os.time() )
	
	--configurable values
	Tilemax = 5000 			--max amount of tiles on a single playfield
	rowsize = 8				--number of tiles in a row (allowed between 4 and 15)
	scrolspd = -1			--speed of scrolling playground (min.: -1, max.: +2)
	scrolprd = 260			--waits this much frames before starts scrolling
	rowtime = 7				--amount of ingame time between two tiles (in minutes)
	brightness = 16
	
	--changing values
	FPS = 60
	currentrow = 1
	year, month, day, hour, minute = 2050,1,1,10,0
	
	--fix values
	WindowDeclaration()
	
	TilesetX, TilesetY = TileSize*3, TileSize*3	
	SizeX, SizeY = rowsize,math.floor(Tilemax/rowsize)
	
	RGB_set = {
		{16*brightness -1, 	8*brightness -1, 	0},
		{16*brightness -1, 	16*brightness -1, 	16*brightness -1},
		{16*brightness -1,	16*brightness -1,	0},
		{16*brightness -1,	0,					0},
		{0	,				0,					16*brightness -1},
	}
	
	Forces = {"Future Empire", "Coalition", "Cybergod", "Anarchists"}
	Categs = {"Hunter", "Soldier", "Protector", "Agent"}
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	counter, scroller = 0,0
	
	font_b = love.graphics.newFont("slkscre.ttf", b)
	

	--complex initializations
	Create_Player()
	Create_Playground(SizeX, SizeY)

	color = math.random(1,#RGB_set)
	posa = math.random(0, WindowX/2)
	posb = math.random(WindowX/2, WindowX - TileSize)
end

function love.draw()
	
	Display_ParallaxBodies()
	--if not love.keyboard.isDown("x") then Display_Playground(16) end
	Display_Playground(16)				--playground
	Display_Interface(8)				--GUI of some kind	
	Display_Buttons()					--buttons	
	Display_Stats(WindowY*0.02)
	
	--write out FPS
	love.graphics.setColor(255,255,255)
	love.graphics.print(love.timer.getFPS().."\n"..FPS,WindowX*0.9,TileSize)

	ScrollPlayground(scrolprd)
	--[[
	for f=1, 4 do
		love.graphics.print(Player.Engagement[f], (WindowX/5)*f, WindowY-hudheight)
		love.graphics.print(Player.Category[f], (WindowX/5)*f, fieldst)
	end
	
	love.graphics.print(
	 (WindowX*(Player.Engagement[1]/100))
	+(WindowX*(Player.Engagement[2]/100))
	+(WindowX*(Player.Engagement[3]/100))
	+(WindowX*(Player.Engagement[4]/100))
	.."\n"..
	Player.Engagement[1]+Player.Engagement[2]+
	Player.Engagement[3]+Player.Engagement[4],
	stepunit*30,stepunit*30)--]]
end

function love.update(dt)
	
	FPS = math.floor(1/dt)
	currentrow = math.floor(scroller/TileSize)+1
	
	Player_SetProps()
end

function Datetime_Incrementor(spd)

	--year, month, day, hour, minute
	minute = minute + spd
	if minute > 59 then hour = hour + 1 minute = minute - 59 end
	if hour > 23 then day = day + 1 hour = hour - 23 end
	
	if month == 2 and day > 28 													then month = month + 1 day = day - 28
	elseif (month == 4 or month == 6 or month == 9 or month == 11) and day > 30	then month = month + 1 day = day - 30
	elseif (month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12) and day > 31
		then month = month + 1 day = day - 31
	end
end

function ChangeToColor(col)
	love.graphics.setColor(RGB_set[col][1],RGB_set[col][2],RGB_set[col][3])
end

function FindLargest(setA)

	local x,y,z=0,0,0
	local tempset = {}
	local ord = {}
	
	for x=1, #setA do
		table.insert(tempset, setA[x])
		table.insert(ord, x)
	end

	for x=1, #tempset-1 do
		if tempset[x] >= tempset[x+1] then 
			z = tempset[x]
			tempset[x] = tempset[x+1]
			tempset[x+1] = z
			
			z = ord[x]
			ord[x] = ord[x+1]
			ord[x+1] = z
		end
	end
	
	return ord[#ord]

end








