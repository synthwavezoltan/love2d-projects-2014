--true if a given tile is impassable
function Physics(x,y)
	
	local returner = false
	
	if Field[x][y] <3 or (Field[x][y] >4 and Field[x][y] <9)
	then returner = true else returner = false end
	
	return returner
end

--obvious function name is obvious
function Human_Motion(subject)

	local p, q = HumanTile(subject)
	
	if subject.timer > 0 then
		subject.timer = subject.timer - 1
		--the actual moving itself: up-down-left-right
		if subject.dir == 1 then
			subject.Y = subject.Y - 1
		elseif subject.dir == 2 then
			subject.Y = subject.Y + 1
		elseif subject.dir == 3 then
			subject.X = subject.X - 1		
		elseif subject.dir == 4 then
			subject.X = subject.X + 1
		end
		
	end
	
	if subject.timer==0 or
		(subject.dir == 1 and Physics(p,q-1)) or
		(subject.dir == 2 and Physics(p,q+1)) or
		(subject.dir == 3 and Physics(p-1,q)) or
		(subject.dir == 4 and Physics(p+1,q))			
	then 
		subject.timer = math.random(32,256)
		local no = subject.dir
		while subject.dir == no do subject.dir = math.random(1,5) end
	end
end

--I guess I'm  gonna use this one heavily.
function GenerateWindow(gimmeastring, sizex, sizey)
	
	love.graphics.setColor(0,0,0)
	love.graphics.rectangle("fill", 512-(sizex/2), 384-(sizey/2), sizex, sizey)
	love.graphics.setColor(255, 255, 255)	
	love.graphics.print(gimmeastring, 512-(sizex/2)+32, 384-(sizey/2)+32)

end

--helpmenu itself
function Human_Documentary(id)
	
	local sum = {}
	
	for i=1, 16 do table.insert(sum,1) end
	for i=1, #HumanSet do sum[HumanSet[i].Type] = sum[HumanSet[i].Type]+1 end
	
	love.graphics.setColor(0,0,0)
	love.graphics.rectangle("fill", 32, 32, 960, 256)
	love.graphics.setColor(255, 255, 255)	
	
	love.graphics.draw(humanmusem, 64, 64)
	for i=1, 16 do love.graphics.print(sum[i], 59*i, 192) end
	
	love.graphics.draw(TilesetPic, Tileset[10], 59*id, 224)
	
	if id ~= 0 then GenerateWindow(Description(id), 416, 152) end

end

--transit panel
function TransitPanel(id)
	
	local sum = {}
	
	for i=1, 16 do table.insert(sum,1) end
	for i=1, #HumanSet do sum[HumanSet[i].Type] = sum[HumanSet[i].Type]+1 end
	
	love.graphics.setColor(0,0,0)
	love.graphics.rectangle("fill", 32, 32, 960, 256)
	love.graphics.setColor(255, 255, 255)	
	
	love.graphics.draw(humanmusem, 64, 64)
	for i=1, 16 do love.graphics.print(sum[i].."\n"..TransitDemand[i].."\n"..sum[i]-TransitDemand[i], 59*i, 192) end
	
	if id ~= 0 then GenerateWindow(Transit_Desc(), 432, 192) end

end

--generate new transit; actually just redoing everything
function NewTransit()

	--transit properties
	TransitDemand = {}
	for i=1, 16 do 
		TransitDemand[i] = math.random(0, 10)
		transittotal = transittotal + TransitDemand[i] end

end

--consequences of accepting
function AcceptTransit()
	
	--decrease amount of people
	for i=1, 16 do
	
		for j= #HumanSet, 1, -1 do
			
			if TransitDemand[i] > 0 then
				if HumanSet[j].Type == i then 
					table.remove(HumanSet, j)
					TransitDemand[i] = TransitDemand[i]-1
				end
			end		
		end
	end
	

	--adding totally random newbies		
	for i=1, transittotal do
		local p, q = math.random(3,SizeX-2), math.random(3,SizeY-2)

		while Field[p][q] ~= 4 and Field[p][q] ~= 3 and Field[p][q] == 1 do
			p, q = math.random(3,SizeX-2), math.random(3,SizeY-2)
		end
		
		HumanSet[i] = { X =p*TileSize, Y = q*TileSize, Type = math.random(1,16),
		dir =math.random(1,5), timer = 128 }
	end

	--remove those who aren't on a valid tile
	for i=#HumanSet, 1, -1 do
		local p, q = HumanTile(HumanSet[i])
		if Physics(p, q) then table.remove(HumanSet, i) end
	end
	
end