function love.keypressed(key)
	
	if Menu then
		if key == "x"  then Menu = false 
			Transit = true 
			love.audio.stop(intro_audio) 
			love.audio.play(click_audio) end

	else
	
		if key == "c" then 
			Help = not Help 
			love.audio.rewind(click_audio)
			love.audio.stop(click_audio)
			love.audio.play(click_audio) end
		
		if key == "y" and Transit then 
			Transit = false
			AcceptTransit()
			NewTransit()
			love.audio.rewind(click_audio)
			love.audio.stop(click_audio)
			love.audio.play(click_audio)		
		elseif key == "n" and Transit then 
			Transit = false
			NewTransit()
			love.audio.rewind(click_audio)
			love.audio.stop(click_audio)
			love.audio.play(click_audio)	
		end
		
		if key =="q" and not Transit and not Help then 		
			for i=1, 7 do
				love.audio.rewind(thegods[i])
				love.audio.stop(thegods[i])
			end
			
			love.audio.play(thegods[math.random(1,7)])
		end
	end

end