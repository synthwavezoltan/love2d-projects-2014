--[[
Rothens, ha ezt valaha elolvasod, akkor kurvára köszi hogy rávettél
hogy ne adjam fel az LD#30-at :D talán így ez jobb mint entry is lesz,
mint egy forgó HTML négyzet. - K
]]
	

require 'roomshape'
require 'behaviour'
require 'key_n_mouse'
require 'texts'

function love.load()
	
	math.randomseed( os.time() )
	--key values
	TileSize = 32
	SizeX, SizeY = 32, 24
	
	--technical variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	
	--game variables
	timer = 0
	
	railX = 0
	trainX = 1279
	trainsize = 0
	showntut = 0
	
	score = 0
	transittotal = 0
	
	Menu = true
	Help = false
	Transit = false
	
	mouseX, mouseY = love.mouse.getX(), love.mouse.getY()
	
	--sounds
	choo_audio = love.audio.newSource("sounds/chouchou.wav")
	intro_audio = love.audio.newSource("sounds/intro.wav")
	click_audio = love.audio.newSource("sounds/click.wav")
	thegods = {}
	
	for i=1, 7 do thegods[i] = love.audio.newSource("sounds/"..i..".wav", "static") end
	
	--pics
	credit = love.graphics.newImage('CREDITS.png')
	title = love.graphics.newImage('TITLE.png')
	humanmusem = love.graphics.newImage('HUMAN_BIG.png')
	
	--font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)		
	
	--tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	
	Tileset = {}
	local tilesetX = TilesetPic:getWidth()
	local tilesetY = TilesetPic:getHeight()
	
	for j=1, tilesetX/TileSize do
		for i=1, tilesetY/TileSize  do 
			Tileset[((i-1)*(tilesetX/TileSize))+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, tilesetX, tilesetY)
		end
	end	

	--humans' textures
	HumansPic = love.graphics.newImage('HUMAN.png')
	Humans = {}
	for i=1, 16 do
		Humans[i] = love.graphics.newQuad((i-1)*4, 0, 4, 16, 64, 16) end
		
	--description of humans
	Humanity = {
	{ Nickname = "Katamori", Sex = "male", Age = "20", Good = "Programming", Bad = "Introvert", Genre = "Thrash metal" },
	{ Nickname = "Tinkerbell", Sex = "female", Age = "19", Good = "Sex", Bad = "at Thinking", Genre = "Dubstep" },
	{ Nickname = "Ahmar Sulur", Sex = "male", Age = "34", Good = "Bombing", Bad = "at Democracy", Genre = "N/A" },
	{ Nickname = "Prof. Sherman", Sex = "male", Age = "63", Good = "Thinking", Bad = "Obese", Genre = "Classical" },
	{ Nickname = "Zweistein", Sex = "male", Age = "57", Good = "Physics", Bad = "Crazy", Genre =  "N/A" },	
	{ Nickname = "MetalJamal_91", Sex = "male", Age = "23", Good = "Vocals", Bad = "Bad at English", Genre = "Death metal" },
	{ Nickname = "Tim Mustaine", Sex = "male", Age = "52", Good = "Guitar", Bad = "Sometimes asshole", Genre = "Thrash metal" },
	{ Nickname = "Denzel", Sex = "male", Age = "26", Good = "Jokes", Bad = "Gerontophil", Genre = "N/A" },
	{ Nickname = "Papa Cosby", Sex = "male", Age = "72", Good = "Jokes", Bad = "Crazy", Genre = "jazz" },
	{ Nickname = "Dee Dee", Sex = "female", Age = "11", Good = "Activity", Bad = "ANNOYING", Genre = "N/A" },
	{ Nickname = "Jeb Kerman", Sex = "kermale", Age = "N/A", Good = "Astrophysics", Bad = "Getting lost", Genre = "N/A" },
	{ Nickname = "Katamori Minecraft", Sex = "male", Age = "17", Good = "Crafting", Bad = "Dies often", Genre = "heavy metal" },
	{ Nickname = "codboy_99", Sex = "male", Age = "14", Good = "Being stupid", Bad = "n00b", Genre = "hip-hop" },	
	{ Nickname = "Old wh", Sex = "female", Age = "36", Good = "Drinking", Bad = "at Sex", Genre = "retro disco" },
	{ Nickname = "Crazy granny", Sex = "female", Age = "70", Good = "Cooking", Bad = "Fragile", Genre = "classical" },	
	{ Nickname = "Edward Elric", Sex = "male", Age = "15", Good = "Alchemy", Bad = "Short", Genre = "N/A" },		
	}
	
	--moving possibilites
	Mov = { "up", "down", "left", "right", "no" }
	
	--playing field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = 1
		end
		
	end
	
	--making the playfield itself
	RoomShapeGenerator()
	
	--the train
	TrainSequence = { }
	trainsize = math.random(2, 20)
	for i=1, trainsize-1 do TrainSequence[i] = math.random(11,15) end
	TrainSequence[trainsize] = 16
	
	trainX = 512
	
	--the humans
	HumanSet = {}
	
	for i=1, 500 do
		
		local p, q = math.random(3,SizeX-2), math.random(3,SizeY-2)

		while Field[p][q] ~= 4 and Field[p][q] ~= 3 and Field[p][q] == 1 do
			p, q = math.random(3,SizeX-2), math.random(3,SizeY-2)
		end
		
		HumanSet[i] = { X =p*TileSize, Y = q*TileSize, Type = math.random(1,16),
		dir =math.random(1,5), timer = 128 }
		

	end
	
	--remove those who aren't on a valid tile
	for i=#HumanSet, 1, -1 do
		local p, q = HumanTile(HumanSet[i])
		if Physics(p, q) then table.remove(HumanSet, i) end
	end
	
	--current mission
	Mission = {
		Type = math.random(1,16),
		Amount = math.random(100,200)
	}
	
	--first new transit
	NewTransit()
	
	--goalnumber
	Current = 0
	
	for i=1, #HumanSet do 
		if HumanSet[i].Type == Mission.Type 
		then Current = Current + 1 end 
	end
	
	--finally, the intro speech
	love.audio.play(intro_audio)
end

function love.draw()
	
	if not Menu then
	
		--playing field
		for j=1,SizeX do
			for i=1, SizeY do
				love.graphics.draw(TilesetPic, Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
			end
		end

		--drawing the railway, the train, and the humans
		for i=1, SizeX do love.graphics.draw(TilesetPic, Tileset[9], (i-1)*TileSize, (railX-1)*TileSize) end 	
		for i=1, #TrainSequence do love.graphics.draw(TilesetPic, Tileset[TrainSequence[i]], trainX+(i-1)*TileSize, (railX-1)*TileSize) end
		for i=1, #HumanSet do love.graphics.draw(HumansPic, Humans[HumanSet[i].Type], HumanSet[i].X, HumanSet[i].Y) 
								--local p,q = HumanTile(HumanSet[i])
								--love.graphics.print(HumanSet[i].timer, HumanSet[i].X, HumanSet[i].Y)
		end
		
		--texts
		--what you have to do?
		love.graphics.setColor(0,0,0)
		love.graphics.rectangle("fill", 32, 0, 960, 32)
		love.graphics.rectangle("fill", 32, 736, 960, 32)
		love.graphics.setColor(255, 255, 255)
		
		
		love.graphics.print("task no. "..(score+1)..": gather "..Mission.Amount.." "..
							Humanity[Mission.Type].Nickname.." clones to the station. "..
							"(current: "..Current..")", 48, 8)
							
		love.graphics.print(tutorial[showntut], 48,746)			
		
		love.graphics.print(#HumanSet, 0,0)
	
	--main menu
	else
		love.graphics.draw(title, 64, 32) 
		GenerateWindow(intro, 896, 288)	
		love.graphics.draw(credit, 64, 512)
	end 	
	
	local p = 0
	if mouseX < 16*64 then p = math.floor(mouseX/59) end
		
	--help & transit
	if Help then Human_Documentary(p)
	elseif Transit then TransitPanel(p) end
	

	
	
	
	
end

function love.update(dt)
	
	if not Menu then timer = timer+1 end
	if timer%400 == 0 then showntut = math.random(1, #tutorial) end
	mouseX, mouseY = love.mouse.getX(), love.mouse.getY()
	
	Current = 0	
	for i=1, #HumanSet do 
		if HumanSet[i].Type == Mission.Type 
		then Current = Current + 1 end 
	end	
	
	--new task
	if Current>=Mission.Amount then
		Mission = {
			Type = math.random(1,16),
			Amount = math.random(100,200)
		}	
	end
	
	if not Menu and not Help then
		
		--train motion (recalculate it in a whole)
		if trainX > 1280 then
			TrainSequence = { }	
			trainsize = math.random(2, 20)
			for i=1, trainsize-1 do TrainSequence[i] = math.random(11,15) end
			TrainSequence[trainsize] = 16
			trainX = 0-(trainsize*TileSize) 
			
			love.audio.play(choo_audio)
		else	
			if not Transit then
				trainX =  trainX + 1				
				--human motion
				for i=1, #HumanSet do
					Human_Motion(HumanSet[i])	
				end
			
			end	
		end
		
		--stop for transit
		if trainX + ((#TrainSequence/2)*TileSize) == 512 then Transit = true end		
		
	end
	

end


--a useful function I wrote back in the day
function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if x-1 >= 1 	and Table[x-1][y] == valueToCheck then asd=asd+1 end
	if x+1 <= SizeX and Table[x+1][y] == valueToCheck then asd=asd+1 end
	if y-1 >= 1 	and Table[x][y-1] == valueToCheck then asd=asd+1 end
	if y+1 <= SizeY and Table[x][y+1] == valueToCheck then asd=asd+1 end
	return asd
end

--counts a human's current tile from its coordinates
function HumanTile(subject)

	local rem_x, rem_y = subject.X%TileSize, subject.Y%TileSize
	local purex, purey = subject.X-rem_x, subject.Y-rem_y
	local tilex, tiley = purex/TileSize, purey/TileSize
	
	return tilex+1,tiley+1

end




