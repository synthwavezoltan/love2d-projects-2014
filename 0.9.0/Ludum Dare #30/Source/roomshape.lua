function RoomShapeGenerator()
	
	local x, y = math.random(1,SizeX), math.random(1,SizeY)	
	
	Field[16][12] = 4
	
	--walkable place in general
	for j=1, 1000 do				
		x, y = math.random(2,SizeX-1), math.random(2,SizeY-1)
	
		while Field[x][y] ~= 4 and CountNeighbours(x,y, 4, Field)==0 do
			x, y = math.random(2,SizeX-1), math.random(2,SizeY-1)
		end
		
		Field[x][y] = 4	

	end
		
	--put down carpet on a way that makes sense..
	Field[16][12] = 3
	
	for i=1, 100 do
		x, y = math.random(2,SizeX-1), math.random(2,SizeY-1)
		while Field[x][y] ~= 3 and CountNeighbours(x,y, 3, Field)==0 do 
			x, y = math.random(2,SizeX-1), math.random(2,SizeY-1) end		
		Field[x][y] = 3				
	end
	
	--the railroad
	railX = math.random(2, SizeY/2)
	
	for i=1, SizeX do Field[i][railX] = 1 end
		
	--post-processing
	for k=1, SizeX-1 do
		for l=2, SizeY-1 do
			
			for p=1, 4 do
			
				--putting down windows
				if Field[k][l-1] <3 and (Field[k][l] == 3 or Field[k][l] == 4) then Field[k][l] = math.random(5, 8) end
				if Field[k][l] <3 and l ~= railX then Field[k][l] = 1+math.random(2,4)%2 end
				
				--3 and 4 neighbour tiles become "solid"
				if CountNeighbours(k, l, 1, Field) > 2 then Field[k][l] = 1 end
				if CountNeighbours(k, l, 3, Field) > 2 then Field[k][l] = 3 end
				if CountNeighbours(k, l, 4, Field) > 2 then Field[k][l] = 4 end	

			end
		end
	end	

end

