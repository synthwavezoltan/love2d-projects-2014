require 'bookkeeping_items'

--the functions of bookkeeping

--item values must be increased or decreased in certain cases; it does that!
function Item_Calculator()

end

--constant balance: summarizing of every bookkeping items
function Item_Summarizer()
	Balance_Money, Balance_Energy, Balance_Food, Balance_Water, Balance_Oxygen = 0,0,0,0,0

	--in order: money, energy, food, water, oxygen
	for i=1, #Bookkeeping_Items_Money do
		Balance_Money = Balance_Money + (Bookkeeping_Items_Money[i].Value * Bookkeeping_Items_Money[i].Amount)
	end
	
	for i=1, #Bookkeeping_Items_Energy do
		Balance_Energy = Balance_Energy + (Bookkeeping_Items_Energy[i].Value * Bookkeeping_Items_Energy[i].Amount)
	end	

	for i=1, #Bookkeeping_Items_Food do
		Balance_Food = Balance_Food + (Bookkeeping_Items_Food[i].Value * Bookkeeping_Items_Food[i].Amount)
	end		
	
	for i=1, #Bookkeeping_Items_Water do
		Balance_Water = Balance_Water + (Bookkeeping_Items_Water[i].Value * Bookkeeping_Items_Water[i].Amount)
	end
	
	for i=1, #Bookkeeping_Items_Oxygen do
		Balance_Oxygen = Balance_Oxygen + (Bookkeeping_Items_Oxygen[i].Value * Bookkeeping_Items_Oxygen[i].Amount)
	end
	

end

--estimating function of the total value of the property
--not necessary yet
function Value_Calculator()

end