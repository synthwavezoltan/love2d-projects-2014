--click events
function love.mousepressed(x,y,button)	

	if button == "l" then
		MenuButtons()

	
	--temporary function to go back to hub easily
	elseif button == "r" then
		Active_Menu = 0
		Service_Organizer_State = 0
		Bookkeeping_Menu_State = 0
	
	
	elseif button == "wu" then
		if gamespeed < 19 then gamespeed = gamespeed+1 end
	elseif button == "wd" then
		if gamespeed > 0 then gamespeed = gamespeed-1 end
	end

end

--using menu system with mouse
function MenuButtons()

	--hubpanel interaction
	if Active_Menu == 0 then
		if MouseX>815 and MouseX<1135 and MouseY>25 and MouseY<215 then
			Active_Menu = 1
		elseif MouseX>465 and MouseX<525 and MouseY>220 and MouseY<475 then
			Active_Menu = 2
		elseif MouseX>570 and MouseX<760 and MouseY>265 and MouseY<515 then
			Active_Menu = 3
		elseif MouseX>805 and MouseX<900 and MouseY>260 and MouseY<440 then
			Active_Menu = 4
		elseif MouseX>945 and MouseX<1130 and MouseY>220 and MouseY<475 then
			Active_Menu = 5
		elseif MouseX>290 and MouseX<415 and MouseY>220 and MouseY<430 then
			Active_Menu = 6	
		elseif MouseX>570 and MouseX<760 and MouseY>25 and MouseY<215 then
			Active_Menu = 7
		elseif MouseX>140 and MouseX<230 and MouseY>220 and MouseY<470 then
			Active_Menu = 8		
		elseif MouseX>310 and MouseX<565 and MouseY>105 and MouseY<175 then
			Active_Menu = 9	
		elseif MouseX>144 and MouseX<271 and MouseY>96 and MouseY<159 then
			Active_Menu = 10				
		end

	
	--facility menu actions		
	elseif Active_Menu == 1 then

		--menuchanging buttons in service organizer
		if MouseY>656 and MouseY<688 then
			if MouseX>172 and MouseX<425 then
				if Service_Organizer_State == 0 then Active_Menu = 0 else Service_Organizer_State = 0 end
			elseif MouseX>511 and MouseX<767 then
				if Service_Organizer_State == 1 then Active_Menu = 0 else Service_Organizer_State = 1 end
			elseif MouseX>855 and MouseX<1110 then
				if Service_Organizer_State == 2 then Active_Menu = 0 else Service_Organizer_State = 2 end
			end
		end
	
	--stock monitoring
	elseif Active_Menu == 2 then
	
	--bookkeeping menu actions
	elseif Active_Menu == 3 then
		
		--on the main page
		if Bookkeeping_Menu_State == 0 then
			
			--internal menu changes
			if MouseX>528 and MouseX<750 and MouseY>208 and MouseY<240 then
				Bookkeeping_Menu_State = 1
			elseif MouseX>528 and MouseX<750 and MouseY>248 and MouseY<280 then
				Bookkeeping_Menu_State = 2
			elseif MouseX>528 and MouseX<750 and MouseY>288 and MouseY<320 then
				Bookkeeping_Menu_State = 3
			elseif MouseX>528 and MouseX<750 and MouseY>328 and MouseY<360 then
				Active_Menu = 0					
			end

		--on the balance menu
		elseif Bookkeeping_Menu_State == 1 then
		
			--back and forth
			if MouseY>640 and MouseY<708 then
				if MouseX>64 and MouseX<128 then
					Bookkeeping_Menu_State = 0	
				elseif MouseX>1152 and MouseX<1215 then	
					Bookkeeping_Menu_State = 2	
				end
			end	
			
			--back to hub
			if MouseY>16 and MouseY<80 
			and MouseX>1152 and MouseX<1215 then
				Active_Menu = 0
				Bookkeeping_Menu_State = 0	
			end	
			
			--item type selecting
			if MouseX>64 and MouseX<128 then	
				if MouseY>136 and MouseY<200 then
					Bookkeeping_Item_State = 0
				elseif MouseY>328 and MouseY<390 then
					Bookkeeping_Item_State = 1									
				elseif MouseY>520 and MouseY<583 then
					Bookkeeping_Item_State = 2
				end
			end				
			
			--item category selecting
			if MouseX>1152 and MouseX<1215 then	
				if MouseY>136 and MouseY<200 then
					Bookkeeping_Item_Category = 0
				elseif MouseY>232 and MouseY<295 then
					Bookkeeping_Item_Category = 1					
				elseif MouseY>328 and MouseY<390 then
					Bookkeeping_Item_Category = 2					
				elseif MouseY>424 and MouseY<487 then
					Bookkeeping_Item_Category = 3					
				elseif MouseY>520 and MouseY<583 then
					Bookkeeping_Item_Category = 4
				end
			end
			
			
		end
	end

end