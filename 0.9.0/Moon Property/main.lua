require 'gui'
require 'misc'
require 'mouse'
require 'rounds'
require 'bookkeeping'
require 'keybinding'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetRow, TilesetCol = 12,12
	TilesetX, TilesetY = TilesetRow*TileSize, TilesetCol*TileSize
	
	--tilegrid-related properties
	SizeX, SizeY = 1024, 1024
	ScreenX, ScreenY = 32, 18
	OffsetX, OffsetY = 0,0					-- in tile units
	
	SmoothOffsetX, SmoothOffsetY = 0,0 		-- in pixels
	ScreenScrolling = "no"
	
	--general vars
	x,y,z,i,j,k = 0,0,0,0,0,0
	game_clock = 0
	FPS = 0
	
	--mouse vars
	MouseX, MouseY = 0,0
	
	--main panel vars
	Active_Menu = 0
	Service_Organizer_State = 0
	Bookkeeping_Menu_State = 0
	Bookkeeping_Item_State = 0
	Bookkeeping_Item_Category = 0
	
	--gametime vars
	gamespeed = 1 --(min 0, max 19)
	Year, Month, Day, Hour, Minute, Second = 2031, 2, 1, 8, 0, 0
	Daily_Second = 8*3600
	
	--game properties	
	--main resources
	Money = 1000000
	Food, Water, Oxygen = 9999,9999,9999
	Energy = 10000
	--changes per hours
	Balance_Money = 0 
	Balance_Food = 0
	Balance_Water = 0
	Balance_Oxygen = 0
	Balance_Energy = 0
	
	--other non-essential resources
	Popularity = 0
	
	Cabin_Lvl = 1
	FacilityList = {}	
	
	
	
	--pictures
	MainHUB = love.graphics.newImage('MAINHUB.png')

	FacilityHUD = love.graphics.newImage('FAC_HUD.png')	
	FacilityMapHUD = love.graphics.newImage('FACMAP_HUD.png')	
	Panorama_HUD = love.graphics.newImage('PANORAMA_HUD.png')	
	Panorama = love.graphics.newImage('PANORAMA.png')
	
	BookMenu = {
		love.graphics.newImage('BOOK_MAIN.png'),
		love.graphics.newImage('BOOK_ITEM.png'),
		love.graphics.newImage('BOOK_FINA.png'),
		love.graphics.newImage('BOOK_OUTD.png')
	}

	--tileset used for surface camera
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, TilesetRow do
		for i=1, TilesetCol do 
			Tileset[((i-1)*TilesetCol)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--initializing font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)	
	
	--uploading field
	Field_Camera = { }
	Field_Map = { }
	
	for j=1,SizeX do
	
		Field_Camera[j] = { }
		Field_Map[j] = { }
	
		for i=1, SizeY do
			Field_Camera[j][i] = 1
			Field_Map[j][i] = math.random(2,3)
		end
		
	end
	
end

function love.draw()
	
	Display_Interface()
	
	love.graphics.print(OffsetY.."\n"..FPS.."\nspd = "..gamespeed,0,0)
	love.graphics.print(MouseX.."\n"..MouseY,1200,100)


	love.graphics.print(math.floor(FPS*math.pow(2,gamespeed)/3600/24) .." days per second",96,800)
end

function love.update(dt)
	Constants(dt)					--declaring constant variables
	
	
	Screen_Scrolling()				--scrolling screen if it's triggered
	KeyActions_Screen_Scrolling()	--triggering aforementioned effect
	
	
	if game_clock%25 == 0 
	then Time() end

	
end













function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




