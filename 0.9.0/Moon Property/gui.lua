function Display_Interface()

	-- main hub
	if Active_Menu == 0 then
		love.graphics.draw(MainHUB,0,0)
		
		Display_Time(152,32)	
		Display_Date(160,496)
		love.graphics.print(math.floor(Daily_Second), 160,512)
			
		love.graphics.print(math.floor(Money), 605, 496)
		love.graphics.print(math.floor(Food),340,32)
		love.graphics.print(math.floor(Water),440,32)
		love.graphics.print(math.floor(Oxygen),640,32)
		

		
	
	--service organizer
	elseif Active_Menu == 1 then
		--facility list
		if Service_Organizer_State == 0 then
		
			love.graphics.draw(FacilityHUD,0,0)
			love.graphics.print("ordinary way of using this menu", 256,256)
			
			
			
			
		--property map
		elseif Service_Organizer_State == 1 then

			for j=1,ScreenX+2 do
				for i=1, ScreenY+6 do
					love.graphics.draw(TilesetPic, 
										Tileset[Field_Map[j+OffsetX][i+OffsetY]], 
										SmoothOffsetX+128+(j-2)*TileSize, 
										SmoothOffsetY+(i-2)*TileSize)
				end
			end	
		
			love.graphics.draw(FacilityMapHUD,0,0)
			
		
			
		--surface camera
		elseif Service_Organizer_State == 2 then
		
			for j=1,ScreenX+2 do
				for i=1, ScreenY+6 do
					love.graphics.draw(TilesetPic, 
										Tileset[Field_Camera[j+OffsetX][i+OffsetY]], 
										SmoothOffsetX+128+(j-2)*TileSize, 
										SmoothOffsetY+(i-2)*TileSize)
				end
			end
			
			
			for i=1, (SizeX*TileSize)/1024 do
			love.graphics.draw(Panorama, ((i-1)*1024)+128-(OffsetX*TileSize)+SmoothOffsetX, -(OffsetY*TileSize)+SmoothOffsetY)
			end
			
			love.graphics.draw(Panorama_HUD, 0,0)	
			
		end
		
		Display_Time(48,16)	
		Display_Date(1024,16)
	
	--stock monitoring
	elseif Active_Menu == 2 then
		love.graphics.print("stock monitoring", 256,256)

		
	--bookkeeping menu
	elseif Active_Menu == 3 then
		
		love.graphics.draw(BookMenu[Bookkeeping_Menu_State+1],0,0)
		
		--main page
		if Bookkeeping_Menu_State == 0 then

			Display_Time(592,100)	
			Display_Date(530,60)
		
			love.graphics.print("Total est. value:", 544,136)
			love.graphics.print("(TOTALVALUE)", 544,176)
			

			if MouseX>528 and MouseX<750 and MouseY>208 and MouseY<240 then
				love.graphics.print("click here!", 544,216)
			else
			love.graphics.print("Resource Balance", 544,216)
			end		
			
			if MouseX>528 and MouseX<750 and MouseY>248 and MouseY<280 then
				love.graphics.print("click here!", 544,256)
			else
				love.graphics.print("Monthly Result", 544,256)
			end		
			
			if MouseX>528 and MouseX<750 and MouseY>288 and MouseY<320 then
				love.graphics.print("click here!", 544,296)
			else
				love.graphics.print("Equipment State", 544,296)
			end		
			
			if MouseX>528 and MouseX<750 and MouseY>328 and MouseY<360 then
				love.graphics.print("click here!", 544,336)
			else
				love.graphics.print("back to main hub",544,336)
			end
			
			--main resources
			Display_Bookkeeping_Resources("Money", Money, Balance_Money, 230,140)
			Display_Bookkeeping_Resources("Energy", Energy, Balance_Energy, 810,140)
			Display_Bookkeeping_Resources("Food", Food, Balance_Food, 230, 480)
			Display_Bookkeeping_Resources("Water", Water, Balance_Water, 520,480)
			Display_Bookkeeping_Resources("Oxygen", Oxygen, Balance_Oxygen, 810,480)
		
		--balance menu
		elseif Bookkeeping_Menu_State == 1 then
		
			Display_Time(592,665)	
			Display_Date(530,40)

			--writing out item types and categories
			if Bookkeeping_Item_State == 1 then
				love.graphics.print("Everything", 854,665)	
			else
				if Bookkeeping_Item_Category == 0 then
					love.graphics.print("Money", 854,665)					
				elseif Bookkeeping_Item_Category == 1 then
					love.graphics.print("Energy", 854,665)					
				elseif Bookkeeping_Item_Category == 2 then						
					love.graphics.print("Food", 854,665)					
				elseif Bookkeeping_Item_Category == 3 then
					love.graphics.print("Water", 854,665)					
				elseif Bookkeeping_Item_Category == 4 then
					love.graphics.print("Oxygen", 854,665)	
				end
			end
			
			--the exact item list
			if Bookkeeping_Item_State == 0 then
				love.graphics.print("Incomes only", 224,665)
				
				i,j = 1,0
				
				if Bookkeeping_Item_Category == 0 then
					while j<#Bookkeeping_Items_Money do
					
						j = j + 1
						
						if Bookkeeping_Items_Money[j].Value >= 0 then
							love.graphics.print(Bookkeeping_Items_Money[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Money[j].Value*Bookkeeping_Items_Money[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end	
					end
				elseif Bookkeeping_Item_Category == 1 then				
					while j<#Bookkeeping_Items_Energy do
					
						j = j + 1
					
						if Bookkeeping_Items_Energy[j].Value >= 0 then
							love.graphics.print(Bookkeeping_Items_Energy[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Energy[j].Value*Bookkeeping_Items_Energy[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end				
				elseif Bookkeeping_Item_Category == 2 then						
					while j<#Bookkeeping_Items_Food do
						
						j = j + 1
						
						if Bookkeeping_Items_Food[j].Value >= 0 then
							love.graphics.print(Bookkeeping_Items_Food[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Food[j].Value*Bookkeeping_Items_Food[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end					
				elseif Bookkeeping_Item_Category == 3 then
					while j<#Bookkeeping_Items_Water do
					
						j = j + 1
					
						if Bookkeeping_Items_Water[j].Value >= 0 then
							love.graphics.print(Bookkeeping_Items_Water[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Water[j].Value*Bookkeeping_Items_Water[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end				
				elseif Bookkeeping_Item_Category == 4 then
					while j<#Bookkeeping_Items_Oxygen do
					
						j = j + 1
					
						if Bookkeeping_Items_Oxygen[j].Value >= 0 then
							love.graphics.print(Bookkeeping_Items_Oxygen[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Oxygen[j].Value*Bookkeeping_Items_Oxygen[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end	
				end
				
			elseif Bookkeeping_Item_State == 1 then
				love.graphics.print("Balance", 224,665)
				
				love.graphics.print("Current daily change in amount of money", 224,215)
				love.graphics.print("Current daily change in amount of energy", 224,255)
				love.graphics.print("Current daily change in amount of food", 224,295)
				love.graphics.print("Current daily change in amount of water", 224,335)
				love.graphics.print("Current daily change in amount of oxygen", 224,375)
				
				love.graphics.print(Balance_Money*24, 850,215)
				love.graphics.print(Balance_Energy*24, 850,255)
				love.graphics.print(Balance_Food*24, 850,295)
				love.graphics.print(Balance_Water*24, 850,335)
				love.graphics.print(Balance_Oxygen*24, 850,375)
				
			elseif Bookkeeping_Item_State == 2 then
				love.graphics.print("Costs only", 224,665)	

				i,j = 1,0
				
				if Bookkeeping_Item_Category == 0 then
					while j<#Bookkeeping_Items_Money do
						
						j = j + 1
						
						if Bookkeeping_Items_Money[j].Value < 0 then
							love.graphics.print(Bookkeeping_Items_Money[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Money[j].Value*Bookkeeping_Items_Money[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end
				elseif Bookkeeping_Item_Category == 1 then
					while j<#Bookkeeping_Items_Energy do
					
						j = j + 1
					
						if Bookkeeping_Items_Energy[j].Value < 0 then
							love.graphics.print(Bookkeeping_Items_Energy[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Energy[j].Value*Bookkeeping_Items_Energy[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end				
				elseif Bookkeeping_Item_Category == 2 then						
					while j<#Bookkeeping_Items_Food do
					
						j = j + 1
					
						if Bookkeeping_Items_Food[j].Value < 0 then
							love.graphics.print(Bookkeeping_Items_Food[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Food[j].Value*Bookkeeping_Items_Food[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end					
				elseif Bookkeeping_Item_Category == 3 then
					while j<#Bookkeeping_Items_Water do
					
						j = j + 1
					
						if Bookkeeping_Items_Water[j].Value < 0 then
							love.graphics.print(Bookkeeping_Items_Water[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Water[j].Value*Bookkeeping_Items_Water[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end				
				elseif Bookkeeping_Item_Category == 4 then
					while j<#Bookkeeping_Items_Oxygen do
					
						j = j + 1
					
						if Bookkeeping_Items_Oxygen[j].Value < 0 then
							love.graphics.print(Bookkeeping_Items_Oxygen[j].Name, 224,215+((i-1)*40))	
							love.graphics.print(Bookkeeping_Items_Oxygen[j].Value*Bookkeeping_Items_Oxygen[j].Amount.."/day", 850,215+((i-1)*40))						
							i = i + 1
						end
					end	
				end
			end

			
		--monthly state
		elseif Bookkeeping_Menu_State == 2 then
			love.graphics.print("monthly financial state", 256,256)
		--amortization
		elseif Bookkeeping_Menu_State == 3 then
			love.graphics.print("amortization", 256,256)		
		end
		
	--labour command
	elseif Active_Menu == 4 then
		love.graphics.print("labour command", 256,256)			
	--contract list
	elseif Active_Menu == 5 then
		love.graphics.print("contract list", 256,256)
	--security panel
	elseif Active_Menu == 6 then
		love.graphics.print("security panel", 256,256)
	--Landing objects
	elseif Active_Menu == 7 then	
		love.graphics.print("Landing objects", 256,256)
	--RSS feed
	elseif Active_Menu == 8 then	
		love.graphics.print("RSS feed", 256,256)	
	--main menu
	elseif Active_Menu == 9 then	
		love.graphics.print("Main Menu", 256,256)	
	--going out
	elseif Active_Menu == 10 then	
		love.graphics.print("Going out", 256,256)			
	end
	
	Display_VariousTexts()
	
end

function Display_VariousTexts()
	
	if Active_Menu == 0 then
		if MouseX>815 and MouseX<1135 and MouseY>25 and MouseY<215 then
			love.graphics.print("Service Organizer",890,496)
		elseif MouseX>465 and MouseX<525 and MouseY>220 and MouseY<475 then
			love.graphics.print("Stock Monitoring",890,496)
		elseif MouseX>570 and MouseX<760 and MouseY>265 and MouseY<515 then
			love.graphics.print("Bookkeeping",890,496)
		elseif MouseX>805 and MouseX<900 and MouseY>260 and MouseY<440 then
			love.graphics.print("Labour Command",890,496)
		elseif MouseX>945 and MouseX<1130 and MouseY>220 and MouseY<475 then
			love.graphics.print("Contract List",890,496)
		elseif MouseX>290 and MouseX<415 and MouseY>220 and MouseY<430 then
			love.graphics.print("Security Panel",890,496)
		elseif MouseX>570 and MouseX<760 and MouseY>25 and MouseY<215 then
			love.graphics.print("Landing objects",890,496)
		elseif MouseX>140 and MouseX<230 and MouseY>220 and MouseY<470 then
			love.graphics.print("RSS feed",890,496)	
		elseif MouseX>310 and MouseX<565 and MouseY>105 and MouseY<175 then
			love.graphics.print("Main Menu",890,496)	
		elseif MouseX>144 and MouseX<271 and MouseY>96 and MouseY<159 then	
			love.graphics.print("Going out",890,496)			
		end
		

	elseif Active_Menu == 1 then

		--surface camera menuchanging buttons
		if MouseY>656 and MouseY<688 then
			if MouseX>172 and MouseX<425 then
				if Service_Organizer_State == 0 then love.graphics.print("Back to HUB",523,16) else love.graphics.print("Facility list",523,16) end
			elseif MouseX>511 and MouseX<767 then
				if Service_Organizer_State == 1 then love.graphics.print("Back to HUB",523,16) else love.graphics.print("Property map",523,16) end
			elseif MouseX>855 and MouseX<1110 then
				if Service_Organizer_State == 2 then love.graphics.print("Back to HUB",523,16) else love.graphics.print("Surface camera",523,16) end
			end
		end


	
	end

end

function Display_Time(x,y)
	
	local a,b,c = "1","2","3"
	
	if Hour < 10 then a = "0"..Hour else a = Hour end
	if Minute < 10 then b = "0"..Minute else b = Minute end
	if Second < 10 then c = "0"..math.floor(Second) else c = math.floor(Second) end
	
	love.graphics.print(a..":"..b..":"..c, x,y)

end

function Display_Date(x,y)

	local a,b = "1","2"
	
	if Day == 1 then a = Day.."st"
	elseif Day == 2 then a = Day.."nd"
	elseif Day == 3 then a = Day.."rd"		
	else a = Day.."th" end
	
	if Month == 1 then b = "January" 
	elseif Month == 2 then b = "February"
	elseif Month == 3 then b = "March"	
	elseif Month == 4 then b = "April"
	elseif Month == 5 then b = "May"	
	elseif Month == 6 then b = "June"
	elseif Month == 7 then b = "July"	
	elseif Month == 8 then b = "August"
	elseif Month == 9 then b = "September"
	elseif Month == 10 then b = "October"	
	elseif Month == 11 then b = "November"
	elseif Month == 12 then b = "December"	end
	
	love.graphics.print(a.." "..b..", "..Year, x,y)
end

function Display_Bookkeeping_Resources(name,variable, balance, x,y)

	local last = math.floor(variable/math.abs(balance/3600))
	
	love.graphics.print(name,x+72,y-32)
	love.graphics.print(math.floor(variable),x+72,y-56)
	
	if balance >= 0 then
		love.graphics.print("enough for infinite\n"
							.."time Since there's\n"
							.."no change in amount", x,y)	
	else
		love.graphics.print("enough for:\n"
							.." - "..math.floor(last).." seconds\n"
							.." - "..math.floor(last/60) .." minutes\n"
							.." - "..math.floor(last/3600) .." hours\n"
							.." - "..math.floor(last/86400) .." days", x,y)

	
	end
end



