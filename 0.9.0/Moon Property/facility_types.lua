--the long list of available facilities you can build

Facility_Types = {

{Name = "Moon in Live!",
Description = "online live streaming of the surface\n"..
				"of the Moon. For hardcore space and\n"..
				"science fanatics!",
				
Category = "Entertainment",

Input = {
	{ Name = "Moon in Live! system consumption", Resource = "Energy"}
}

Output =  {
	{ Name = "Moon in Live! subscriptions", Resource = "Money"}
	}
	
Constructing_Flatcost = {1,0,0,0,0},
},


{Name = "MoonCast Podcast Station",
Description = "online live streaming of the surface\n"..
				"of the Moon. For hardcore space and\n"..
				"science fanatics!",
				
Category = "Entertainment",

Input = {
	{ Name = "MoonCast cabin system consumption", Resource = "Energy"},
	{ Name = "MoonCast cabin life support", Resource = "Oxygen"},
}

Output =  {
	{ Name = "MoonCast Youtube incomes", Resource = "Money"},
	{ Name = "MoonCast Merchandising", Resource = "Money"}
	}
}

Constructing_Flatcost = {1,0,0,0,1},

	
}