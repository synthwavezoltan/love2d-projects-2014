--functions for key-related actions that should run on every frame if key is hold down
--this one is for screen scrolling and also simplified from Cybercraft counterpart
function KeyActions_Screen_Scrolling()

	--if either on map menu or surfcam menu
	if Active_Menu == 1 and Service_Organizer_State > 0 then
	
		if love.keyboard.isDown("w") and OffsetY > 0 then ScreenScrolling = "up"	end		
		if love.keyboard.isDown("s") and OffsetY < SizeY-ScreenY then ScreenScrolling = "down" end	
		if love.keyboard.isDown("a") and OffsetX > 0 then ScreenScrolling = "left"	end
		if love.keyboard.isDown("d") and OffsetX < SizeX-ScreenX then ScreenScrolling = "right"	end
		
	end

end