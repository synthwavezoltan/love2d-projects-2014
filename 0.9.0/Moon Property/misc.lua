--constant values' declaration
function Constants(dt)
	game_clock = game_clock+1
	FPS = math.floor(1/dt)
	
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	
	Item_Summarizer()
end

--the function that provides you scrolling on map or on surf-cam
--a modification of the one I used in Cybercraft
--in which "Is_Moving" bool and direction is merged into one variable
-- I love you Lua <3
function Screen_Scrolling()
		
		--if SrceenScrolling stores a direction
		if ScreenScrolling ~= "no" then
			--if the screen is NOT within a tile (=on "edge")
			if (math.abs(SmoothOffsetX) == TileSize or math.abs(SmoothOffsetY) == TileSize)	then		
							
				if ScreenScrolling == 'up' then 		--for every ScreenScrollings
					SmoothOffsetY = 0 					--change player location
					OffsetY = OffsetY - 1 				--jump a tile
				elseif ScreenScrolling == 'down' then 
					SmoothOffsetY = 0 
					OffsetY = OffsetY + 1
				elseif ScreenScrolling == 'left' then
					SmoothOffsetX = 0 
					OffsetX = OffsetX - 1
				elseif ScreenScrolling == 'right' then 
					SmoothOffsetX = 0 
					OffsetX = OffsetX + 1			
				end		
				
				ScreenScrolling = "no"		
				
			--if within a tile, then for every ScreenScrollings: scroll screen
			else										
				if ScreenScrolling == 'up' then SmoothOffsetY = SmoothOffsetY + 8						
				elseif ScreenScrolling == 'down' then SmoothOffsetY = SmoothOffsetY - 8	
				elseif ScreenScrolling == 'left' then SmoothOffsetX = SmoothOffsetX + 8		
				elseif ScreenScrolling == 'right' then SmoothOffsetX = SmoothOffsetX - 8 end		
			end	
		end
end