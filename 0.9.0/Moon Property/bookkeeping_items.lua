--the long list of provided balance modifiers
Bookkeeping_Items_Money = {
{Name = "A", Type = "o", Value = -1, Amount = 0 },
{Name = "B", Type = "o", Value = -1, Amount = 0 },
{Name = "C", Type = "o", Value = -1, Amount = 0 },

{Name = "Moon in Live! subscriptions", Type = "o", Value = 1, Amount = 0 },
{Name = "MANI", Type = "o", Value = 1, Amount = 0 }
}

Bookkeeping_Items_Energy = {
{Name = "Lvl1 living cabin consumption", Type = "cabin1", Value = -1, Amount = 1 },
{Name = "Lvl1 living cabin life support system", Type = "cabin1", Value = -1, Amount = 1 },
{Name = "Moon in Live! system consumption", Type = "cabin1", Value = -1, Amount = 1 },

{Name = "Lvl1 living cabin solar panels", Type = "cabin1", Value = 1, Amount = 1 }
}

Bookkeeping_Items_Food = {
{Name = "My own food consumption", Type = "basic", Value = -3, Amount = 1 },

{Name = "Greenhouse Garden output", Type = "placeholder", Value = 1, Amount = 0 }
}

Bookkeeping_Items_Water = {
{Name = "Lvl1 living cabin water supply", Type = "cabin1", Value = -7, Amount = 1 },

{Name = "Distillation Facility output", Type = "placeholder", Value = 1, Amount = 0 }
}

Bookkeeping_Items_Oxygen = {
{Name = "Lvl1 living cabin life support", Type = "cabin1", Value = -10, Amount = 1 },

{Name = "Greenhouse output", Type = "placeholder", Value = 1, Amount = 0 }
}