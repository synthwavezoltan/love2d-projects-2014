require 'mercenary_creator'
require 'mousebehav'
require 'tactics'
require 'key_actions'

function love.load()

	--uploading custom font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)		

	--variables
	--general
	i,j,Overall = 0,0,0
	--mouse
	MouseInIt = false							--is mouse on the playfield?
	MercOnTile = false							--is there any merc on the selected tile?
	ID_to_write = 0								--technical variable to return the merc ID of chosen tile	
	Selected_Merc = 0							--number of currently selected mercenary
	--FPS rate
	FPS = 0
	--sizes
	Max_col_length, Max_row_length = 64, 64		--total site
	ScreenSizeY, ScreenSizeX = 16, 16			--where you can see the main events: that is actually drawn	
	--camera
	GameField_OffsetX, GameField_OffsetY = 0, 0	--camera offsets	
	
	
	
	
	--loading picture quads from files
	--mercenary icons
	Merc_Col = love.graphics.newImage('MERC_COL.png')
	Merc_Ico = love.graphics.newImage('MERC_ICO.png')
	Iconset = { }
	
	for i=1, 23 do 
		Iconset[i] = love.graphics.newQuad((i-1)*32, 0, 32, 32, 736, 32)
	end
	
	--tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*32, (i-1)*32, 32, 32, 96, 96)
		end
	end
	
	--gameplay field
	Field_A = { }
	Field_B = { }	
	Field_C = { }	
	Field_D = { }
	Field_E = { }	
	
	for j=1,Max_row_length do
	
	Field_A[j] = { }
	Field_B[j] = { }
	Field_C[j] = { }
	Field_D[j] = { }
	Field_E[j] = { }	
	
		for i=1, Max_col_length do
			Field_A[j][i] = 1 	--uses TilesetPic
			Field_B[j][i] = 0 	--uses Merc_Col
			Field_C[j][i] = 0 	--uses Merc_Ico
			Field_D[j][i] = 0	--uses for tactics
			Field_E[j][i] = 0	--uses for nothing yet
			
		end
	end	
	
	
	--creating mercenary set at the very beginning
	Mercenary_Set = { }
	
	Generate_Merc_Set()
	
	for j = 1, #Validity do
		for i = 1, #Validity[j] do
			Overall = Overall+1
		end
	end
	
	Analyzer(1)
end

function love.draw()
	
	--playing field
	for j=1,ScreenSizeX do
		for i=1, ScreenSizeY do
		--Field_A, for Tileset[] textures
			love.graphics.draw(TilesetPic, Tileset[Field_A[j + GameField_OffsetX][i + GameField_OffsetY]], (j-1)*32, (i-1)*32)
			
		--Field_B + C for mercenary icons
			if Field_B[j + GameField_OffsetX][i + GameField_OffsetY] ~= 0 then
				love.graphics.draw(Merc_Col, Iconset[Field_B[j + GameField_OffsetX][i + GameField_OffsetY]], (j-1)*32, (i-1)*32)
			end
			
			if Field_C[j + GameField_OffsetX][i + GameField_OffsetY] ~= 0 then
				love.graphics.draw(Merc_Ico, Iconset[Field_C[j + GameField_OffsetX][i + GameField_OffsetY]], (j-1)*32, (i-1)*32)
			end	
			
		--Field_D, for tactics
			if Field_D[j + GameField_OffsetX][i + GameField_OffsetY] ~= 0 then
				love.graphics.print(Field_D[j + GameField_OffsetX][i + GameField_OffsetY],(j-1)*32, (i-1)*32)
			end
		end
	end

	--various texts
	--FPS 
	love.graphics.print("FPS: "..FPS,0,512)
	--offset
	love.graphics.print("Screen offset = "..GameField_OffsetX.." "..GameField_OffsetY,0,528)
	--mouse
	if MouseInIt then
		love.graphics.print("Mouse = "..MouseX..", "..MouseY,0,544)
		love.graphics.print("Chosen Tile Num = "..ChosenTile_X..", "..ChosenTile_Y,0,560)
	end	
	
	if MercOnTile then
		Selected_Merc = ID_to_write
		love.graphics.print(Mercenary_Set[ID_to_write].Alliance.."\n"..Mercenary_Set[ID_to_write].Type..
							"\n\nHP = "..Mercenary_Set[ID_to_write].HP..
							"\nArmor = "..Mercenary_Set[ID_to_write].Armor..
							"\nEnergy = "..Mercenary_Set[ID_to_write].Energy..
							"\nQuickness = "..Mercenary_Set[ID_to_write].Reflex..
							"\nHacking Speed = "..Mercenary_Set[ID_to_write].Hack_Spd..

							"\n\nDamage:"..
							"\n-melee = "..Mercenary_Set[ID_to_write].Dmg_Melee..
							"\n-ranged = "..Mercenary_Set[ID_to_write].Dmg_Ranged..
							"\n-passive = "..Mercenary_Set[ID_to_write].Dmg_Passive..
							"\n-tearing = "..Mercenary_Set[ID_to_write].Dmg_Energy..
							
							"\n\nRange = "..Mercenary_Set[ID_to_write].Range.." squares"..
							"\nAccuracy = "..Mercenary_Set[ID_to_write].Accuracy.."%"..
							
							"\n\nXP level = "..Mercenary_Set[ID_to_write].XP_Lvl..
							"\nAll XP pts = "..Mercenary_Set[ID_to_write].XP_Amount..
							
							"\n\nCoordinates = "..Mercenary_Set[ID_to_write].X_Pos..
							", "..Mercenary_Set[ID_to_write].Y_Pos..
							
							"\nRemaining steps = "..Mercenary_Set[ID_to_write].Stepnum_Rem..
							"/"..Mercenary_Set[ID_to_write].Stepnum_Base,512, 0)							

						
			
	else
		Selected_Merc = 0
		love.graphics.print("No mercenary selected.", 512, 0)
	end

	
end

function love.update(dt)

	FPS = math.floor(1/dt)
	MouseBehaviour()
	
	--ALWAYS give the icon colos
	for i=1,#Mercenary_Set do
		Field_B[Mercenary_Set[i].X_Pos][Mercenary_Set[i].Y_Pos] = Mercenary_Set[i].Icon_Col
		Field_C[Mercenary_Set[i].X_Pos][Mercenary_Set[i].Y_Pos] = Mercenary_Set[i].Icon_Ico	
	end

end

function love.keypressed(key)
	
end

--other functions
--generating a set of mercenaries
function Generate_Merc_Set()
		for i=1, 10 do
			Mercenary_Set[i] = Mercenary_Creator()
			
			--get an ID
			Mercenary_Set[i].ID = i
			
			--put mercs on random places
			while Field_B[Mercenary_Set[i].X_Pos][Mercenary_Set[i].Y_Pos] ~= 0 do
				Mercenary_Set[i].X_Pos = math.random(2, Max_row_length)
				Mercenary_Set[i].Y_Pos = math.random(2, Max_col_length)			
			end
			
		end
end

--calculating walkable areas
--[[
function NextStepEstimator(mercnum)

	local OpenList = { }
	local ClosedList = { }
	
	local G = 0
	
	--functions that returns false if a certain point is not on OpenList or ClosedList
	function IsItOpen(x,y)

		local check = false
	
		for i=1, #OpenList do
			if OpenList[i].X == x and OpenList[i].Y == y and #OpenList > 1 then
				check = true
				break
			else
				check = false
			end
		end
		
		return check
	end	
	
	function IsItClosed(x,y)
	
		local check = false
	
		for i=1, #ClosedList do
			if ClosedList[i].X == x and ClosedList[i].Y == y and #ClosedList > 1 then
				check = true
				break
			else
				check = false
			end
		end
		
		return check
	end
	
	--function that counts with a certain point
	function Dst_Calc(j,i, ParentX, ParentY)
		--estimation
		G = Field_D[ParentX][ParentY] + (j-ParentX)^2 + (i-ParentY)^2
		
		if Field_A[j][i] == 1 and Field_B[j][i] == 0	--the point is FULLY empty
		and (j>0 and i>0)								--both i and j are bigger than 0
		and IsItClosed(j,i) == false					--the point is not closed

		then
					
			--then give the distance value for Field_D (which based on dst formula)
			--always choose the shorter way
			if Field_D[j][i] == 0 then
				Field_D[j][i] = G
			end			
			
			if Field_D[j][i] ~= 0 and G < Field_D[j][i] then
				Field_D[j][i] = Field_D[ParentX][ParentY] + (j-ParentX)^2 + (i-ParentY)^2		
			end
					
			--then, add this point to OpenList 
			if IsItOpen(j, i) == false then
				table.insert(OpenList, { X = j, Y = i} )
			end
		end
	end
	
	--function that does the important steps for the neighbours of a point
	--FORGOTTON PROTOTYPE FUNCTION!
	function Dst_Calc_Ngbh(Parent_OpenID)
	
		--adding neighbours to the list and then put parent to closed list
		Dst_Calc(OpenList[Parent_OpenID].X-1,OpenList[Parent_OpenID].Y-1, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)
		Dst_Calc(OpenList[Parent_OpenID].X,OpenList[Parent_OpenID].Y-1, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)
		Dst_Calc(OpenList[Parent_OpenID].X+1,OpenList[Parent_OpenID].Y-1, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)
		
		Dst_Calc(OpenList[Parent_OpenID].X-1,OpenList[Parent_OpenID].Y, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)
		Dst_Calc(OpenList[Parent_OpenID].X+1,OpenList[Parent_OpenID].Y, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)
		
		Dst_Calc(OpenList[Parent_OpenID].X-1,OpenList[Parent_OpenID].Y+1, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)
		Dst_Calc(OpenList[Parent_OpenID].X,OpenList[Parent_OpenID].Y+1, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)	
		Dst_Calc(OpenList[Parent_OpenID].X+1,OpenList[Parent_OpenID].Y+1, OpenList[Parent_OpenID].X, OpenList[Parent_OpenID].Y)	

		--switch parent into closed list 
		table.insert(ClosedList, { X = OpenList[Parent_OpenID].X, Y = OpenList[Parent_OpenID].Y} )
		table.remove(OpenList, Parent_OpenID)		
	end
	
	
	--adding starting point to open list
	OpenList[1] = { X = Mercenary_Set[mercnum].X_Pos, Y = Mercenary_Set[mercnum].Y_Pos}	
	Dst_Calc_Ngbh(1)
	
	--searching the smallest G in OpenSet
	local smlst, smlst_B, smlst_ID = 0,0,1
	
	while smlst < 10 do
		smlst = Field_D[OpenList[1].X][OpenList[1].Y]
		
		for k=1, #OpenList-1 do
			smlst_B	= Field_D[OpenList[k+1].X][OpenList[k+1].Y]
			
			if smlst_B < smlst then
				smlst = smlst_B
				smlst_ID = k
			end
		end	
		Dst_Calc_Ngbh(smlst_ID)
	end
end
]]


