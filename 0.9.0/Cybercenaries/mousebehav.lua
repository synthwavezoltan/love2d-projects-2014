function MouseBehaviour()

	MouseX = love.mouse.getX()
	MouseY = love.mouse.getY()
	
	ChosenTile_X = math.floor(MouseX/32) + GameField_OffsetX + 1
	ChosenTile_Y = math.floor(MouseY/32) + GameField_OffsetY + 1
	
	-- if cursor is within GameField
	if MouseX < 32*16 and MouseY < 32*16
	then
		MouseInIt = true												--write out values declared above
		ChosenTile_A_Num = Field_A[ChosenTile_X][ChosenTile_Y]		--calculate the A-num of the tile I'm on
	else 
		MouseInIt = false
	end
	
	--click events
	function love.mousepressed( x, y, button )	
	
		if button == "l" then
			--if mouse is on playfield
			if MouseInIt then
				--if there aren't any selected merc unit
				if Selected_Merc == 0 then
					--return with ID_to_write; for details, check the function def. below
					GetMercFromTile()
				--if there's a selected unit
				else
					-- move the merc away BUT ONLY IF THE FIELD IS FREE!
					if Field_B[ChosenTile_X][ChosenTile_Y] == 0 then
					
						--deleting previous colors
						Field_B[Mercenary_Set[ID_to_write].X_Pos][Mercenary_Set[ID_to_write].Y_Pos] = 0
						Field_C[Mercenary_Set[ID_to_write].X_Pos][Mercenary_Set[ID_to_write].Y_Pos] = 0
						
						--giving new coordinates
						Mercenary_Set[ID_to_write].X_Pos = ChosenTile_X
						Mercenary_Set[ID_to_write].Y_Pos = ChosenTile_Y
						
						--giving colors
						Field_B[Mercenary_Set[ID_to_write].X_Pos][Mercenary_Set[ID_to_write].Y_Pos] = Mercenary_Set[ID_to_write].Icon_Col
						Field_C[Mercenary_Set[ID_to_write].X_Pos][Mercenary_Set[ID_to_write].Y_Pos] = Mercenary_Set[ID_to_write].Icon_Ico
					--if the field isn't free, then select the merc that stays on that
					else
						GetMercFromTile()
					end
				end
			end
		end
		
		if button == "r" then
			--if there IS a selected merc then deselect it
			if Selected_Merc ~= 0 then
				MercOnTile = false
				Selected_Merc = 0
			end
		end
	end
end

--gives to "ID_to_write" variable the ID of the merc that stays on the specific tile
function GetMercFromTile()

	for i=1, #Mercenary_Set do
		if Mercenary_Set[i].X_Pos == ChosenTile_X and Mercenary_Set[i].Y_Pos == ChosenTile_Y then
			ID_to_write = Mercenary_Set[i].ID
			MercOnTile = true
			break
		else
			MercOnTile = false
		end
	end
					
end