--try to analyze situations
function Analyzer(Unit_num)
	--risk calculator
	for k=1, #Mercenary_Set do
		for j=1,Max_row_length do
			for i=1, Max_col_length do
				if (j-Mercenary_Set[k].X_Pos)^2 + (i-Mercenary_Set[k].Y_Pos)^2 < (Mercenary_Set[k].Range)^2 then
					Field_D[j][i] = Field_D[j][i] + 1
				end
			end
		end
	end
end

--general behaviour of mercenaries
function Mercenary_Behaviour()

end

--other functions

--screenscrolling
function ScrollUp(dst)
	if GameField_OffsetY > 1				--offset should be bigger than 0
	then
		GameField_OffsetY = GameField_OffsetY - dst
	end
end

function ScrollDown(dst)
	if GameField_OffsetY < Max_col_length - ScreenSizeY 	--offset should be smaller than the full size minus screen size
	then
		GameField_OffsetY = GameField_OffsetY + dst
	end
end

function ScrollLeft(dst)
	if GameField_OffsetX > 1				--offset should be bigger than 0
	then
		GameField_OffsetX = GameField_OffsetX - dst
	end
end

function ScrollRight(dst)
	if GameField_OffsetX < Max_row_length - ScreenSizeX 	--offset should be smaller than the full size minus screen size
	then
		GameField_OffsetX = GameField_OffsetX + dst
	end
end