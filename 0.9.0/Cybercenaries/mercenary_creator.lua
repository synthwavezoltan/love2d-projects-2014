Types = { "Civilian",
"Soldier",
"Suicide Bomber",
"Sniper",
"Medic",
"Healer",
"Protector",
"Assassin",
"Specop Agent",
"Trooper",
"Hunter",
"Hacker",
"Stormtrooper",
"Infantry",
"Officer",
"Spy",
"Fighter",
"Scout",
"Conqueror",
"Shield",
"Exterminator",
"Guard",
"Marksman",
}

Alliances = {
"United Forces", 	
"Streetmaul",		
"Sect",				
"Red Fist",			
"Justice Empire",	
"Shadow Army",		
"Cyborg",			
"Khali",			
"Goldrusher",		
"Jade Legion",	
"Alien",			
"Moebius",			
}

Validity = { }
Validity[1] = { 1, 2, 4, 5, 9, 10, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23}
Validity[2] = { 1, 2, 3, 5, 8, 12, 16, 17, 18, 22, 23}
Validity[3] = { 1, 2, 3, 6, 7, 8, 11, 15, 16, 17, 18, 19, 20, 21, 22, 23}
Validity[4] = { 1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 23}
Validity[5] = { 1, 2, 4, 5, 9, 10, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23}
Validity[6] = { 2, 4, 6, 7, 8, 9, 10, 12, 13, 15, 16, 20, 21, 22, 23}
Validity[7] = { 2, 7, 10, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22}
Validity[8] = { 1, 2, 3, 6, 7, 8, 10, 11, 14, 15, 16, 17, 18, 19, 21, 22, 23}
Validity[9] = { 1, 2, 5, 7, 10, 11, 12, 14, 16, 17, 18, 22, 23}
Validity[10] = { 1, 2, 4, 6, 7,8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22}
Validity[11] = { 2, 7, 8, 9, 11, 12, 13, 14, 16, 18, 19, 20, 21, 22}
Validity[12] = { 2, 4, 6, 7, 8, 9, 11, 13, 14, 16, 17, 18, 19, 20, 21}


function Mercenary_Creator()

Mercenary = { }

local all = math.random(#Alliances)
local typ = math.random(#Validity[all])

Mercenary = 
{
ID = 0,

Alliance = Alliances[all],
Type = Types[Validity[all][typ]],
HP = 100 + math.random(0, 99),
Armor = 0 + typ,
Reflex = 100,
Energy = 100,
Range = math.random(2, 10),
Accuracy = math.random(50, 100),
Dmg_Melee = 1,
Dmg_Ranged = 5,
Dmg_Energy = 0,
Dmg_Passive = 0,
XP_Amount = 0,
XP_Lvl = 1,
Hack_Spd = 10,

Icon_Col = all,
Icon_Ico = Validity[all][typ],

X_Pos = math.random(2, Max_row_length),
Y_Pos = math.random(2, Max_col_length),	

Stepnum_Base = 10,
Stepnum_Rem = 10		
}

return Mercenary

end