SolidTiles = 
{2, 3}

function SolidChecker(x,y)

	local isSolid = false
	
	--gate 1: it's on SolidTiles list
	for i=1, #SolidTiles do
		if Field[x][y] == SolidTiles[i] then	
			isSolid = true
			break
		else
			isSolid = false
		end
	end
		

	
	return isSolid
end

function Gravity()

--we have to check which tile can be found below playerx
--and which below playerx+sizex
--I don't plan to make player wider than "tilesize"
--therefore there's enough to check the two endpoints
	local newy  = 1+math.floor((Player.Y + Player.sizeY)/TileSize)
	local newxa = 1+math.floor((Player.X			   )/TileSize)
	local newxb = 1+math.floor((Player.X + Player.sizeX)/TileSize)

	if not SolidChecker(newxa, newy) and not SolidChecker(newxb, newy)
	then 
		--stop at last step
		--first check if it's the last step
		--then if it is then act as if player was on ground
		local tempy  = 1+math.floor((Player.Y + gravity_store + Player.sizeY)/TileSize)
		
		if not (not SolidChecker(newxa, newy) and not SolidChecker(newxb, newy)) then
			Player.Y = Player.Y - (Player.Y%TileSize)
			Player.IsOnGround = true 
			gravity_store = 0		
		else	
			gravity_store = gravity_store + gforce
			Player.Y = Player.Y + gravity_store
			Player.IsOnGround = false
		end
	else 
		Player.Y = Player.Y - (Player.Y%TileSize)
		Player.IsOnGround = true 
		gravity_store = 0
	end

end

function Jump()
	
	if height > 0 then 
		--[[
		local above =   math.floor(Player.Y					/TileSize) - 4
		local newxa = 1+math.floor((Player.X			   )/TileSize)
		local newxb = 1+math.floor((Player.X + Player.sizeX)/TileSize)
		
		if not SolidChecker(newxa, above) and not SolidChecker(newxb, above) then]]
		Player.Y = Player.Y - height 	
		--end
		
		
		height = height - gforce 		

	end

end
