require 'input'
require 'physics'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	SizeX, SizeY = 40, 20

	OffsetBorder = 8
	Scrolling_Value = 1
	
	init = (720 - (SizeY*TileSize))/2
	shakex, shakey = 0, 0
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	
	height = 0 				--storing at vectors
	gravity_store = 0
	move_store = 0	
	
	gforce = 0.2			--y vector used for gravity -> acceleration indicator
	moveforce = 2			--x vector for walking
	
	
	
	--creating tileset
	TilesetPics = {}
	Tileset = {}
	Load_Tilesets(Tileset, 'TILESET.png', 32)
	
	--creating field
	Field = { }
	
	for i=1,SizeX do
	
		Field[i] = { }
	
		for j=1, SizeY do
			Field[i][j] = 1

			if i==1 or i==SizeX
			or j==1 or j==SizeY then
				Field[i][j] = 2
			end
		end
		
	end
	
	for i=1, 100 do Field[math.random(2,SizeX)][math.random(2,SizeY)] = 2 end

	
	--creating player
	Player = {
		X = 64, Y = 64, TileX = 2, TileY = 2,
		OffsetX = 0, OffsetY = 0,
		sizeX = TileSize, sizeY = TileSize,
		Mov_Dir = "no", 
		IsMoving = false, IsOnGround = false,
	}
	
end

function love.draw()
	
	--field
	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPics[1], Tileset[Field[j][i]], shakex+(j-1)*TileSize, shakey+init+(i-1)*TileSize)
		end
	end
	
	love.graphics.print(Player.Y%TileSize, 0,0)
	
	--player
	--love.graphics.draw(TilesetPics[1], Tileset[4], shakex+(Player.TileX-1)*TileSize, shakey+init+(Player.TileY-1)*TileSize)
	love.graphics.draw(TilesetPics[1], Tileset[9], shakex+Player.X, shakey+init+Player.Y)
	
		local above =   math.floor(Player.Y					/TileSize)
		local newxa = 1+math.floor((Player.X			   )/TileSize)
		local newxb = 1+math.floor((Player.X + Player.sizeX)/TileSize)


love.graphics.draw(TilesetPics[1], Tileset[4], shakex+(newxa-1)*TileSize, shakey+init+(above-1)*TileSize)
love.graphics.draw(TilesetPics[1], Tileset[4], shakex+(newxb-1)*TileSize, shakey+init+(above-1)*TileSize)
end

function love.update(dt)
	
	Player.TileX = 1+math.floor((Player.X+(TileSize/2))/TileSize)
	Player.TileY = 1+math.floor((Player.Y+(TileSize/2))/TileSize)	
	
	Moving()
	Gravity()
	Jump()

	--[[
	shakex = math.random(-32, 32)
	shakey = math.random(-32, 32)
	]]
end


function SmoothMoving_Player (direction, speed)
	if math.abs(Player.OffsetX) == TileSize 
	or math.abs(Player.OffsetY) == TileSize then 	--if the player scrolled a tile
		Player.IsMoving = false						--stop moving 
		--and then for every directions: change player location
		if direction == 'up' then 		 Player.TileY = Player.TileY - 1 
		elseif direction == 'down' then  Player.TileY = Player.TileY + 1
		elseif direction == 'left' then  Player.TileX = Player.TileX - 1
		elseif direction == 'right' then Player.TileX = Player.TileX + 1 end		
		
		if direction == 'up' or direction == 'down'    then Player.OffsetY = 0 end
		if direction == 'left' or direction == 'right' then Player.OffsetX = 0 end
	else
		--if player is within a tile, then for every directions: scroll player
		if     direction == 'up' 	then Player.OffsetY = Player.OffsetY - speed 				
		elseif direction == 'down'  then Player.OffsetY = Player.OffsetY + speed  
		elseif direction == 'left'  then Player.OffsetX = Player.OffsetX - speed 			
		elseif direction == 'right' then Player.OffsetX = Player.OffsetX + speed 
		end		
	end	
end


--a table with the name of "SetName" must be declared before its call
function Load_Tilesets(SetName, ImageName, UnitSize)
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local CharX = TilesetPics[1]:getWidth()
	local CharY = TilesetPics[1]:getHeight()
	
	local TilesX = CharX/TileSize
	local TilesY = CharY/TileSize

	for j=1, TilesY do
		for i=1, TilesX do			
			SetName[((j-1)*TilesX) + i] = love.graphics.newQuad((i-1)*UnitSize, (j-1)*UnitSize, UnitSize, UnitSize, CharX, CharY)
		end
	end	
end





function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end


--count mode returns the amount of "value" in "Table"
--indicate mode returns true if "value" is found in "Table"
--list mode returns the coordinates of "value" positions in "Table"
function Colors(Table, valueToCheck, mode)

	local asd = 0
	local list = {}
	local indicator = true
	
	for j=1, SizeX do
		for i=1, SizeY do
			if mode == "count" then
				if Table[j][i] == valueToCheck then asd=asd+1 end
			elseif mode == "list" then
				if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
			elseif mode == "indicate" then
				indicator = indicator or Table[j][i] == valueToCheck
			end
		end
	end
	
	return asd
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




