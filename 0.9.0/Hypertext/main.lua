require 'textgenerator'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 32, 24
	
	Year, Month, Day = math.random(2015, 2090), math.random(1,12), math.random(1,28)
	Hours,Minutes,Seconds = math.random(0,23),0,0
	
	Hometown = CitySet[math.random(#CitySet)]
	
	Followers = 0
	
	MainMenu = true
	
	Advertisement_Number = #Advertisements	
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	game_clock = 0	
	
	--initializing font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)	
	
	--tileset
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = math.random(1,3)		
		end
		
	end
	
end

function love.draw()

	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPic, Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
		end
	end
	
	love.graphics.print(Hometown..", "..Hours..":"..Minutes..":"..Seconds.." - date is "..Day.."."..Month.."."..Year,0,0)
	love.graphics.print("Advertisements - join Hypertext Legion to remove ads!"..game_clock,0, 550)
	
	love.graphics.print(Advertisements[Advertisement_Number],0,600)
	
	if MainMenu then
		love.graphics.print(MainMenuText,0,70) 
	end
end

function love.update(dt)
	game_clock = game_clock + 1
	
	if game_clock%200 == 0 then
		for j=1,SizeX do
			for i=1, SizeY do
				Field[j][i] = math.random(1,3)
			
			end
		end	
	end
	
	if game_clock%1000 == 0 then Advertisement_Number = math.random(#Advertisements) end
	if game_clock%60 == 0 then Seconds = Seconds + 1 end	
	if Seconds == 60 then Minutes = Minutes + 1 Seconds = 0 end	
end





