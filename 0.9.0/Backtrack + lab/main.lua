function love.load()
	math.randomseed( os.time() )
	x,y,z,i,j,k = 0,0,0,0,0,0
	TileX, TileY = 0,0
	SizeX, SizeY = 128, 96
	counter = 0
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	cl=0
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*16, (i-1)*16, 16, 16, 48, 48)
		end
	end	
	
	--uploading field
	Field = { }
	Numbers = { }
	HelpTable = { }
	
	for j=1,SizeX do
	
	Field[j] = { }
	Numbers[j] = { }
	HelpTable[j] = { }	
	
		for i=1, SizeY do
			Field[j][i] = 1
			Numbers[j][i] = 0
			HelpTable[j][i] = 0				
		end
	end
	
	--creating the maze
	Numbers[50][50] = 9 
	Answer()
	while CountColors(Numbers, 2) ~= 0 do Answer()	end
	
	--backtrack: start,finish,selector
	StartX, StartY = 1,1
	FinishX,FinishY = 1,1
	SelectedX, SelectedY = 1,1

	while Numbers[StartX][StartY] ~= 9 do StartX, StartY = math.random(1,127), math.random(1,95) end
	while Numbers[FinishX][FinishY] ~= 9 do FinishX, FinishY = math.random(1,127), math.random(1,95) end

	SelectedX, SelectedY = StartX, StartY
	
	Numbers[StartX][StartY] = 4
	Numbers[FinishX][FinishY] = 6	
	
	labsize = CountColors(Numbers, 9)
end

function love.draw()

	for j=1,SizeX do
		for i=1, SizeY do
			if Numbers[j][i] ~= 0 then 
			love.graphics.draw(TilesetPic, Tileset[Numbers[j][i]], (j-1)*8, (i-1)*8)
			--love.graphics.print(Numbers[j][i],(j-1)*16, (i-1)*16)
			end			
		end
	end
	
	love.graphics.print(math.ceil(CountColors(Numbers, 5)/(labsize/100))..
						" % is explored\n"..labsize..
						"\n"..cl,0,0)

end

function love.update(dt)


	
	if CountNeighbours(FinishX, FinishY, 4, Numbers) == 0 	
	then Iterator(9,4) 
	cl = cl+ 1 end

end

--[[
----------------------------------------------------------------------------------------
				THE LABYRINTH GENERATOR AND NECESSARY FUNCTIONS
				(made in April 2014 by Katamori, unoptimized yet)
----------------------------------------------------------------------------------------
]]


function Answer()
	
	BackLoad(HelpTable, Numbers)	
	
	for j=2,SizeX-1 do
		for i=2, SizeY-1 do
		
			if Numbers[j][i] ~= 9 and Numbers[j-1][i] == 9 then HelpTable[j][i] = 1 end
			if Numbers[j][i] ~= 9 and Numbers[j+1][i] == 9 then HelpTable[j][i] = 1 end
			if Numbers[j][i] ~= 9 and Numbers[j][i-1] == 9 then HelpTable[j][i] = 1 end
			if Numbers[j][i] ~= 9 and Numbers[j][i+1] == 9 then HelpTable[j][i] = 1 end			
		end
	end		
	
	for j=2,SizeX-1 do
		for i=2, SizeY-1 do
		
			if Numbers[j][i] ~= 9 
			and CountNeighbours(j,i, 9, Numbers) == 1
			and FullNeighbours(j,i, 9, Numbers) < 3			
			and HelpTable[j][i] == 1 
			then HelpTable[j][i] = 2 end
			
		end
	end		
	
	j,i=10,10
	
	if #ListColors(HelpTable, 2) ~= 0 then
		lista = ListColors(HelpTable, 2)
		
		rand = math.random(#ListColors(HelpTable, 2))
		HelpTable[lista[rand][1]][lista[rand][2]] = 9
	end	
		
	BackLoad(Numbers, HelpTable)
end

function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,SizeX-1 do
		for i=2, SizeY-1 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,SizeX-1 do
		for i=2, SizeY-1 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,SizeX do
		for i=1, SizeY do
			a[j][i] = b[j][i]
		end
	end

end

--[[
----------------------------------------------------------------------------------------
				THE BACKTRACKING PROGRAM TO FIND EXIT
				(made in April 2014 by Katamori, unoptimized yet)
----------------------------------------------------------------------------------------
]]

--DA FUNCTION
--1: you CAN visit (here: 9)
--2: you CAN NOT visit (here: 1)
--3: you ALREADY VISITED (here: 4)
--4: WRONG WAY (here: 5)

function Iterator(condition, writeit)

	local wrong_neighbours = 0
	

	--up
	if SelectedY-1 > 0 and Numbers[SelectedX][SelectedY-1] == condition then
		Numbers[SelectedX][SelectedY] = writeit
		SelectedY = SelectedY-1
	else
		wrong_neighbours = wrong_neighbours + 1
		--right
		if SelectedX+1 <= SizeX and Numbers[SelectedX+1][SelectedY] == condition then
			Numbers[SelectedX][SelectedY] = writeit
			SelectedX = SelectedX+1
		else
			wrong_neighbours = wrong_neighbours + 1
			--down
			if SelectedY+1 <= SizeY and Numbers[SelectedX][SelectedY+1] == condition then
				Numbers[SelectedX][SelectedY] = writeit
				SelectedY = SelectedY+1
			else
				wrong_neighbours = wrong_neighbours + 1	
					--left
					if SelectedX-1 > 0 and Numbers[SelectedX-1][SelectedY] == condition then
						Numbers[SelectedX][SelectedY] = writeit
						SelectedX = SelectedX-1
					else
						wrong_neighbours = wrong_neighbours + 1
				end
			end
		end		
	end
	


	

	
	if wrong_neighbours == 4 then 
		--check if finishpoint is in the neighbour
		if CountNeighbours(SelectedX, SelectedY, 6, Numbers) == 0 
		then Iterator(writeit,5) else end
	end

end


