function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 32, 24
	MouseX, MouseY = love.mouse.getX(), love.mouse.getY()
	SpecX, SpecY = 0,0
	VectorX, VectorY = 0,0
	Gravity = 0 
	GForce = 0.1 --a vector with X:0 and some positive Y value
	Wind = 0
	WForce = 0.02 --a vector with Y:0 and some positive X value
	Launchspeed = 10 --multiplier to moving calculator
	Trajectory = { } --a table for a lot of dots

	x,y,z,i,j,k = 0,0,0,0,0,0
	degree = 0
	radian = 0
	click = false	

	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	Player = {
	X = 512,
	Y = 700,
	}
	
end

function love.draw()

	love.graphics.draw(TilesetPic, Tileset[5], Player.X, Player.Y, math.rad(degree+90), 1, 1, 16, 16)
	--love.graphics.line(MouseX, MouseY, Player.X, Player.Y)
	love.graphics.print("Angle in radians: "..radian..
						"\nAngle in degrees: "..degree..
						"\nAngle sine: "..math.sin(radian)..
						"\nAngle cosine: "..math.cos(radian),MouseX,MouseY+20)
						
	if click then love.graphics.print(SpecX..","..SpecY.."\nclick = true",0,0) 
	else love.graphics.print(SpecX..","..SpecY.."\nclick = false",0,0) end
	
	if #Trajectory >= 4 and not click then love.graphics.line(Trajectory) end
	
	--for i=1,#Trajectory do love.graphics.print(Trajectory[i],64,i*16) end
end

function love.update(dt)
	MouseX = love.mouse.getX()
	MouseY = love.mouse.getY()
	
	
	
	--I need cos/sin: in front of/neighbour
	--if there's no aim yet (~SpecX/Y are both 0), then look to the dir of mouse	
	if not click then
		VectorX = MouseX-Player.X
		VectorY = MouseY-Player.Y
	else 
		VectorX = SpecX-Player.X
		VectorY = SpecY-Player.Y	
	end

	--rollback degree counter
	if degree>=360 then degree = degree-360 end	
	if degree<=0 then degree = degree+360 end
	
	--counting moving
	if click then
		Gravity = Gravity + GForce
		Wind = Wind + WForce
		
		--MATH.COS AND MATH.SIN NEED THE ANGLE IN FUCKIN RADIAN!!!!!!!!
		Player.X = Player.X + math.cos(radian)*Launchspeed-Wind
		Player.Y = Player.Y + math.sin(radian)*Launchspeed+Gravity
		
	else
		--count degrees (only when click is false)
		degree = math.deg(math.atan2(VectorY,VectorX)) 
		radian = math.rad(degree)
		--calculating trajectory
		k = Player.X	--Xpos
		l = Player.Y	--Ypos
		m = 0			--Gravity
		n = 0			--Wind
		for i=0, 200 do
			Trajectory[i*2-1] = k
			Trajectory[i*2] = l
			m = m+GForce
			n = n+WForce
			k = k+math.cos(radian)*Launchspeed-n
			l = l+math.sin(radian)*Launchspeed+m
		end		
	end
	
	--rollback if falls out of visible field
	if Player.Y<0 or Player.Y>700 or Player.X<0 or Player.X>1024 then
		click = false
		Gravity = 0
		Player.X = 512
		Player.Y = 700		
	end
	

end

function love.mousepressed(x,y,button)

	if button == 'l' then
		if not click then
			click = true
			SpecX = MouseX
			SpecY = MouseY
			Gravity = 0	
			Wind = 0			
		end

	end
end




