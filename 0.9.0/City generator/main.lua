function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 64, 48
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0

	dir = 1 						--1 means right, 2 means left, 3 mean down, 4 means up
	LotID, StreetID = 1,2 			--their number in Tileset[]
	IterationNumber = 10
	
	StartPoint = {10,10}
	
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = LotID		
		end
		
	end
	
	--put down a random street tile
	Field[math.random(1,SizeX)][math.random(1,SizeY)] = StreetID
	
	--generate roads
	i=0
	
	while i<IterationNumber do
	
		StartPoint = DefineStartPoint(Field)
		dir = SelectDirection(Field, StartPoint[1],StartPoint[2])
		CreateStreet(Field, StartPoint[1], StartPoint[2], (i%4)+1)
		
		i=i+1
	end
	
	
end

function love.draw()
	--drawing out field
	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPic, Tileset[Field[j][i]], (j-1)*TileSize/2, (i-1)*TileSize/2)
		end
	end
	
	love.graphics.print(StartPoint[1]..", "..StartPoint[2]..", "..dir,0,0)
	
end

function love.update(dt)

end



--create exactly one street
function CreateStreet(ArrA, StreetStartpointX, StreetStartpointY, direction)

	local SelectedTile = {}
	
	SelectedTile[1] = StreetStartpointX
	SelectedTile[2] = StreetStartpointY
	
	while (0<SelectedTile[1] and SelectedTile[1]<=SizeX and 0<SelectedTile[2] and SelectedTile[2]<=SizeY) do

		if ArrA[SelectedTile[1]][SelectedTile[2]] == StreetID then break end
	
		ArrA[SelectedTile[1]][SelectedTile[2]] = StreetID
	
		if direction == 1 then SelectedTile[1] = SelectedTile[1] + 1 end	
		if direction == 2 then SelectedTile[1] = SelectedTile[1] - 1 end		
		if direction == 3 then SelectedTile[2] = SelectedTile[2] + 1 end	
		if direction == 4 then SelectedTile[2] = SelectedTile[2] - 1 end		
	end
	
	return ArrA;
end



--choose an optimal point to start
function DefineStartPoint(ArrB)

	local PreferredPoints = {}

	for i=1, SizeX do
		for j=1, SizeY do

			if ArrB[i][j] == LotID then
				if NeighbourCheck(ArrB, i, j, StreetID) then
					if SelectDirection(ArrB, i, j) ~= 0 then
						table.insert(PreferredPoints, {i,j})
					end
				end
			end
		
		end
	end
	
	if #PreferredPoints<3
	then 
	StartPoint = {10,10}
	return StartPoint
	else
	local rand = math.random(#PreferredPoints)
	return PreferredPoints[rand] 
	end
end



--choose an optimal direction for the road
function SelectDirection(ArrC, checkpointX,checkpointY)

	local Dirset = { }
		
	if checkpointX-1>0 
	and ArrC[checkpointX-1][checkpointY] == LotID 
	and StrictNeighbourCheck(ArrC,checkpointX-1,checkpointY,LotID) 
	then table.insert(Dirset,1)
	elseif checkpointX+1<=SizeX 
	and ArrC[checkpointX+1][checkpointY] == LotID 
	and StrictNeighbourCheck(ArrC,checkpointX+1,checkpointY,LotID) 
	then table.insert(Dirset,2)
	elseif checkpointY-1>0 
	and ArrC[checkpointX][checkpointY-1] == LotID 
	and StrictNeighbourCheck(ArrC,checkpointX,checkpointY-1,LotID) 
	then table.insert(Dirset,3)
	elseif checkpointY+1<=SizeY 
	and ArrC[checkpointX][checkpointY+1] == LotID 
	and StrictNeighbourCheck(ArrC,checkpointX,checkpointY+1,LotID) 
	then table.insert(Dirset,4)
	end
	
	if #Dirset ~= 0 then return Dirset[math.random(#Dirset)] else return 0 end

end

--checking neighbours
function NeighbourCheck(ArrD, X, Y, IDtoCheck)

	--if the tile is out of boundaries, don't even check -> you won't find anything there!
	if (X-1>0 and ArrD[X-1][Y] == IDtoCheck)
	or (X+1<=SizeX and ArrD[X+1][Y] == IDtoCheck)
	or (Y-1>0 and ArrD[X][Y-1] == IDtoCheck)
	or (Y+1<=SizeY and ArrD[X][Y+1] == IDtoCheck)
	
	then return true
	else return false
	end

end

--checking neighbours (STRICT)
function StrictNeighbourCheck(ArrD, X, Y, IDtoCheck)

	--if the tile is out of boundaries, don't even check -> you won't find anything there!
	if (X-1>0 and ArrD[X-1][Y] == IDtoCheck)
	and (X+1<=SizeX and ArrD[X+1][Y] == IDtoCheck)
	and (Y-1>0 and ArrD[X][Y-1] == IDtoCheck)
	and (Y+1<=SizeY and ArrD[X][Y+1] == IDtoCheck)
	
	then return true
	else return false
	end

end

--[[
PSZEUDOKÓD:

LOVE.LOAD:
int i,j;
int HázID, ÚtID;
int Kezdőpont[], Irány; //1=fel, 2=le, 3=jbalra, 4=jobbra
int IterációkSzáma;
int Játéktér[][];

kép Csempekészlet[];

Csempekészlet feltöltése; 					(nem részletezem, evidens)
Játéktér feltöltése; 						(nem részletezem, evidens)

i=random X koordináta;
j=random Y koordináta;
játéktér[i][j] = ÚtID;

for i<IterációkSzáma

	Kezdőpont[] = KezdőpontDefiniálás(Játéktér);
	Irány = Irányválasztás(Játéktér,Kezdőpont);
	ÚtkészítőFüggvény(Játéktér, Kezdőpont, Irány); 

end



LOVE.DRAW:
Játéktér kirajzolása, 						(nem részletezem, evidens)



KEZDŐPONTDEFINIÁLÁS:
function KezdőpontDefiniálás(int Tömb[][])

	local MegfelelőPontok = {}
	local Kezdőpont[2]

	for i<szélesség
		for j<magasság
		
			if Tömb[i][j] == HázID then
				if Szomszédvizsgálat(Tömb, Tömb[i], Tömb[j], ÚtID) then
					if then
						table.insert(MegfelelőPontok, {i,j})
					end
				end
			end
		
		end
	end
	
	local random = *random szám MegfelelőPontok méretén belül*
	
	Kezdőpont[0] = MegfelelőPontok[random][0];
	Kezdőpont[1] = MegfelelőPontok[random][1];
	
	return Kezdőpont;
end



IRÁNYVÁLASZTÁS:
function Irányválasztás(int Tömb[][], beX,beY)
		
	if Tömb[beX-1][beY] == HázID and beX-1>=0 and Szomszédvizsgálat(Tömb,beX-1,beY,HázID) then return 1
	if Tömb[beX+1][beY] == HázID and beX+1<=szélesség and Szomszédvizsgálat(Tömb,beX+1,beY,HázID) then return 2	
	if Tömb[beX][beY-1] == HázID and beY-1>=0 and Szomszédvizsgálat(Tömb,beX,beY-1,HázID) then return 3
	if Tömb[beX][beY+1] == HázID and beY+1<=magasság and Szomszédvizsgálat(Tömb,beX,beY+1,HázID) then return 4		

end



SZOMSZÉDVIZSGÁLAT:
function Szomszédvizsgálat(int Tömb[][], X, Y, int KeresettID)

	if Tömb[X-1][Y] == KeresettID
	or Tömb[X+1][Y] == KeresettID
	or Tömb[X][Y-1] == KeresettID
	or Tömb[X][Y+1] == KeresettID
	
	then return true
	else return false

end



ÚTKÉSZÍTŐFÜGGVÉNY:
function ÚtkészítőFüggvény(int Tömb[][], int Kezdőpont[], int Irány)

	local KijelöltMező[] = Kezdőpont[]
	
	while Tömb[KijelöltMező[0] ][KijelöltMező[1] ] ~= ÚtID do
	
		Tömb[KijelöltMező[0] ][KijelöltMező[1] ] = ÚtID
		
		if Irány == 1 then KijelöltMező[0] = KijelöltMező[0] + 1
		elseif Irány == 2 then KijelöltMező[0] = KijelöltMező[0] - 1	
		elseif Irány == 3 then KijelöltMező[1] = KijelöltMező[1] + 1		
		elseif Irány == 4 then KijelöltMező[1] = KijelöltMező[1] - 1 end		
	end
	
	return Tömb;
end


LEBONTÁS:

0.szint:
generálj várost



1.szint:
a) hozd létre a játékteret
b) amíg nem lesz elég út, töltsd fel utakkal
c) rajzold ki



2.szint:
1a:
tölts fel egy 2 dimenziós tömböt házelemekkel


1b:
amíg nem lesz elég út: 
- tegyél le egy utat



3.szint:
1b3: 
amíg nincs elég út:

a) keress egy megfelelő pontot
b) válassz egy megfelelő irányt
c) amíg az útnak nincs vége:
- jelöld ki az adott irányban eggyel arrébb lévő pontot és változtasd úttá



4.szint:
1b3a:
a) ürítsük ki a "megfelelő pontok" listát

b) a legelső lépésben véletlenszerűen tegyünk le egy "út" elemet a játéktéren 

c) amíg a játéktér utolsó elemét el nem érjük, minden pontra:
- nézzük meg, "ház" elem-e
	- ha igen, nézzük meg, van-e a szomszédjában "út" elem
		- ha igen, nézzük meg, van-e legalább egy megfelelő irány mellette (feltételek: 1b3ba)
			- ha igen, adjuk hozzá a "megfelelő pontok" listához
			- ha nem, ugorjunk a következő pontra
		- ha nem, ugorjunk a következő pontra
	- ha nem, ugorjunk a következő pontra
	
d) válasszunk ki véletlenszerűen egy pontot a "megfelelő pontok" listából, legyen ez a "kezdőpont"



1b3b:
a) keressünk egy irányt, amire teljesül, hogy a "kezdőpont" mellett az adott irányban lévő mező:
- "ház" elem
- a játéktéren BELÜL van
- és olyan ház, aminek csak ház-szomszédja van

az 1b3ac-ben megadott feltételek szerint KELL lennie legalább egy ilyen pontnak.

1b3c: 
a) definiáljunk egy "kijelölt mező" változót, ami kezdésnek legyen a "kezdőpont" mellett az adott irányban lévő mező

b) amíg a "kijelölt mező" nem "út" elem vagy amíg a játéktéren BELÜL van
- változtassuk meg "kijelölt mező" elemet "út" elemmé
- léptessük a "kijelölt mezőt" eggyel arrább

c) ellenőrizzük, a "kijelölt mező" a játéktéren BELÜL van-e
- ha igen, akkor csak "út" elem lehet (ez volt a kilépési feltétel), nincs semmi dolgunk
- ha nem, akkor elértük a terep szélét, azaz a "kijelölt mezőre" meg kell hívni az útkészítő fgv.-t






A TELJES PROGRAM:
- tölts fel egy 2 dimenziós tömböt házelemekkel

- véletlenszerűen tegyünk le egy "út" elemet a játéktéren 

- amíg nincs elég út
	- ürítsük ki a "megfelelő pontok" listát
	
	KEZDŐPONTDEFINIÁLÁS:
	- amíg a játéktér utolsó elemét el nem érjük, minden pontra:
		- nézzük meg, "ház" elem-e
			- ha igen, nézzük meg, van-e a szomszédjában "út" elem
				- ha igen, nézzük meg, van-e legalább egy megfelelő irány mellette (feltételek: 1b3ba)
					- ha igen, adjuk hozzá a "megfelelő pontok" listához
					- ha nem, ugorjunk a következő pontra
				- ha nem, ugorjunk a következő pontra
			- ha nem, ugorjunk a következő pontra
	
	- válasszunk ki véletlenszerűen egy pontot a "megfelelő pontok" listából, legyen ez a "kezdőpont"

	IRÁNYVÁLASZTÁS:
	- keressünk egy irányt, amire teljesül, hogy a "kezdőpont" mellett az adott irányban lévő mező:
		- "ház" elem
		- a játéktéren BELÜL van
		- és olyan ház, aminek csak ház-szomszédja van
		(az 1b3ac-ben megadott feltételek szerint KELL lennie legalább egy ilyen pontnak.)
	
	- definiáljunk egy "kijelölt mező" változót, ami kezdésnek legyen a "kezdőpont" mellett az adott irányban lévő mező
	
	ÚTKÉSZÍTÉS:
	- amíg a "kijelölt mező" nem "út" elem vagy amíg a játéktéren BELÜL van
		- változtassuk meg "kijelölt mező" elemet "út" elemmé
		- léptessük a "kijelölt mezőt" eggyel arrább

	- ellenőrizzük, a "kijelölt mező" a játéktéren BELÜL van-e
		- ha igen, akkor csak "út" elem lehet (ez volt a kilépési feltétel), nincs semmi dolgunk
		- ha nem, akkor elértük a terep szélét, azaz a "kijelölt mezőre" meg kell hívni az útkészítő fgv.-t


- rajzold ki

]]--






