require 'playerdef'

function WindowDeclaration()


	WindowX, WindowY = love.graphics.getWidth(), love.graphics.getHeight()

	horizontal_size = math.floor(((WindowX/TileSize) - 4))
	vertical_size = math.floor((((WindowY*0.8)/TileSize) - 6))
	horizontal_begin = (WindowX - (horizontal_size*TileSize))/2
	vertical_begin = ((WindowY*0.8) - (vertical_size*TileSize))/2

end

function love.load()

	--key values
	math.randomseed( os.time() )

	TileSize = 32
	TilesetX, TilesetY = TileSize*3, TileSize*3
	
	WindowDeclaration()
	
	coord = {{1, 1}, {1, 1}, {1, 1}, {1, 1}}
	
	SizeX, SizeY = (2*horizontal_size) + (2*vertical_size), 4
	
	brightness = 16
	
	RGB_set = {
		{16*brightness -1, 	8*brightness -1, 	0},
		{16*brightness -1, 	16*brightness -1, 	16*brightness -1},
		{16*brightness -1,	16*brightness -1,	0},
		{16*brightness -1,	0,					0},
		{0	,				0,					16*brightness -1},
	}
	
	Forces = {"Future Empire", "Coalition", "Church of the Cybergod", "Anarchists"}
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	
	TilesetPics = {}
	Tileset = {}
	Load_Tilesets(Tileset, 'TILESET.png', TileSize)
	
	year = 2050 month = 1 day = 1 hour = 10
	
	CreatePlayer()
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = {cat = math.random(1,12), eng = math.random(1,#RGB_set) 
			}
		end
		
	end
	
end

function love.draw()
	
	--love.graphics.print(horizontal_size.."\n"..vertical_size.."\n"..horizontal_begin.."\n"..vertical_begin,0,0)
	brightness = 16 Set_RGB()	
	--playing field
	for j=1,SizeX do
		for i=1, SizeY do
			
			love.graphics.setColor(
			RGB_set[Field[j][i].eng][1], 
			RGB_set[Field[j][i].eng][2], 
			RGB_set[Field[j][i].eng][3])
			
			SetCoord(i,j)
			
			--bottom			
			if j >= 1 and j <= horizontal_size then			
				love.graphics.draw(TilesetPics[1], Tileset[Field[j][i].cat],
				coord[1][1], coord[1][2])
			end
			
			--right
			if j >= horizontal_size and j < horizontal_size + vertical_size then			
				love.graphics.draw(TilesetPics[1], Tileset[Field[j][i].cat],
				coord[2][1], coord[2][2])
			end
			
			--up
			if j >= horizontal_size + vertical_size and j < (horizontal_size*2) + vertical_size then			
				love.graphics.draw(TilesetPics[1], Tileset[Field[j][i].cat],
				coord[3][1], coord[3][2])					
			end

			--left
			if j >= (horizontal_size*2) + vertical_size and j < SizeX then			
				love.graphics.draw(TilesetPics[1], Tileset[Field[j][i].cat],
				coord[4][1], coord[4][2])	
			end				
		end
	end
	
	--GUI of some kind
	brightness = 8
	Set_RGB()
	love.graphics.setColor(
	RGB_set[Player.Engagement][1], 
	RGB_set[Player.Engagement][2], 
	RGB_set[Player.Engagement][3])
	love.graphics.rectangle("fill", 0, WindowY*0.8, WindowX, WindowY*0.2)
	
	love.graphics.setColor(255,255,255)
	love.graphics.print(
	year.."."..month.."."..day..", "..hour..":00".."\n"
	.."Player is called "..Player.Name..", who is \na "
	..Player.Age.." year old "..Player.Occupation
	.." and belongs to the "..Forces[Player.Engagement],
	0,WindowY*0.81)
	

end

function love.update(dt)
	k = k+1
	
	if k%60 == 0 then z=z+1 end
	
end

function Set_RGB()

	RGB_set = {
		{16*brightness -1, 	8*brightness -1, 	0},
		{16*brightness -1, 	16*brightness -1, 	16*brightness -1},
		{16*brightness -1,	16*brightness -1,	0},
		{16*brightness -1,	0,					0},
		{0	,				0,					16*brightness -1},
	}

end


--a table with the name of "SetName" must be declared before its call
function Load_Tilesets(SetName, ImageName, UnitSize)
	love.graphics.setDefaultFilter( "nearest", "nearest", 1 )
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local CharX = TilesetPics[1]:getWidth()*(TileSize/8)
	local CharY = TilesetPics[1]:getHeight()*(TileSize/8)
	
	local TilesX = CharX/TileSize
	local TilesY = CharY/TileSize

	for j=1, TilesY do
		for i=1, TilesX do			
			SetName[((j-1)*TilesX) + i] = love.graphics.newQuad((i-1)*UnitSize, (j-1)*UnitSize, UnitSize, UnitSize, CharX, CharY)
		end
	end	
end







function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end


--count mode returns the amount of "value" in "Table"
--indicate mode returns true if "value" is found in "Table"
--list mode returns the coordinates of "value" positions in "Table"
function Colors(Table, valueToCheck, mode)

	local asd = 0
	local list = {}
	local indicator = true
	
	for j=1, SizeX do
		for i=1, SizeY do
			if mode == "count" then
				if Table[j][i] == valueToCheck then asd=asd+1 end
			elseif mode == "list" then
				if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
			elseif mode == "indicate" then
				indicator = indicator or Table[j][i] == valueToCheck
			end
		end
	end
	
	return asd
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end

function SetCoord(i,j)

	coord = {
		--bottom
		{	horizontal_begin + (j-1)*TileSize, 
			((WindowY*0.8) - (1*TileSize)) + (i-2)*TileSize
		},
		--right
		{	(WindowX - (2*TileSize)) + (i-1)*TileSize, 
			(WindowY*0.8) - vertical_begin - (j-horizontal_size+1)*TileSize
		},
		--up
		{	(WindowX - horizontal_begin) - (j-(horizontal_size + vertical_size)+1)*TileSize, 
			TileSize - (i-1)*TileSize	
		},
		--left
		{	((1*TileSize)) - (i-1)*TileSize, 
			vertical_begin + (j-((horizontal_size*2) + vertical_size))*TileSize 
		},
	}

end

function love.keypressed(key)

if key =="1" then love.window.setMode(320*0.6,320) WindowDeclaration() end
if key =="2" then love.window.setMode(400*0.6,400) WindowDeclaration() end
if key =="3" then love.window.setMode(480*0.6,480) WindowDeclaration() end
if key =="4" then love.window.setMode(600*0.6,600) WindowDeclaration() end
if key =="5" then love.window.setMode(720*0.6,720) WindowDeclaration() end
if key =="6" then love.window.setMode(900*0.6,900) WindowDeclaration() end
if key =="7" then love.window.setMode(1280*0.6,1280) WindowDeclaration() end

end





