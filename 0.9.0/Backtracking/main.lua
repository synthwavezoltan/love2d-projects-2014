function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 16
	TilesetX, TilesetY = 48, 48
	SizeX, SizeY = 49, 34
	rand = 0
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0

	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = 1	
		end
		
	end
	
	--start,finish,selector
	StartX, StartY = 40,30
	FinishX,FinishY = 2,2
	SelectedX, SelectedY = 15,15
	
	for j=1,SizeX do
		for i=1, SizeY do
			rand = math.random(1000)%3
			if rand == 0 then Field[j][i] = 2 end
		end
	end
	
	--while Field[StartX][StartY] ~= 1 do StartX, StartY =  math.random(2,SizeX-2), math.random(2,SizeY-2) end
	--while Field[FinishX][FinishY] ~= 1 do FinishX, FinishY =  math.random(2,SizeX-2), math.random(2,SizeY-2) end
	
	SelectedX, SelectedY = StartX, StartY
	
	Field[StartX][StartY] = 4
	Field[FinishX][FinishY] = 9
	
	--call the recursion
	--while Field[SelectedY][SelectedY] ~= 9 do Iterator(Field,1,3) end
	
	rand=0
end

function love.draw()

	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPic, Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
		end
	end
	
	love.graphics.print(SelectedX..","..SelectedY,(SelectedX-1)*TileSize, (SelectedY-1)*TileSize)
	
end

function love.update(dt)
	rand = rand+1
	
	if rand%10 == 0 
	and CountNeighbours(SelectedX, SelectedY, 9, Field) == 0 
	and CountNeighbours(SelectedX, SelectedY, 7, Field) ~= 4 	
	then Iterator(1,4) end
end


--DA FUNCTION
--1: you CAN visit
--2: you CAN NOT visit
--3: you ALREADY VISITED
--4: WRONG WAY
function Iterator(condition, writeit)

	local wrong_neighbours = 0
	
	--up
	if SelectedY-1 > 0 and Field[SelectedX][SelectedY-1] == condition then
		Field[SelectedX][SelectedY] = writeit
		SelectedY = SelectedY-1
	else
		wrong_neighbours = wrong_neighbours + 1
		--right
		if SelectedX+1 <= SizeX and Field[SelectedX+1][SelectedY] == condition then
			Field[SelectedX][SelectedY] = writeit
			SelectedX = SelectedX+1
		else
			wrong_neighbours = wrong_neighbours + 1
			--down
			if SelectedY+1 <= SizeY and Field[SelectedX][SelectedY+1] == condition then
				Field[SelectedX][SelectedY] = writeit
				SelectedY = SelectedY+1
			else
				wrong_neighbours = wrong_neighbours + 1	
					--left
					if SelectedX-1 > 0 and Field[SelectedX-1][SelectedY] == condition then
						Field[SelectedX][SelectedY] = writeit
						SelectedX = SelectedX-1
					else
						wrong_neighbours = wrong_neighbours + 1
				end
			end
		end		
	end
	
	if wrong_neighbours == 4 then Iterator(writeit,7) end

end


function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if x-1 > 0 and Table[x-1][y] == valueToCheck then asd=asd+1 end
	if x+1 < SizeY and Table[x+1][y] == valueToCheck then asd=asd+1 end
	if y-1 > 0 and Table[x][y-1] == valueToCheck then asd=asd+1 end
	if y+1 < SizeX and Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end


function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




