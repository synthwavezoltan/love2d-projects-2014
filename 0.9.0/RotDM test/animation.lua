--animates a given tile; must be run for every tiles
function TileAnimations(a, b, speed, layer)

	local tile = 1
	local dix = FieldOffX+shakex+(a-1)*TileSize
	local diy = FieldOffY+shakey+init+(b-1)*TileSize
	
	--layer-oriented declaration
	if layer == 0 
	then tile = Field[a][b]
	else tile = DecorLayer[layer][a][b] end
	
	--variables used for the 3-phase animations
	local trph_flaga = (gameclock%speed >  (speed/3)*2)
	local trph_flagb = (gameclock%speed <=  speed/3   )
	
	--variables used for the 4-phase animations
	local fph_flaga = (math.ceil((gameclock%speed)/(speed/2)))%2 == 0
	local fph_flagb = (math.ceil((gameclock%speed)/(speed/4)))%2 == 0
	
	
	
	
	--animating 3-phase animations
	if AnimationSequence[tile] ~= nil and #AnimationSequence[tile] == 3 then
	
		if 	   trph_flaga and		trph_flagb then tile = AnimationSequence[tile][1] end
		if not trph_flaga and 	not trph_flagb then tile = AnimationSequence[tile][2] end 
		if     trph_flaga and	not	trph_flagb then tile = AnimationSequence[tile][3] end
	
		love.graphics.draw(TilesetPics[1], Tileset[tile], dix, diy)
	
	
	end
	
	--animating 4-phase animations
	if AnimationSequence[tile] ~= nil and #AnimationSequence[tile] == 4 then
	
		if not fph_flaga and not fph_flagb then tile = AnimationSequence[tile][1] end
		if not fph_flaga and     fph_flagb then tile = AnimationSequence[tile][2] end 
		if	   fph_flaga and not fph_flagb then tile = AnimationSequence[tile][3] end
		if     fph_flaga and     fph_flagb then tile = AnimationSequence[tile][4] end
		
		love.graphics.draw(TilesetPics[1], Tileset[tile], dix, diy)
	
	
	end


	
	

end


--[[

110000 1 0 0 (x%6 <= 2)
000011 0 0 1 (x%6 > 4)

00001111 0 0 1 1 ((math.ceil((x%8)/4))%2 == 0)
00110011 0 1 0 1 ((math.ceil((x%8)/2))%2 == 0)

]]