--behaviour of triggers
function Behaviour_Triggers()

	if Player.TileTrigger then

		if Field[Player.TileX][Player.TileY] == NamedTiles["Right trigger"]
		then 
			T_Negation("right", Player.TileX, Player.TileY, false) 
			Player.TileTrigger = false 
		end 	
	
		if Field[Player.TileX][Player.TileY] == NamedTiles["Left trigger"] 
		then 
			T_Negation("left", Player.TileX, Player.TileY, false) 
			Player.TileTrigger = false			
		end 	
	
		if Field[Player.TileX][Player.TileY] == NamedTiles["Up trigger"] 
		then 
			T_Negation("up", Player.TileX, Player.TileY, false) 
			Player.TileTrigger = false 
		end 	
	
		if Field[Player.TileX][Player.TileY] == NamedTiles["Down trigger"]
		then 
			T_Negation("down", Player.TileX, Player.TileY, false) 
			Player.TileTrigger = false			
		end 	
	
	end
	
end


--behaviour of locks
function Behaviour_Locks()

end


--behaviour of DMOs
function Behaviour_DMOs()
	
	if DMOstate then for i=1, #DMOset do Field[DMOset[i][1]][DMOset[i][2]] = NamedTiles["Dark Matter Orb on"]  end
				else for i=1, #DMOset do Field[DMOset[i][1]][DMOset[i][2]] = NamedTiles["Dark Matter Orb off"] end end

	
end


