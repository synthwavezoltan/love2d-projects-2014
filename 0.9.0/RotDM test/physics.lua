--those tiles in Tileset which shouldn't be allowed for player to cross
SolidTiles = 
{			3, 	4, 	5, 	6, 	7, 	8,
	10, 11, 12, 13, 14, 15, 16,
	19, 20, 21, 22,

	
	}

function SolidChecker(x,y)

	local isSolid = false
	
	--gate 1: it's on SolidTiles list
	for i=1, #SolidTiles do
		if Field[x][y] == SolidTiles[i] then	
			isSolid = true
			break
		else
			isSolid = false
		end
	end
		

	
	return isSolid
end

