--not an actual load, just generates some things randomly
function Load_Test()

	for i=1,SizeX do
		for j=1, SizeY do
			Field[i][j] = 1

			if i<4 or i>SizeX-5
			or j<4 or j>SizeY-5 then
				Field[i][j] = 3
			end
			
			for k=1, DecorLayerNum do
				DecorLayer[k][i][j] = 180
			end
		end	
	end

	--some randomly placed affected floors
	for i=1, 400 do Field[math.random(6,SizeX-5)][math.random(6,SizeY-5)] = 2 end
	
	--design experiments
	--floors
	for i=4, SizeY-5 do Field[4][i] = 17 end
	for i=4, math.random(4, 9) do Field[5][i] = 17 end
	for i=4, SizeX-5 do Field[i][7] = 17 end
	
	for i=1, 70 do Field[math.random(5,SizeX-5)][math.random(5,SizeY-5)] = 17 end
	
	--walls	
	for i=4, SizeX-5 do 
		Field[i][4] = math.random(11,15) 
		Field[i][5] = math.random(11,15) 
		Field[i][6] = math.random(11,15) 		
	end	
	Field[4][4] = 10 
	Field[4][5] = 10 
	Field[4][6] = 10 	
	Field[SizeX-5][4] = 16	
	Field[SizeX-5][5] = 16
	Field[SizeX-5][6] = 16	
	
	--exitgate properties
	ExitGate.X 			= math.random(10, SizeX-10)*TileSize -4*4
	ExitGate.Y 			= math.random(10, SizeY-10)*TileSize -6*4		
	ExitGate.ExitTileX 	= math.floor((4*4+ExitGate.X)/TileSize) + 1
	ExitGate.ExitTileY 	= math.floor((6*4+ExitGate.Y)/TileSize) + 1

	
	--creating place around the gate
	local x, y = ExitGate.ExitTileX - 1, ExitGate.ExitTileY - 1
	
	for i=x, x+2 do
		Field[i][y-2] = 3
		Field[i][y-3] = 3
	end
	
	Field[x  ][y-1] = 10
	Field[x+1][y-1] = math.random(11, 15)
	Field[x+2][y-1] = 16
	Field[x  ][y  ] = 10
	Field[x+1][y  ] = math.random(11, 15)
	Field[x+2][y  ] = 16
	Field[x+1][y+1] = 1

	--some triggers
	Field[11][14] = 24		
	Field[ 9][14] = 25
	Field[10][14] = 26
	Field[10][15] = 27

	
	for i=3, 14 do
		Field[2*i][7] = math.random(26,27)
	end
	
	--some dark matter orbs	
	for i=3, 14 do
		Field[2*i][9] = math.random(35, 36)
	end	
	
	--some bio-monitors
	for i=1, 5 do
	Field[20+i][ 3] =  7 
	Field[20+i][ 4] = 74
	Field[20+i][ 5] = math.random(83,86)
	Field[20+i][ 6] = 92
	end

end

function Load_Empty()

	for i=1,SizeX do
		for j=1, SizeY do
			Field[i][j] = 17

			if i<4 or i>SizeX-5
			or j<4 or j>SizeY-5 then
				Field[i][j] = 3
			end
			
			for k=1, DecorLayerNum do
				DecorLayer[k][i][j] = 180
			end
		end	
	end

	--walls	
	for i=4, SizeX-5 do 
		Field[i][4] = math.random(11,15) 
		Field[i][5] = math.random(11,15) 
	end	
	Field[4][4] = 10 
	Field[4][5] = 10 
	Field[SizeX-5][4] = 16	
	Field[SizeX-5][5] = 16
	
	--exitgate properties
	ExitGate.X 			= math.random(10, SizeX-10)*TileSize -4*4
	ExitGate.Y 			= math.random(10, SizeY-10)*TileSize -6*4		
	ExitGate.ExitTileX 	= math.floor((4*4+ExitGate.X)/TileSize) + 1
	ExitGate.ExitTileY 	= math.floor((6*4+ExitGate.Y)/TileSize) + 1

	
	--creating place around the gate
	local x, y = ExitGate.ExitTileX - 1, ExitGate.ExitTileY - 1
	
	for i=x, x+2 do
		Field[i][y-2] = 3
		Field[i][y-3] = 3
	end
	
	Field[x  ][y-1] = 10
	Field[x+1][y-1] = math.random(11, 15)
	Field[x+2][y-1] = 16
	Field[x  ][y  ] = 10
	Field[x+1][y  ] = math.random(11, 15)
	Field[x+2][y  ] = 16
	Field[x+1][y+1] = 1	
	
end