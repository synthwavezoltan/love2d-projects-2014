require 'input'
require 'physics'
require 'mapload'
require 'animation'
require 'behaviour'
require 't-negation'

function love.load()

	math.randomseed( os.time() )
	--key values, used in displaying
	TileSize = 32						--size of a single tile in pixels
	SizeX, SizeY = 40, 40				--size of playing field
	ScreenX, ScreenY = 1280, 640		--size of the part of the window in which field is displayed

	Scrolling_Value = 1
	
	init = (720 - (20*TileSize))/2		--Y offset
	shakex, shakey = 0, 0
	
	FieldOffX = 0
	FieldOffY = 0
	
	DecorLayerNum = 4					--amount of used decoration layers
	
	--technical variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	gameclock = 0
	standclock = 0
	
	MouseX, MouseY = 0, 0
	MouseTileX, MouseTileY = 0, 0


	EditingMode = false
	EditedLayer = 0
	EditSelTile = 1


	

	
	--game variables
	power_max = 250
	power_used = 0
	power_empty = 0
	
	DMOset = {}
	DMOstate = false
	
	--constants
	
	--elements of this set are tileIDs respecting to TILESET.PNG
	--these are additional sets which contain a sequence of numbers
	--this sequence defines the animation (can't explain better)
	
	AnimationSequence = {}
	
	AnimationSequence[24] = { 24, 136, 137, 138 }	--triggers
	AnimationSequence[25] = { 25, 145, 146, 147 }
	AnimationSequence[26] = { 26, 154, 155, 156 }
	AnimationSequence[27] = { 27, 163, 164, 165 }	
	
	AnimationSequence[33] = { 33, 139, 140, 141 }	--locks
	AnimationSequence[34] = { 34, 148, 149, 150 }
	AnimationSequence[35] = { 35, 157, 158, 159 }
	AnimationSequence[36] = { 36, 166, 167, 168 }		
	
	AnimationSequence[31] = { 31, 151, 152, 153 }	--switches
	AnimationSequence[32] = { 32, 160, 161, 162 }
	
	AnimationSequence[22] = { 22, 134, 135}			--dark matter orbs
	AnimationSequence[23] = { 23, 143, 144}
	
	AnimationSequence[84] = { 84, 169, 170, 171 }	--bio-mechanic monitor 1
	
	--test for naming tiles
	NamedTiles = {}
	
	NamedTiles["Lamp Floor on"] 		= 2
	NamedTiles["Lamp Floor off"] 		= 1	
	
	NamedTiles["Up trigger"]			= 26
	NamedTiles["Down trigger"]			= 27
	NamedTiles["Left trigger"]			= 25
	NamedTiles["Right trigger"]			= 24
	
	NamedTiles["Up lock"]				= 35
	NamedTiles["Down lock"]				= 36
	NamedTiles["Left lock"]				= 33
	NamedTiles["Right lock"]			= 34
	
	NamedTiles["Vertical switch"]		= 31
	NamedTiles["Horizontal switch"]		= 32
	
	NamedTiles["Dark Matter Orb on"] 	= 23
	NamedTiles["Dark Matter Orb off"] 	= 22

	NamedTiles["Empty tile"] 			= 180

	
	--loading font
	font = love.graphics.newFont("slkscre.ttf", 16)
	love.graphics.setFont(font)	
	
	--creating/loading pictures	
	TilesetPics = {}								--creating tileset
	Tileset = {}
	Load_Tilesets(Tileset, 'TILESET.png', 32)							
	GateTex = {}									--same goes for exit gate
	Load_Tilesets(GateTex, 'GATE.png', 64)		
	PlayerSprites = {}								--and for the sprites of the player
	Load_Tilesets(PlayerSprites, 'MYRTES.PNG', 32)	
	
	--other pics	
	Pics = {}
	Pics[1] = love.graphics.newImage('HUD.png')	
	Pics[2] = love.graphics.newImage('HUD.png')
	Pics[3] = love.graphics.newImage('HUD.png')
	
	for i=1, #Pics do Pics[i]:setFilter("nearest", "nearest") end
	
	--creating field
	Field = { }
	
	for i=1,SizeX do
	
		Field[i] = { }
	
		for j=1, SizeY do
			Field[i][j] = 1

			if i<4 or i>SizeX-5
			or j<4 or j>SizeY-5 then
				Field[i][j] = 3
			end
		end
		
	end
	
	--creating an additional set of layers for decorations
	DecorLayer = { }
	
	for k=1, DecorLayerNum do
		DecorLayer[k] = { }	
		
		for i=1,SizeX do
			DecorLayer[k][i] = { }	
			for j=1, SizeY do DecorLayer[k][i][j] = 180 end		
		end			
	end
	
	
	--creating player
	Player = {
		X = 64, Y = 96, 
		TileX = 4, TileY = 6,
		OffsetX = 0, OffsetY = 0,
		sizeX = TileSize, sizeY = TileSize,
		Mov_Dir = "no", 
		Speed = 4,				--(of scrolling)
		TileTrigger = true,		--used for triggers and switches
		
		Sprite = 1,
		
		IsMoving = false, IsOnGround = false,
	}
	
	--creating gateobject
	ExitGate = {
		X = 0,
		Y = 0,
		Sprite = 1,
		Open = false,
		ExitTileX = 0,
		ExitTileY = 0,
	}
	
	--and finally: load a test map
	Load_Empty()
	
	
end

function love.draw()
	
	--field
	for j=1,SizeX do
		for i=1, SizeY do
			--shows where the tile is gonna be displayed on the screen
			local dispx = FieldOffX+shakex+(j-1)*TileSize
			local dispy = FieldOffY+shakey+init+(i-1)*TileSize
			--display the tile only if it would be displayed within the display area
			if  dispx > 0-TileSize and dispx < ScreenX + TileSize
			and dispy > 0-TileSize and dispy < ScreenY + TileSize then
				love.graphics.draw(TilesetPics[1], Tileset[Field[j][i]], dispx, dispy) 
				TileAnimations(j, i, 40, 0)
			end
		end
	end
	

	
	--player
	--display player only if it would be displayed within the display area
	local dispx = FieldOffX + shakex	   	 + Player.OffsetX + (Player.TileX-1)*TileSize
	local dispy = FieldOffY + shakey +	init + Player.OffsetY + (Player.TileY-1)*TileSize - (TileSize/2)
	
	if  dispx > 0-TileSize and dispx < ScreenX + TileSize
	and dispy > 0-TileSize and dispy < ScreenY + TileSize then
		love.graphics.draw(TilesetPics[3], PlayerSprites[Player.Sprite], dispx, dispy)
	end

	--decorate layers
	for j=1,SizeX do
		for i=1, SizeY do	
			local dispx = FieldOffX+shakex+(j-1)*TileSize
			local dispy = FieldOffY+shakey+init+(i-1)*TileSize

			if  dispx > 0-TileSize and dispx < ScreenX + TileSize
			and dispy > 0-TileSize and dispy < ScreenY + TileSize then
												
				for k=1, DecorLayerNum do
					--set translucency in case when player is near to the drawable
					if 	j >= Player.TileX - 2 and j <= Player.TileX + 2
					and i >= Player.TileY - 2 and i <= Player.TileY + 2
					then love.graphics.setBlendMode("additive")
					else love.graphics.setBlendMode("alpha") end				
					
					--then draw them
					love.graphics.draw(TilesetPics[1], Tileset[DecorLayer[k][j][i]], dispx, dispy) 
					TileAnimations(j, i, 40, k)
					
					--set back
					love.graphics.setBlendMode("alpha")
				end
				
				
			end
		end
	end	
	
	--displaying gate
	love.graphics.draw(TilesetPics[2], GateTex[ExitGate.Sprite], FieldOffX + ExitGate.X , FieldOffY + ExitGate.Y)
	
	
	
	
	
	--an attempt to some simpler way to implement scroll
	if  dispx < 		  (TileSize*3) then FieldOffX = FieldOffX + (Player.Speed*1) end
	if  dispx > ScreenX	- (TileSize*4) then FieldOffX = FieldOffX - (Player.Speed*1) end
	if  dispy < init 	+ (TileSize*3) then FieldOffY = FieldOffY + (Player.Speed*1) end
	if  dispy > ScreenY	- (TileSize*3) then FieldOffY = FieldOffY - (Player.Speed*1) end
	
--[[
	for i=3, 80 do
		love.graphics.circle("line",  dispx+16, dispy+16, 2*(gameclock%i), gameclock%(i*3))y
	end
]]
	
--[[
	--demo exit indicator
	love.graphics.line(
	FieldOffX + ExitGate.X + 16, 
	FieldOffY + ExitGate.Y + 16,
	
	16 + FieldOffX + shakex	   	 	+ Player.OffsetX + (Player.TileX-1)*TileSize,
	16 + FieldOffY + shakey + init 	+ Player.OffsetY + (Player.TileY-1)*TileSize - (TileSize/2)
	)	
]]	
	--HUD
	love.graphics.draw(Pics[1], 0, 0)
	
	--map end text
	if 	Player.TileX == ExitGate.ExitTileX
	and Player.TileY == ExitGate.ExitTileY
	then love.graphics.print("you're winner! ...okay, well, that's what I made so far.", 96, 18)
	end
	
	--debug text
	love.graphics.print("Power usage = "..power_used.."/"..power_max..
						"\nUnused power (test): "..power_empty, 736,0)
						
	love.graphics.print("Number of DMOs = "..#DMOset..
						"\nSelected tile: "..EditSelTile, 1024,0)						
	
	if EditingMode
	then love.graphics.print("Editing mode", 0,0)
	else love.graphics.print("Playing mode", 0,0) end
	
	--editing mode
	--display selected tile at mouse
	--display row of selected tile of Tileset
	if EditingMode then
		local dispx = (MouseTileX-1)*TileSize
		local dispy = (MouseTileY)*TileSize
		love.graphics.draw(TilesetPics[1], Tileset[EditSelTile], ((MouseTileX-1)*TileSize)+FieldOffX, (MouseTileY*TileSize)+8+FieldOffY)
		love.graphics.print(MouseTileX..", "..MouseTileY, MouseX, MouseY)
		--love.graphics.print(MouseTileX..", "..MouseTileY, MouseX, MouseY-16)
		--love.graphics.print(MouseX - FieldOffX..", "..MouseY - FieldOffY - init, MouseX, MouseY-16)
		
		local rem = EditSelTile%9
		
		for i = EditSelTile - rem +1, EditSelTile - rem + 9 do
			love.graphics.draw(TilesetPics[1], Tileset[i], 512 + 32*(i - EditSelTile), 688)
		end
		
		love.graphics.print("Current layer: "..EditedLayer, 992, 688)
	end



end

function love.update(dt)
	
	--------------------------
	--	VARIABLE DEFINITION	--
	--------------------------
	
	--set some variables
	gameclock 			= 	gameclock + 1
	
	--checks, for how many frames player is standing
	if not Player.IsMoving then 
	standclock 			= 	standclock + 1 
	else 
	standclock			= 	0 
	end	
	
	--checks whether exit is open
	if 	Field[ExitGate.ExitTileX  ][ExitGate.ExitTileY] == 2 then 
	ExitGate.Open 		= 	true 
	else 
	ExitGate.Open 		= 	false 
	end	
	

	MouseX 				= 	love.mouse.getX()
	MouseY 				= 	love.mouse.getY()	
	MouseTileX 			= 	1+math.floor((MouseX - FieldOffX		 )/TileSize)
	MouseTileY 			= 	1+math.floor((MouseY - FieldOffY - init)/TileSize)	
	
	power_empty			= 	0
	power_used 			= 	0
	
	for j=1,SizeX do
		for i=1, SizeY do	
		
			if Field[j][i] == NamedTiles["Lamp Floor off"] 
			or Field[j][i] == NamedTiles["Dark Matter Orb off"] 
			then power_empty = power_empty +1 end
			
			if Field[j][i] == NamedTiles["Lamp Floor on"] 
			or Field[j][i] == NamedTiles["Dark Matter Orb on"] 
			then power_used  = power_used  +1 end

		end
	end	
	
	--------------------------------
	--  MANDATORY FUNCTION CALLS  --
	--------------------------------
	
	Repeated_Key_Behaviour()

	if Player.IsMoving then
		SmoothMoving_Player(Player.Mov_Dir, Player.Speed)
	end	
	
	Behaviour_Triggers()
	Behaviour_DMOs()
	
	--EditingAction()

	---------------------
	--  MISCELLANEOUS  --
	---------------------
	
	--player animation
	if Player.IsMoving then 
		if 		Player.Mov_Dir == "right" then Player.Sprite = 2 + math.floor(((gameclock%30)/15))
		elseif  Player.Mov_Dir == "left"  then Player.Sprite = 4 + math.floor(((gameclock%30)/15)) end
	else 
		if standclock > 2 then 
			while Player.Sprite%3 ~= 1 do Player.Sprite = Player.Sprite - 1 end
		end 
	end

	--exit gate animation
	if gameclock%8 == 0 then ExitGate.Sprite = ExitGate.Sprite + 1 end 

	if ExitGate.Open 
	then if ExitGate.Sprite > 15 then ExitGate.Sprite = 6 end
	else if ExitGate.Sprite >  5 then ExitGate.Sprite = 1 end
	end	
	
	--very simple implementation of death
	if Field[Player.TileX][Player.TileY] == 1 then
		Player.TileX = 4
		Player.TileY = 7
		FieldOffX = 0
		FieldOffY = 0
	end
	

	
	--[[
	shakex = math.random(-32, 32)
	shakey = math.random(-32, 32)
	]]
end










function SmoothMoving_Player (direction, speed)
	if math.abs(Player.OffsetX) == TileSize 
	or math.abs(Player.OffsetY) == TileSize then 	--if the player scrolled a tile
		Player.IsMoving = false						--stop moving 
		--and then for every directions: change player location
		if direction == 'up' then 		 Player.TileY = Player.TileY - 1 
		elseif direction == 'down' then  Player.TileY = Player.TileY + 1 
		elseif direction == 'left' then  Player.TileX = Player.TileX - 1 
		elseif direction == 'right' then Player.TileX = Player.TileX + 1 end		
		
		if direction == 'up' or direction == 'down'    then Player.OffsetY = 0 end
		if direction == 'left' or direction == 'right' then Player.OffsetX = 0 end
		
		Player.TileTrigger = true
	else
		--if player is within a tile, then for every directions: scroll player
		if     direction == 'up' 	then Player.OffsetY = Player.OffsetY - speed 				
		elseif direction == 'down'  then Player.OffsetY = Player.OffsetY + speed  
		elseif direction == 'left'  then Player.OffsetX = Player.OffsetX - speed 			
		elseif direction == 'right' then Player.OffsetX = Player.OffsetX + speed 
		end		
	end	
end












--a table with the name of "SetName" must be declared before its call
function Load_Tilesets(SetName, ImageName, UnitSize)
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	TilesetPics[#TilesetPics]:setFilter("nearest", "nearest")	
	
	local modifier = 4
	local TilesetX = TilesetPics[#TilesetPics]:getWidth()*modifier
	local TilesetY = TilesetPics[#TilesetPics]:getHeight()*modifier
	
	local TilesX = TilesetX/UnitSize
	local TilesY = TilesetY/UnitSize

	for j=1, TilesY do
		for i=1, TilesX do			
			SetName[((j-1)*TilesX) + i] = love.graphics.newQuad((i-1)*UnitSize, (j-1)*UnitSize, UnitSize, UnitSize, TilesetX, TilesetY)
		end
	end	
end





function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end


--count mode returns the amount of "value" in "Table"
--indicate mode returns true if "value" is found in "Table"
--list mode returns the coordinates of "value" positions in "Table"
function Colors(Table, valueToCheck, mode)

	local asd = 0
	local list = {}
	local indicator = true
	
	for j=1, SizeX do
		for i=1, SizeY do
			if mode == "count" then
				if Table[j][i] == valueToCheck then asd=asd+1 end
			elseif mode == "list" then
				if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
			elseif mode == "indicate" then
				indicator = indicator or Table[j][i] == valueToCheck
			end
		end
	end
	
	return asd
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




