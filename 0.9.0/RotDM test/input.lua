function love.keypressed(key)
	
	if EditingMode then
		if key == "up" 		and EditSelTile > 9 			then EditSelTile = EditSelTile - 9 end
		if key == "down" 	and EditSelTile < #Tileset - 9 	then EditSelTile = EditSelTile + 9 end
		if key == "left" 	and EditSelTile > 1 			then EditSelTile = EditSelTile - 1 end
		if key == "right" 	and EditSelTile < #Tileset - 1 	then EditSelTile = EditSelTile + 1 end
		
		if key == "r" then EditedLayer = EditedLayer+1 end
		if key == "q" then EditedLayer = EditedLayer-1 end
		if key == "0" then EditedLayer = 0 end
	else
		if key == "up" 		or key == "down" 
		or key == "left" 	or key == "right"
		then T_Negation(key, Player.TileX, Player.TileY, true) end
	end
	
	if key == "e" then
		EditingMode = not EditingMode
		
		if EditingMode 
		then --Load_Empty()
		else --Load_Test() 
		end
	end
	

end

function love.mousepressed(x,y,button)	
	
	--mouse actions in editing mode
	if EditingMode then
		if button == "l" then 
			--pre-checks
			--if the selected tile is a Dark Matter Orb (DMO), then add its position to "DMOset"
			if (EditSelTile == NamedTiles["Dark Matter Orb off"]
			or  EditSelTile == NamedTiles["Dark Matter Orb on"])
			and Field[MouseTileX][MouseTileY] ~= NamedTiles["Dark Matter Orb off"]
			and Field[MouseTileX][MouseTileY] ~= NamedTiles["Dark Matter Orb on"]				
			then
				table.insert(DMOset, {MouseTileX, MouseTileY})
			end
			
			--the actual action
			if EditedLayer == 0 
			then Field[MouseTileX][MouseTileY] = EditSelTile 
			else DecorLayer[EditedLayer][MouseTileX][MouseTileY] = EditSelTile 
			end
		end
	else
	
	end
	
end

function EditingAction()
	if love.mouse.isDown("l") then 
		if not (EditSelTile == NamedTiles["Dark Matter Orb off"] or EditSelTile == NamedTiles["Dark Matter Orb on"]) then
			if EditedLayer == 0 
			then Field[MouseTileX][MouseTileY] = EditSelTile 
			else DecorLayer[EditedLayer][MouseTileX][MouseTileY] = EditSelTile 
			end
		end
	end
end






--separated input functions

function Repeated_Key_Behaviour()

	--player moving; only if not moving yet
	if not Player.IsMoving then
		if love.keyboard.isDown("w") and not SolidChecker(Player.TileX, Player.TileY-1) then
			Player.IsMoving = true
			Player.Mov_Dir = "up"	
		end
		if love.keyboard.isDown("s") and not SolidChecker(Player.TileX, Player.TileY+1) then
			Player.IsMoving = true
			Player.Mov_Dir = "down"	
		end
		if love.keyboard.isDown("a")and not SolidChecker(Player.TileX-1, Player.TileY) then
			Player.IsMoving = true
			Player.Mov_Dir = "left"	
		end
		if love.keyboard.isDown("d") and not SolidChecker(Player.TileX+1, Player.TileY) then
			Player.IsMoving = true
			Player.Mov_Dir = "right"	
		end	
	end
end


function Moving()

	if love.keyboard.isDown("w") then end
	
	if love.keyboard.isDown("s") then end
	
	if love.keyboard.isDown("a") then end
	
	if love.keyboard.isDown("d") then end
	

	
end