--the main gameplay action
--functionized to be able to trigger it by anything
function T_Negation(dir, startx, starty, AdjacentMustOn)

--[[
	11 -> 1&1&1 | 0&0|1 = 1|0 = 1
	10 -> 1&1&0 | 0&0|0 = 0|0 = 0
	01 -> 0&0&1 | 1&1|1 = 0|1 = 1
	00 -> 0&0&0 | 1&1|0 = 0|1 = 1
]]

	if 		dir == "up"		
	and	(
	(AdjacentMustOn == true 	and (AdjacentMustOn 		and Field[startx  ][starty-1] == 2))
	or
	(AdjacentMustOn == false 	and ((not AdjacentMustOn) 	or  Field[startx  ][starty-1] == 2))
	)
	then 
	
		local x = startx
		local y = starty-1
		
		--rules for regular floors
		if Field[x  ][y  ] < 3 					   		and power_used < power_max then Field[x  ][y  ] = 3-Field[x  ][y  ] end
		if Field[x-1][y-1] < 3 and y > 0 and x > 0 		and power_used < power_max then Field[x-1][y-1] = 3-Field[x-1][y-1] end
		if Field[x  ][y-1] < 3 and y > 0		   		and power_used < power_max then Field[x  ][y-1] = 3-Field[x  ][y-1] end	
		if Field[x+1][y-1] < 3 and y > 0 and x <= SizeX	and power_used < power_max then Field[x+1][y-1] = 3-Field[x+1][y-1] end
	
		--rules for DMOs
		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb off"]) 			  or (Field[x-1][y-1] == NamedTiles["Dark Matter Orb off"] and y > 0 and x > 0)  		
		or (Field[x  ][y-1] == NamedTiles["Dark Matter Orb off"] and y > 0	) or (Field[x+1][y-1] == NamedTiles["Dark Matter Orb off"] and y > 0 and x <= SizeX)	
		then DMOstate = true end
		
		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb on"]) 			or (Field[x-1][y-1] == NamedTiles["Dark Matter Orb on"] and y > 0 and x > 0) 		
		or (Field[x  ][y-1] == NamedTiles["Dark Matter Orb on"] and y > 0)  or (Field[x+1][y-1] == NamedTiles["Dark Matter Orb on"] and y > 0 and x <= SizeX)	
		then DMOstate = false end		
		
		
	elseif	dir == "down"
	and	(
	(AdjacentMustOn == true 	and (AdjacentMustOn 		and Field[startx  ][starty+1] == 2))
	or
	(AdjacentMustOn == false 	and ((not AdjacentMustOn) 	or  Field[startx  ][starty+1] == 2))
	)
	then 
	
		local x = startx
		local y = starty+1
		
		
		if Field[x  ][y  ] < 3 					   		 	 and power_used < power_max then Field[x  ][y  ] = 3-Field[x  ][y  ] end
		if Field[x-1][y+1] < 3 and y <= SizeY and x > 0 	 and power_used < power_max then Field[x-1][y+1] = 3-Field[x-1][y+1] end
		if Field[x  ][y+1] < 3 and y <= SizeY		   		 and power_used < power_max then Field[x  ][y+1] = 3-Field[x  ][y+1] end	
		if Field[x+1][y+1] < 3 and y <= SizeY and x <= SizeX and power_used < power_max then Field[x+1][y+1] = 3-Field[x+1][y+1] end

		
		
		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb off"]) 					or (Field[x-1][y+1] == NamedTiles["Dark Matter Orb off"] and y <= SizeY and x > 0) 	 
        or (Field[x  ][y+1] == NamedTiles["Dark Matter Orb off"] and y <= SizeY)	or (Field[x+1][y+1] == NamedTiles["Dark Matter Orb off"] and y <= SizeY and x <= SizeX) 
		then DMOstate = true end
		
		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb on"]) 				or (Field[x-1][y+1] == NamedTiles["Dark Matter Orb on"] and y <= SizeY and x > 0) 	 
        or (Field[x  ][y+1] == NamedTiles["Dark Matter Orb on"] and y <= SizeY)	or (Field[x+1][y+1] == NamedTiles["Dark Matter Orb on"] and y <= SizeY and x <= SizeX) 
		then DMOstate = false end		
		
	
	
	elseif	dir == "left"
	and	(
	(AdjacentMustOn == true 	and (AdjacentMustOn 		and Field[startx-1][starty  ] == 2))
	or
	(AdjacentMustOn == false 	and ((not AdjacentMustOn) 	or  Field[startx-1][starty  ] == 2))
	)
	then 
	
		local x = startx-1
		local y = starty

		if Field[x  ][y  ] < 3 							and power_used < power_max then Field[x  ][y  ] = 3-Field[x  ][y  ] end
		if Field[x-1][y-1] < 3 and y > 0	  and x > 0 and power_used < power_max then Field[x-1][y-1] = 3-Field[x-1][y-1] end
		if Field[x-1][y  ] < 3 				  and x > 0 and power_used < power_max then Field[x-1][y  ] = 3-Field[x-1][y  ] end	
		if Field[x-1][y+1] < 3 and y <= SizeY and x > 0 and power_used < power_max then Field[x-1][y+1] = 3-Field[x-1][y+1] end		


		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb off"]) 		    or (Field[x-1][y-1] == NamedTiles["Dark Matter Orb off"] and y > 0	  and x > 0)  		
		or (Field[x-1][y  ] == NamedTiles["Dark Matter Orb off"] and x > 0) or (Field[x-1][y+1] == NamedTiles["Dark Matter Orb off"] and y <= SizeY and x > 0)	
		then DMOstate = true end
		
		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb on"])  		   or (Field[x-1][y-1] == NamedTiles["Dark Matter Orb on"] and y > 0 	  and x > 0) 		
		or (Field[x-1][y  ] == NamedTiles["Dark Matter Orb on"] and x > 0) or (Field[x-1][y+1] == NamedTiles["Dark Matter Orb on"] and y <= SizeY and x > 0)	
		then DMOstate = false end	
		
		
	elseif	dir == "right"	
	and	(
	(AdjacentMustOn == true 	and (AdjacentMustOn 		and Field[startx+1][starty  ] == 2))
	or
	(AdjacentMustOn == false 	and ((not AdjacentMustOn) 	or  Field[startx+1][starty  ] == 2))
	)
	then 
	
		local x = startx+1
		local y = starty

		if Field[x  ][y  ] < 3 								 and power_used < power_max then Field[x  ][y  ] = 3-Field[x  ][y  ] end
		if Field[x+1][y-1] < 3 and y > 0	  and x <= SizeX and power_used < power_max then Field[x+1][y-1] = 3-Field[x+1][y-1] end
		if Field[x+1][y  ] < 3 				  and x <= SizeX and power_used < power_max then Field[x+1][y  ] = 3-Field[x+1][y  ] end	
		if Field[x+1][y+1] < 3 and y <= SizeY and x <= SizeX and power_used < power_max then Field[x+1][y+1] = 3-Field[x+1][y+1] end

		
		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb off"]) 			 		or (Field[x+1][y-1] == NamedTiles["Dark Matter Orb off"] and y > 0 	 and x <= SizeX)  		
		or (Field[x+1][y  ] == NamedTiles["Dark Matter Orb off"] and x <= SizeX) 	or (Field[x+1][y+1] == NamedTiles["Dark Matter Orb off"] and y <= SizeY and x <= SizeX)	
		then DMOstate = true end
		
		if (Field[x  ][y  ] == NamedTiles["Dark Matter Orb on"])  		 		or (Field[x+1][y-1] == NamedTiles["Dark Matter Orb on"] and y > 0 	 and x <= SizeX) 		
		or (Field[x+1][y  ] == NamedTiles["Dark Matter Orb on"] and x <= SizeX)	or (Field[x+1][y+1] == NamedTiles["Dark Matter Orb on"] and y <= SizeY and x <= SizeX)	
		then DMOstate = false end	

		
	end

end