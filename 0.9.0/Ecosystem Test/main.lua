function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 8
	TilesetX, TilesetY = TileSize*3, TileSize*3
	SizeX, SizeY = 128, 96
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	FPS = 0
	game_clock = 0
	
	Global_Oxygen_Level = 10000
	Global_COO_Level = 1000
	Global_Nitrogen_Level = 109090
	Global_CO_Level = 10
	
	Plant_Life_Length = 100
	
	
	
	TilesetPic = love.graphics.newImage('TILESET.png')
	Tileset = { }
	
	for j=1, 3 do
		for i=1, 3 do 
			Tileset[((i-1)*3)+j] = love.graphics.newQuad((j-1)*TileSize, (i-1)*TileSize, TileSize, TileSize, TilesetX, TilesetY)
		end
	end	
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
			Field[j][i] = 2		
		end
		
	end
	
	--put down some plants
	Plants = {}
	
	Plants[1] = {X = math.random(1,SizeX), Y = math.random(1,SizeY), Age = 0 }
	

	for i=2, 5 do
	
		x, y = math.random(1,SizeX), math.random(1,SizeY)
		

		while Field[x][y] == 3 do 
			x, y = math.random(1,SizeX), math.random(1,SizeY)
		end
	
		Plants[i] = { X = x, Y = y, Age = 0}
	end

end

function love.draw()

	for i=1, #Plants do 
		if Plants[i].X ~= 0 then
			Field[Plants[i].X][Plants[i].Y] = 3
		end
		--love.graphics.draw(TilesetPic, Tileset[3], (Plants[i].X-1)*TileSize, (Plants[i].Y-1)*TileSize) 
		--love.graphics.print(i, (Plants[i].X-1)*TileSize, (Plants[i].Y-1)*TileSize) 
	end
	
	for j=1,SizeX do
		for i=1, SizeY do
			love.graphics.draw(TilesetPic, Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
		end
	end
	
	love.graphics.print("Plaínt Population = "..#Plants..
						"\nO2 amount = "..Global_Oxygen_Level..
						"\nO2 per plant = "..math.floor(Global_Oxygen_Level/#Plants),0,0)
	love.graphics.print(FPS,1000,0)
	love.graphics.print(game_clock,900,16)
	
	i=1
	while i<#Plants and Plants[i].X ~= 0 do
		love.graphics.print("X = "..Plants[i].X..", Y = "..Plants[i].Y..", Age = "..Plants[i].Age, 300,16*i)
		i = i+1
	end
end

function love.update(dt)
		game_clock = game_clock + 1
		FPS = math.floor(1/dt)
		
		if game_clock % 1 == 0 and #Plants ~= 0 then

			for i=1, #Plants do	
				if Plants[i].X ~= 0 then
					Plant_Death(i)
					Plant_Reproduction(Plants[i])
					Plant_Respiration(Plants[i])
					Plant_Aging(Plants[i])	
				end
			end			

		end
end



--reproduction
--now literally absolutely randomised, as far as I pissed of myself on some previous fails
function Plant_Reproduction(plant)
	
	local likelihood = math.random(1,100)
	local PossibilitesX = {plant.X+1, plant.X, plant.X-1}
	local PossibilitesY = {plant.Y+1, plant.Y, plant.Y-1}
	local x = math.random(1, 3)
	local y = math.random(1, 3)
	local hasNeighbour = false
	
	--checks if place has at least one free neighbour
	for i=1, 3 do
		for j=1, 3 do
			if not (i==2 and j==2) 
			and not Occupied(PossibilitesX[i], PossibilitesY[j]) then
				hasNeighbour = true break
			end
		end
		if hasNeighbour then break end
	end
	
	if likelihood < 15
	and Global_Oxygen_Level/#Plants >200	--reproduce itself only in case of proper amount of oxygen
	and hasNeighbour then
	
		while Occupied(PossibilitesX[x], PossibilitesY[y]) 
		or PossibilitesX[x] < 1 or PossibilitesY[y] < 1 
		or PossibilitesX[x] > SizeX or PossibilitesY[y] > SizeY 
		or (x == 2 and y == 2) do
			x = math.random(1, 3) y = math.random(1, 3)		
		end
	
		table.insert(Plants, { X = PossibilitesX[x], Y = PossibilitesY[y], Age = 0} )
	end
	
end

function Plant_Respiration(plant)
	if Global_Oxygen_Level > 0 then
		Global_Oxygen_Level = Global_Oxygen_Level - 1
	else
		Field[plant.X][plant.Y] = 4
		table.remove(Plants, ID)
	end	
end

function Plant_Aging(plant)
	plant.Age = plant.Age + 1
end

function Plant_Death(ID)

	if Plants[ID].Age >= Plant_Life_Length 							--by age
	or (Global_Oxygen_Level < 2000 and math.random(1,100) <5)		--by low level of O2, with a given chance
	then
		Field[Plants[ID].X][Plants[ID].Y] = 4
		Plants[ID].X = 0
		--NOTE: Plants.X = 0 indicates death
	end

end









function Occupied(x, y)

	local tempbool = false
	
	for i=1, #Plants do
		if(Plants[i].X == x and Plants[i].Y == y) or (x<1 or y<1 or x>SizeX or y>SizeY)
		then tempbool = true break end
	end
	
	return tempbool

end











function CountNeighbours(x,y, valueToCheck, Table)

	local asd=0

	if Table[x-1][y] == valueToCheck then asd=asd+1 end
	if Table[x+1][y] == valueToCheck then asd=asd+1 end
	if Table[x][y-1] == valueToCheck then asd=asd+1 end
	if Table[x][y+1] == valueToCheck then asd=asd+1 end	
	return asd
end

function FullNeighbours(x,y, valueToCheck, Table)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			if Table[x-j][y-i] == valueToCheck then asd=asd+1 end	
		end
	end
	
	return asd
end

function CountColors(Table, valueToCheck)

	local asd=0
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(Table, valueToCheck)

	local list = {}
	for j=2,127 do
		for i=2, 95 do
			if Table[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function BackLoad(a,b)

	for j=1,128 do
		for i=1, 96 do
			a[j][i] = b[j][i]
		end
	end

end




