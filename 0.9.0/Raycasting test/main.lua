require 'conf'

function love.load()

	math.randomseed( os.time() )
	--key values
	TileSize = 32
	TilesetX, TilesetY = 96, 96
	SizeX, SizeY = 64, 40
	
	--variables
	x,y,z,i,j,k = 0,0,0,0,0,0
	FPS = 0

	TilesetPics = {}
	Tileset = {}
	Load_Tilesets(Tileset, 'TILESET.png', 32)
	
	TilesetPics[1]:setFilter("nearest", "nearest")
	
	sky = love.graphics.newImage('SKY.PNG')
	
	TilesetX, TilesetY = TilesetPics[1]:getWidth(), TilesetPics[1]:getHeight()
	
	--uploading field
	Field = { }
	
	for j=1,SizeX do
	
		Field[j] = { }
	
		for i=1, SizeY do
				if i==1 or i==SizeY or j==1 or j==SizeX then
				Field[j][i] = 3
				else
				Field[j][i] = 1	
				end
		end
		
	end
	
	Field[3][3] = 1
	
	for i=1, 200 do Field[math.random(2,SizeX-1)][math.random(2,SizeY-1)] = 2 end
	
	--new shit
	render = false
	gameclock = 0
	
	--creating player
	Player = { X = 63, Y = 63, direction = 225 }
	
	--creating rays
	raynum = 1280
	FOV = 66
	
	Rays = {}
	
	for i=1, raynum do
	 Rays[i] = { 
		angle = Player.direction + (FOV/raynum)*(i-1),
		enddotX = 0,
		enddotY = 0,
		wallheight = 0,
		comingX = 0,
		comingY = 0,

		}
	end
	


	for i=1, #Rays do
	DegreeFix(Rays[i].angle)
	
	 Rays[i].enddotX, Rays[i].enddotY, 
	 Rays[i].comingX, Rays[i].comingY,
	 Rays[i].coldir = Distance_Calculator(Player.X, Player.Y, Rays[i].angle, false)
	 Rays[i].wallheight = Distance_Calculator(Player.X, Player.Y, Rays[i].angle, true)
	end
	
end

function love.draw()

	if render then
		--map
		for j=1,SizeX do
			for i=1, SizeY do
				love.graphics.draw(TilesetPics[1], Tileset[Field[j][i]], (j-1)*TileSize, (i-1)*TileSize)
			end
		end

		love.graphics.draw(TilesetPics[1], Tileset[1], Player.X-16, Player.Y-16)
		
		for i=1, #Rays do
			--love.graphics.print(Field[Rays[i].comingX+1][Rays[i].comingY+1], Rays[i].comingX*TileSize, Rays[i].comingY*TileSize)
			love.graphics.line(Player.X, Player.Y, Rays[i].enddotX, Rays[i].enddotY)
		end
	
	else
	
		--render
		love.graphics.draw(sky, 0, 0)
		
		for i=1, #Rays do
			--x start, y start, x length, y length
			
			--textured wall			
			local NewTileSize = TileSize
			local ofs = 0
			
			if     Rays[i].coldir == "hor" then ofs = Rays[i].enddotX%TileSize
			elseif Rays[i].coldir == "ver" then ofs = Rays[i].enddotY%TileSize end			
			
			local slice = love.graphics.newQuad(
			ofs, 0, 
			1, Rays[i].wallheight, 
			TilesetX, Rays[i].wallheight*3)

			love.graphics.draw(TilesetPics[1], slice, (i-1)*(1280/raynum), 360-(Rays[i].wallheight/2))	
			

			
			--[[
			--untextured walls
		

			local br = 255			
			
			--if Rays[i].wallheight/4 < 255 then br = Rays[i].wallheight/4 end
			
			love.graphics.setColor( br,br,br )
			
			if i%16 == 0 then love.graphics.print(math.floor(br), (i-1)*(1280/raynum), 360-(Rays[i].wallheight/2)-32) end
			love.graphics.rectangle("fill", (i-1)*(1280/raynum), 360-(Rays[i].wallheight/2), (1280/raynum), Rays[i].wallheight)

			]]
		end
	
	end
	
	love.graphics.print(math.floor(Player.direction).." <-> "..Rays[raynum].angle.."\n"..FPS..
	"\n"..math.sin(math.rad(Player.direction))..
	"\n"..math.cos(math.rad(Player.direction)),0,0)
end



function love.update(dt)
	
	gameclock = gameclock+1
	FPS = math.ceil(1/dt)
	
	--rays as constants
	for i=1, raynum do

		DegreeFix(Rays[i].angle)
	
		Rays[i].angle = 180 - (Player.direction - (FOV/2) + (FOV/raynum)*(i-1))
		Rays[i].enddotX, Rays[i].enddotY,
		Rays[i].comingX, Rays[i].comingY,
		Rays[i].coldir = Distance_Calculator(Player.X, Player.Y, Rays[i].angle, false)
		Rays[i].wallheight = Distance_Calculator(Player.X, Player.Y, Rays[i].angle, true)	
		
	end		
		
	KeyActions()

end



--returns the distance of the closest wall to (x,y)
--input: a ray, output: endpoints of ray
function Distance_Calculator(x,y,dir, wallheight)
	

	local radian = math.rad(dir)
	local DeltaX, DeltaY = 0,0
	local ComingHorX, ComingHorY = 2,2
	local ComingVerX, ComingverY = 2,2
	local EndVer = {}			--endpoint of only vertically aligned rays
	local EndHor = {}			--endpoint of only horizontally aligned rays
	local Xmulti, Ymulti = 1, 1
	local HorLength, VerLength = 0, 0

	--1: where the ray goes? (X+, Y+ / X-, Y- / X+, Y- / X-, Y+)
	--2: set X to edge until wall is found
	--3: set Y to edge until wall is found
	--4: compare full length of Player-EndVer and Player-EndHor
	--5: return EndVer or EndVor, depending on which makes a shorter ray
	
	--[[
		0-90: X+, Y+ (sin+, cos+)
		90-180: X+, Y- (sin+, cos-)
		180-270: X-, Y- (you know the rest of it)
		270-360: X-, Y+
	]]
	
	--create rays horizontally (X)
	EndHor[1], EndHor[2] = x, y	
	radian = math.rad(dir)
	
	--set from player to first intersection
	--is the line facing up or down?
	if math.sin(radian)<= 0 then EndHor[2] = (math.floor(y/TileSize)*TileSize) - 1			
							else EndHor[2] = (math.floor(y/TileSize)*TileSize) + TileSize end	
							
	
	EndHor[1] = x + ((y-EndHor[2])/math.tan(radian))
	
	ComingHorX = math.floor(EndHor[1]/TileSize)
	ComingHorY = math.floor(EndHor[2]/TileSize)
	

	--then set from first intersection to next 'til wall is found
	while ComingHorX > 0 and ComingHorX < SizeX-1 
	and ComingHorY > 0 and ComingHorY < SizeY-1
	and Field[ComingHorX+1][ComingHorY+1] ~= 2 do
		
		radian = math.rad(180-dir)
		
		if math.sin(radian)<= 0 then Ymulti = -1 else Ymulti = 1 end

		DeltaY = Ymulti*TileSize
		DeltaX = DeltaY/math.tan(radian)

		EndHor[2] = EndHor[2] + DeltaY
		EndHor[1] = EndHor[1] + DeltaX
		
		
		--sadly, I have to calculate ComingHorX and ComingHorY
		--either way to check if EndHor hits a wall
		ComingHorX = math.floor(EndHor[1]/TileSize)
		ComingHorY = math.floor(EndHor[2]/TileSize)
	
	end

	--calculate length (square root is not necessary)
	--finally, the whole shit is multiplied by cos(radian)
	HorLength = ((EndHor[1] - Player.X)*(EndHor[1] - Player.X) + (EndHor[2] - Player.Y)*(EndHor[2] - Player.Y)) 
	--HorLength = HorLength*math.cos(math.rad(dir-Rays[raynum/2].angle))





	
	
	--create rays vertically (Y)
	EndVer[1], EndVer[2] = x, y
	radian = math.rad(dir-180)	
	
	--set from player to first intersection
	if math.cos(radian)<= 0 then EndVer[1] = (math.floor(x/TileSize)*TileSize) - 1			
							else EndVer[1] = (math.floor(x/TileSize)*TileSize) + TileSize end	
							
	
	EndVer[2] = y + ((x-EndVer[1])*math.tan(radian))
	
	ComingVerX = math.floor(EndVer[1]/TileSize)
	ComingVerY = math.floor(EndVer[2]/TileSize)

	--then set from first intersection to next 'til wall is found
	while ComingVerX > 0 and ComingVerX < SizeX-1 
	and ComingVerY > 0 and ComingVerY < SizeY-1
	and Field[ComingVerX+1][ComingVerY+1] ~= 2 do
	
		radian = math.rad(180-dir)
		
		if math.cos(radian)<= 0 then Xmulti = -1 else Xmulti = 1 end

		DeltaX = Xmulti*TileSize
		DeltaY = DeltaX*math.tan(radian)

		EndVer[1] = EndVer[1] + DeltaX
		EndVer[2] = EndVer[2] + DeltaY
		
		--sadly, I have to calculate ComingHorX and ComingHorY
		--either way to check if EndHor hits a wall
		ComingVerX = math.floor(EndVer[1]/TileSize)
		ComingVerY = math.floor(EndVer[2]/TileSize)
	
	end

	--calculate length (square root is not necessary)	
	VerLength = ((EndVer[1] - Player.X)*(EndVer[1] - Player.X) + (EndVer[2] - Player.Y)*(EndVer[2] - Player.Y)) 
	--VerLength = VerLength*math.cos(math.rad(dir-Rays[raynum/2].angle))

	if wallheight then
		if HorLength < VerLength 
		then return (TileSize/HorLength)*200000
		else return (TileSize/VerLength)*200000 end	
	else

		
		if HorLength < VerLength
		then return EndHor[1], EndHor[2], ComingHorX, ComingHorY, "hor"
		else return EndVer[1], EndVer[2], ComingVerX, ComingVerY, "ver" end

	end
	
end

function KeyActions()

	local middle = (Rays[raynum/2].angle + Rays[(raynum/2)+1].angle)/2

	if love.keyboard.isDown("w") then 
		Player.X = Player.X + math.cos(math.rad(Player.direction))*1
		Player.Y = Player.Y + math.sin(math.rad(Player.direction))*1
	end		
	if love.keyboard.isDown("s") then 
		Player.X = Player.X - math.cos(math.rad(Player.direction))*1
		Player.Y = Player.Y - math.sin(math.rad(Player.direction))*1
	end	
	if love.keyboard.isDown("a") then 
		--Player.X = Player.X - math.cos(math.rad(Rays[raynum/2].angle))*2
		--Player.Y = Player.Y + math.sin(math.rad(Rays[raynum/2].angle))*2
	end		
	if love.keyboard.isDown("d") then 
		--Player.X = Player.X + math.cos(math.rad(Rays[raynum/2].angle))*2
		--Player.Y = Player.Y - math.sin(math.rad(Rays[raynum/2].angle))*2
	end
	
	
	if love.keyboard.isDown("q") then Player.direction = Player.direction - 2 if Player.direction<0 then Player.direction = Player.direction + 360 end	end
	if love.keyboard.isDown("e") then Player.direction = Player.direction + 2 if Player.direction>360 then Player.direction = Player.direction - 360 end	end	
		
end

function love.keypressed( key )

	if key == "f" then render = not render end

end

function DegreeFix(dir)

	if dir<=0 then dir = dir+360 end
	if dir>=360 then dir = dir-360 end

end


--a table with the name of "SetName" must be declared before its call
function Load_Tilesets(SetName, ImageName, UnitSize)
	table.insert(TilesetPics, love.graphics.newImage(ImageName))
	
	local CharX = TilesetPics[1]:getWidth()
	local CharY = TilesetPics[1]:getHeight()
	
	for j=1, CharX do
		for i=1, CharY do 
			SetName[((i-1)*CharX)+j] = love.graphics.newQuad((j-1)*UnitSize, (i-1)*UnitSize, UnitSize, UnitSize, CharX, CharY)
		end
	end	
end



